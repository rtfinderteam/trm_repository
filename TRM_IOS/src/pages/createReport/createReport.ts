import { Component, OnInit } from '@angular/core';
import { NavController, DateTime } from 'ionic-angular';
import { ImagePicker } from '@ionic-native/image-picker';
import { ToastController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { ViewReport } from '../viewReport/viewReport';
import { AlertController } from 'ionic-angular';
import { Engine } from '../../models/engine';
import { Material } from '../../models/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Customer } from '../../models/customer';
import { Report } from '../../models/report';
import { DatePipe } from '@angular/common';
import { Toollist } from '../../models/toollist';
import { Toolvaluepage } from '../../models/Toolvaluepage';
import { Storage } from '@ionic/storage';


@Component({
    selector: 'page-createReport',
    templateUrl: 'createReport.html'
})
export class CreateReport implements OnInit {
    public reportData: Array<string>;

    //Binding Variables
    //Basic Information
    public projectname: string;
    public employeeshort: string;
    public date: string;
    public buildingcomponent: string;
    public workpiececlamping: string;
    public strength: string;
    public information: string;
    public summary: string;

    //Customer
    public name: string;
    public contactpartner: string;
    public branch: string;
    allCustomer: Customer[];
    selectedCustomerId: number;
    selectedCustomer: Customer[];

    //Maschine
    public manufacturer: string;
    public type: string;
    public spindle: string;
    public coolantOut: number;
    public coolantIn: number;
    public power: number;
    public rotationSpeed: number;
    public feed: number;
    public constructionYear: number;
    public hourlyRate: number;
    public controlling: string;
    public programming: string;
    public path: string;
    public picturename: string;
    allEngines: Engine[];
    selectedEngineId: number;
    selectedEngine: Engine[];

    //Material
    public dinCode: number;
    public enCode: number;
    public designation: string;
    public hardness: string;
    public materialGroup: string;
    public crpercent: number;
    public sipercent: number;
    public nipercent: number;
    public cupercent: number;
    public nbpercent: number;
    public cepercent: number;
    public znpercent: number;
    public wppercent: number;
    public mnpercent: number;
    public vpercent: number;
    public tipercent: number;
    public copercent: number;
    public alpercent: number;
    public bpercent: number;
    public spercent: number;
    public npercent: number;
    public mopercent: number;
    public pbpercent: number;
    public fepercent: number;
    public cpercent: number;
    public mgpercent: number;
    allMaterials: Material[];
    selectedMaterialId: number;
    selectedMaterial: Material[];

    //TOOLS
    usedTools: number = 0;
    //Toollist
    manufacture: string;
    manufacturepicture: number = 1;
    iscompetitor: boolean;
    toolholder: string;
    toolid: string;
    insert: string;
    coating: string;
    flutes: number;
    cuttingedges: number;
    diameter: number;
    necklength: number;
    overhang: number;
    processing: string;

    //Toolvaluepage
    processingVP: string;
    noise: string;
    cooling: string;
    operationtime: number;
    ofwear: string;
    wearintensity: string;
    strategy: string;
    materialremovalrate: string;
    rampingangle: number;
    application: string;
    actualcuttingspeed: number;
    actualspindlespeed: number;
    actualfeedperminute: number;
    actualfeedpertooth: number;
    actualfeedperrevolution: number;
    actualaxial: number;
    actualradial: number;
    expectedcuttingspeed: number;
    expectedspindelspeed: number;
    expectedfeedperminute: number;
    expectedfeedpertooth: number;
    expectedfeedperrevolution: number;
    expectedaxial: number;
    expectedradial: number;
    resulotion: number;
    hasavideo: boolean;
    aplicationtype: string;


    toollists: Toollist[] = [];
    toolvaluepages: Toolvaluepage[] = [];

    savedReportId: number;

    gotEngineFromDatabase: boolean = false;
    gotMaterialFromDatabase: boolean = false;
    gotCustomerFromDatabase: boolean = false;
    engineParams: Engine = new Engine;
    materialParams: Material = new Material;
    customerParams: Customer = new Customer;
    reportParams: Report = new Report;

    apiConnection: boolean = true;

    ngOnInit() {
        this.checkSavedReport();
        this.selectEngines();
        this.selectMaterials();
        this.selectCustomer();
    }

    checkSavedReport() {
        let saved: boolean;
        this.storage.get('isReportSaved').then((val) => {
            console.log(val);
            if (val != null) {
                saved = val;
            }
            if (saved) {
                let alert = this.alertCtrl.create();
                alert.setTitle('Saved Report Found');
                alert.addButton('Cancel');
                alert.addButton({
                    text: 'Load',
                    handler: (data: any) => {
                        let material: Material
                        let engine: Engine
                        let customer: Customer;
                        this.storage.get('material').then((val) => {
                            if (val != null) {
                                material = val;
                                this.dinCode = material.dincode;
                                this.enCode = material.encode;
                                this.designation = material.designation;
                                this.hardness = material.hardness;
                                this.materialGroup = material.materialgroup;
                                this.crpercent = material.crpercent;
                                this.sipercent = material.sipercent;
                                this.nipercent = material.nipercent;
                                this.cupercent = material.cupercent;
                                this.nbpercent = material.nbpercent;
                                this.cepercent = material.cepercent;
                                this.znpercent = material.znpercent;
                                this.wppercent = material.wpercent;
                                this.mnpercent = material.mnpercent;
                                this.vpercent = material.vpercent;
                                this.tipercent = material.tipercent;
                                this.copercent = material.copercent;
                                this.alpercent = material.alpercent;
                                this.bpercent = material.bpercent;
                                this.spercent = material.spercent;
                                this.npercent = material.npercent;
                                this.mopercent = material.mopercent;
                                this.pbpercent = material.pbpercent;
                                this.fepercent = material.fepercent;
                                this.cpercent = material.cpercent;
                                this.mgpercent = material.mgpercent;
                            }
                        });
                        this.storage.get('engine').then((val) => {
                            if (val != null) {
                                engine = val;
                                this.manufacture = engine.manufacture;
                                this.type = engine.typ;
                                this.spindle = engine.spindel;
                                this.coolantOut = engine.coolantoutside;
                                this.coolantIn = engine.coolantinside;
                                this.power = engine.power;
                                this.rotationSpeed = engine.rotationalspeed;
                                this.feed = engine.feed;
                                this.constructionYear = engine.activationyear;
                                this.hourlyRate = engine.hourlyrate;
                                this.programming = engine.programming;
                                this.controlling = engine.controlling;
                                this.path = engine.path;
                                this.picturename = engine.picturename;
                            }
                        });
                        this.storage.get('customer').then((val) => {
                            if (val != null) {
                                customer = val;
                                this.name = customer.name;
                                this.branch = customer.branch;
                                this.contactpartner = customer.contactpartner;
                            }
                        });
                        this.storage.get('projectname').then((val) => {
                            if (val != null) {
                                this.projectname = val;
                            }
                        });
                        this.storage.get('employeeshort').then((val) => {
                            if (val != null) {
                                this.employeeshort = val;
                            }
                        });
                        this.storage.get('date').then((val) => {
                            if (val != null) {
                                this.date = val;
                            }
                        });
                        this.storage.get('buildingcomponent').then((val) => {
                            if (val != null) {
                                this.buildingcomponent = val;
                            }
                        });
                        this.storage.get('workpiececlamping').then((val) => {
                            if (val != null) {
                                this.workpiececlamping = val;
                            }
                        });
                        this.storage.get('strength').then((val) => {
                            if (val != null) {
                                this.strength = val;
                            }
                        });
                        this.storage.get('information').then((val) => {
                            if (val != null) {
                                this.information = val;
                            }
                        });
                        this.storage.get('summary').then((val) => {
                            if (val != null) {
                                this.summary = val;
                            }
                        });
                    }
                });
                alert.present();
            }
        });
    }

    getHeader(): Headers {
        const headers = new Headers();
        headers.append('Content-Type', 'text/JSON');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');;
        headers.append("Authorization", "Basic " + btoa('admin:nimda'));
        return headers;
    }

    selectEngines() {
        console.log("--------------allEngines-------------");
        let opt = new RequestOptions({
            headers: this.getHeader()
        });
        this.http.get('http://localhost:53673/api/Engine', opt)
            .subscribe(response => {
                console.log("RESPONSE IM HTTP: ");
                console.log(response);
                this.allEngines = response.json() as Engine[];
            });
    }

    selectMaterials() {
        console.log("--------------allMaterials-------------");
        let opt = new RequestOptions({
            headers: this.getHeader()
        });
        this.http.get('http://localhost:53673/api/Material', opt)
            .subscribe(response => {
                console.log("RESPONSE IM HTTP: ");
                console.log(response);
                this.allMaterials = response.json() as Material[];
            });
    }

    selectCustomer() {
        console.log("--------------allCustomer-------------");
        let opt = new RequestOptions({
            headers: this.getHeader()
        });
        this.http.get('http://localhost:53673/api/Customer', opt)
            .subscribe(response => {
                console.log("RESPONSE IM HTTP: ");
                console.log(response);
                this.allCustomer = response.json() as Customer[];
            });
    }

    constructor(public navCtrl: NavController, private toastCtrl: ToastController, private alertCtrl: AlertController,
        public http: Http, private datePipe: DatePipe, private storage: Storage) { }

    saveReport() {
        if (this.allCustomer == null || this.allEngines == null || this.allMaterials == null) {
            this.apiConnection = false;
        }
        let alert = this.alertCtrl.create();
        alert.setTitle('Finish creating an Report: ' + this.usedTools + ' Tool added');


        alert.addButton('Cancel');
        alert.addButton({
            text: 'Ok',
            handler: (data: any) => {
                this.saveReportAfterCheck();
            }
        });

        alert.present();
    }

    saveReportAfterCheck() {
        if (this.apiConnection) {
            if (!this.gotEngineFromDatabase) {
                console.log("SAVE ENGINE");
                this.fillEngineParams();

                //save Engine in Database
                const body = JSON.stringify(this.engineParams);
                console.log("ENGINEBODY " + body);
                const headers = this.getHeader();
                this.http.post('http://localhost:53673/api/Engine', body, { headers: headers })
                    .subscribe(response => {
                        console.log(response);
                        this.selectedEngineId = response.json() as number;
                    });
            }

            if (!this.gotMaterialFromDatabase) {
                console.log("SAVE MATERIAL");
                this.fillMaterialParams();

                //save Material in Database
                const body = JSON.stringify(this.materialParams);
                console.log("MATERIALBODY " + body);
                const headers = this.getHeader();
                this.http.post('http://localhost:53673/api/Material', body, { headers: headers })
                    .subscribe(response => {
                        console.log(response);
                        this.selectedMaterialId = response.json() as number;
                    });
            }

            if (!this.gotCustomerFromDatabase) {
                console.log("SAVE CUSTOMER");
                this.fillCustomerParams();

                const body = JSON.stringify(this.customerParams);
                const headers = this.getHeader();
                this.http.post('http://localhost:53673/api/Customer', body, { headers: headers })
                    .subscribe(response => {
                        console.log(response);
                        this.selectedCustomerId = response.json() as number;
                    });
            }

            this.fillReportParams();
            const body = JSON.stringify(this.reportParams);
            console.log("REPORTBODY " + body);
            const headers = this.getHeader();
            this.http.post('http://localhost:53673/api/Report', body, { headers: headers })
                .subscribe(response => {
                    console.log(response);
                    this.savedReportId = response.json() as number;

                    let cnt: number = 0;

                    for (let item of this.toollists) {
                        item.report = this.savedReportId;

                        const body = JSON.stringify(item);
                        console.log("TOOLLIST " + body);
                        const headers = this.getHeader();
                        this.http.post('http://localhost:53673/api/Toollist', body, { headers: headers })
                            .subscribe(response => {
                                let id = response.json() as number;
                                console.log('TOOLVALUEPAGES: ' + this.toolvaluepages);
                                console.log('TOOLVALUEPAGES cnt: ' + this.toolvaluepages[cnt]);
                                this.toolvaluepages[cnt].tool_list = id;


                                const body = JSON.stringify(this.toolvaluepages[cnt]);

                                console.log("TOOLVALUEPAGE " + body);
                                const headers = this.getHeader();
                                this.http.post('http://localhost:53673/api/Toolvaluepage', body, { headers: headers })
                                    .subscribe(response => {
                                        console.log(response);

                                    });
                                cnt++;
                            });
                    }

                    let toast = this.toastCtrl.create({
                        message: 'Report added! ID: ' + this.savedReportId,
                        duration: 2000,
                        position: 'bottom'
                    });
                    toast.present();
                });
        } else {
            let toast = this.toastCtrl.create({
                message: 'No Connection available. Report saved ',
                duration: 2000,
                position: 'bottom'
            });
            toast.present();


            this.fillCustomerParams();
            this.fillEngineParams();
            this.fillMaterialParams();
            this.storage.set('engine', this.engineParams);
            this.storage.set('customer', this.customerParams);
            this.storage.set('material', this.materialParams);
            this.storage.set('projectname', this.projectname);
            this.storage.set('employeeshort', this.employeeshort);
            this.storage.set('date', this.date);
            this.storage.set('buildingcomponent', this.buildingcomponent);
            this.storage.set('workpiececlamping', this.workpiececlamping);
            this.storage.set('strength', this.strength);
            this.storage.set('information', this.information);
            this.storage.set('summary', this.summary);
            this.storage.set('isReportSaved', true);

        }
    }

    fillReportParams() {
        let d = new Date('1.1.2000 9:00:00 PM');
        let split = this.date.split('.');
        d.setDate(Number.parseInt(split[0]));
        d.setMonth(Number.parseInt(split[1]));
        d.setFullYear(Number.parseInt(split[2]));
        let newdate = this.datePipe.transform(d, 'dd/MM/yyyy hh:mm:ss tt');
        console.log(newdate);
        this.reportParams.projectname = this.projectname;
        this.reportParams.employeeshort = this.employeeshort;
        this.reportParams.date = newdate;
        this.reportParams.buildingcomponent = this.buildingcomponent;
        this.reportParams.workpiececlamping = this.workpiececlamping;
        this.reportParams.strength = this.strength;
        this.reportParams.information = this.information;
        this.reportParams.summary = this.summary;
        this.reportParams.engine = this.selectedEngineId;
        this.reportParams.customer = this.selectedCustomerId;
        this.reportParams.material = this.selectedMaterialId;
    }

    fillCustomerParams() {
        this.customerParams.name = this.name;
        this.customerParams.contactpartner = this.contactpartner;
        this.customerParams.branch = this.branch;
    }


    fillMaterialParams() {
        this.materialParams.dincode = this.dinCode;
        this.materialParams.encode = this.enCode;
        this.materialParams.designation = this.designation;
        this.materialParams.hardness = this.hardness;
        this.materialParams.crpercent = this.crpercent;
        this.materialParams.sipercent = this.sipercent;
        this.materialParams.nipercent = this.nipercent;
        this.materialParams.cupercent = this.cupercent;
        this.materialParams.nbpercent = this.nbpercent;
        this.materialParams.cepercent = this.cepercent;
        this.materialParams.znpercent = this.znpercent;
        this.materialParams.wpercent = this.wppercent;
        this.materialParams.mnpercent = this.mnpercent;
        this.materialParams.vpercent = this.vpercent;
        this.materialParams.tipercent = this.tipercent;
        this.materialParams.copercent = this.copercent;
        this.materialParams.alpercent = this.alpercent;
        this.materialParams.bpercent = this.bpercent;
        this.materialParams.spercent = this.spercent;
        this.materialParams.npercent = this.npercent;
        this.materialParams.mopercent = this.mopercent;
        this.materialParams.pbpercent = this.pbpercent;
        this.materialParams.fepercent = this.fepercent;
        this.materialParams.cpercent = this.cpercent;
        this.materialParams.mgpercent = this.mgpercent;
    }


    fillEngineParams() {
        this.engineParams.manufacture = this.manufacturer;
        this.engineParams.typ = this.type;
        this.engineParams.spindel = this.spindle;
        this.engineParams.coolantoutside = this.coolantOut;
        this.engineParams.coolantinside = this.coolantIn;
        this.engineParams.power = this.power;
        this.engineParams.rotationalspeed = this.rotationSpeed;
        this.engineParams.feed = this.feed;
        this.engineParams.activationyear = this.constructionYear;
        this.engineParams.hourlyrate = this.hourlyRate;
        this.engineParams.programming = this.programming;
        this.engineParams.controlling = this.controlling;
        this.engineParams.path = this.path;
        this.engineParams.picturename = this.picturename;
    }

    doEngine() { //Alert, selecting an Engine from the database
        let alert = this.alertCtrl.create();
        alert.setTitle('Select an Engine');

        for (let engine of this.allEngines) {
            alert.addInput({
                type: 'radio',
                label: engine.manufacture + ', ' + engine.typ,
                value: engine.id + ''
            });
        }
        alert.addButton('Cancel');
        alert.addButton({
            text: 'Ok',
            handler: (data: any) => {
                console.log('Radio data:', data);
                this.selectedEngineId = data;
                this.setEngine();
            }
        });

        alert.present();
    }

    setEngine() {
        let opt = new RequestOptions({
            headers: this.getHeader()
        });
        this.http.get('http://localhost:53673/api/Engine/' + this.selectedEngineId, opt)
            .subscribe(response => {
                console.log(response);
                this.selectedEngine = response.json() as Engine[];

                //Binding, set the values of the selected Engine
                this.manufacturer = this.selectedEngine[0].manufacture;
                this.type = this.selectedEngine[0].typ;
                this.spindle = this.selectedEngine[0].spindel;
                this.coolantOut = this.selectedEngine[0].coolantoutside;
                this.coolantIn = this.selectedEngine[0].coolantinside;
                this.power = this.selectedEngine[0].power;
                this.rotationSpeed = this.selectedEngine[0].rotationalspeed;
                this.feed = this.selectedEngine[0].feed;
                this.constructionYear = this.selectedEngine[0].activationyear;
                this.hourlyRate = this.selectedEngine[0].hourlyrate;
                this.controlling = this.selectedEngine[0].controlling;
                this.programming = this.selectedEngine[0].programming;
                this.path = this.selectedEngine[0].path;
                this.picturename = this.selectedEngine[0].picturename;
            });
        this.gotEngineFromDatabase = true;
    }

    doMaterial() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Select a Material');

        for (let material of this.allMaterials) {
            alert.addInput({
                type: 'radio',
                label: material.designation + ', Hardness: ' + material.hardness + ', ' + material.materialgroup,
                value: material.id + ''
            });
        }
        alert.addButton('Cancel');
        alert.addButton({
            text: 'Ok',
            handler: (data: any) => {
                console.log('Radio data:', data);
                this.selectedMaterialId = data;
                this.setMaterial();
            }
        });

        alert.present();
    }

    setMaterial() {
        let opt = new RequestOptions({
            headers: this.getHeader()
        });
        this.http.get('http://localhost:53673/api/Material/' + this.selectedMaterialId, opt)
            .subscribe(response => {
                console.log(response);
                this.selectedMaterial = response.json() as Material[];

                //Binding, set the values of the selected Material
                this.dinCode = this.selectedMaterial[0].dincode;
                this.enCode = this.selectedMaterial[0].encode;
                this.designation = this.selectedMaterial[0].designation;
                this.hardness = this.selectedMaterial[0].hardness;
                this.materialGroup = this.selectedMaterial[0].materialgroup;
                this.crpercent = this.selectedMaterial[0].crpercent;
                this.sipercent = this.selectedMaterial[0].sipercent;
                this.nipercent = this.selectedMaterial[0].nipercent;
                this.cupercent = this.selectedMaterial[0].cupercent;
                this.nbpercent = this.selectedMaterial[0].nbpercent;
                this.cepercent = this.selectedMaterial[0].cepercent;
                this.znpercent = this.selectedMaterial[0].znpercent;
                this.wppercent = this.selectedMaterial[0].wpercent;
                this.mnpercent = this.selectedMaterial[0].mnpercent;
                this.vpercent = this.selectedMaterial[0].vpercent;
                this.tipercent = this.selectedMaterial[0].tipercent;
                this.copercent = this.selectedMaterial[0].copercent;
                this.alpercent = this.selectedMaterial[0].alpercent;
                this.bpercent = this.selectedMaterial[0].bpercent;
                this.spercent = this.selectedMaterial[0].spercent;
                this.npercent = this.selectedMaterial[0].nbpercent;
                this.mopercent = this.selectedMaterial[0].mopercent;
                this.pbpercent = this.selectedMaterial[0].pbpercent;
                this.fepercent = this.selectedMaterial[0].fepercent;
                this.cpercent = this.selectedMaterial[0].cpercent;
                this.mgpercent = this.selectedMaterial[0].mgpercent;
            });

        this.gotMaterialFromDatabase = true;
    }

    doCustomer() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Select a Customer');

        for (let customer of this.allCustomer) {
            alert.addInput({
                type: 'radio',
                label: customer.name + ' ,' + customer.branch,
                value: customer.id + ''
            });
        }
        alert.addButton('Cancel');
        alert.addButton({
            text: 'Ok',
            handler: (data: any) => {
                console.log('Radio data:', data);
                this.selectedCustomerId = data;
                this.setCustomer();
            }
        });

        alert.present();
    }

    setCustomer() {
        let opt = new RequestOptions({
            headers: this.getHeader()
        });
        this.http.get('http://localhost:53673/api/Customer/' + this.selectedCustomerId, opt)
            .subscribe(response => {
                console.log(response);
                this.selectedCustomer = response.json() as Customer[];

                //Binding, set the values of the selected Customer
                this.name = this.selectedCustomer[0].name;
                this.branch = this.selectedCustomer[0].branch;
                this.contactpartner = this.selectedCustomer[0].contactpartner;

            });
        this.gotCustomerFromDatabase = true;
    }

    addTool() {
        this.setData();
        this.clearFields();
        let toast = this.toastCtrl.create({
            message: 'Tool added',
            duration: 2000,
            position: 'bottom'
        });
        toast.present();
    }

    setData() {
        let toollist: Toollist = new Toollist();
        let toolvaluepage: Toolvaluepage = new Toolvaluepage();

        toollist.manufacture = this.manufacture;
        toollist.manufacture_picture = this.manufacturepicture;
        toollist.iscompentitor = this.iscompetitor;
        toollist.toolholder = this.toolholder;
        toollist.toolid = this.toolid;
        toollist.insert = this.insert;
        toollist.coating = this.coating;
        toollist.flutes = this.flutes;
        toollist.cuttingedges = this.cuttingedges;
        toollist.diameter = this.diameter;
        toollist.necklength = this.necklength;
        toollist.overhang = this.overhang;
        toollist.processing = this.processing;

        this.toollists.push(toollist);

        toolvaluepage.processing = this.processingVP;
        toolvaluepage.noise = this.noise;
        toolvaluepage.cooling = this.cooling;
        toolvaluepage.operationtime = this.operationtime;
        toolvaluepage.ofwear = this.ofwear;
        toolvaluepage.wearintensity = this.wearintensity;
        toolvaluepage.strategy = this.strategy;
        toolvaluepage.materialremovalrate = this.materialremovalrate;
        toolvaluepage.rampingangle = this.rampingangle;
        toolvaluepage.application = this.application;
        toolvaluepage.actualcuttingspeed = this.actualcuttingspeed;
        toolvaluepage.actualspindlespeed = this.actualspindlespeed;
        toolvaluepage.actualfeedperminute = this.actualfeedperminute;
        toolvaluepage.actualfeedpertooth = this.actualfeedpertooth;
        toolvaluepage.actualfeedperrevolution = this.actualfeedperrevolution;
        toolvaluepage.actualaxial = this.actualaxial;
        toolvaluepage.actualradial = this.actualradial;
        toolvaluepage.expectedcuttingspeed = this.expectedcuttingspeed;
        toolvaluepage.expectedspindelspeed = this.expectedspindelspeed;
        toolvaluepage.expectedfeedperminute = this.expectedfeedperminute;
        toolvaluepage.expectedfeedpertooth = this.expectedfeedpertooth;
        toolvaluepage.expectedfeedperrevolution = this.expectedfeedperrevolution;
        toolvaluepage.expectedaxial = this.expectedaxial;
        toolvaluepage.expectedradial = this.expectedradial;
        toolvaluepage.resulotion = this.resulotion;
        toolvaluepage.hasavideo = this.hasavideo;
        toolvaluepage.aplicationtype = this.aplicationtype;

        this.toolvaluepages.push(toolvaluepage);
        console.log(toolvaluepage);
        this.usedTools++;
    }

    clearFields() {
        this.manufacture = "";
        this.iscompetitor = false;
        this.toolholder = "";
        this.toolid = "";
        this.insert = "";
        this.coating = "";
        this.flutes = null;
        this.cuttingedges = null;
        this.diameter = null;
        this.necklength = null;
        this.overhang = null;
        this.processing = "";

        //Toolvaluepage
        this.processingVP = "";
        this.noise = "";
        this.cooling = "";
        this.operationtime = null;
        this.ofwear = "";
        this.wearintensity = "";
        this.strategy = "";
        this.materialremovalrate = "";
        this.rampingangle = null;
        this.application = "";
        this.actualcuttingspeed = null;
        this.actualspindlespeed = null;
        this.actualfeedperminute = null;
        this.actualfeedpertooth = null;
        this.actualfeedperrevolution = null;
        this.actualaxial = null;
        this.actualradial = null;
        this.expectedcuttingspeed = null;
        this.expectedspindelspeed = null;
        this.expectedfeedperminute = null;
        this.expectedfeedpertooth = null;
        this.expectedfeedperrevolution = null;
        this.expectedaxial = null;
        this.expectedradial = null;
        this.resulotion = null;
        this.hasavideo = null;
        this.aplicationtype = "";
    }
}