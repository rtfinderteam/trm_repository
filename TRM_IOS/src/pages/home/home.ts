import { Component } from '@angular/core';
import { Report } from '../../models/report';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Material } from '../../models/material';
import { Engine } from '../../models/engine';
import { Customer } from '../../models/customer';
import { AlertController } from 'ionic-angular';

import * as pdfmake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  frequentlyUsed: number[] = [];
  usedReports: Report[] = [];
  selectedReports: Report[] = [];
  material: Material = new Material;
  engine: Engine = new Engine;
  customer: Customer = new Customer;

  constructor(private storage: Storage, private http: Http, private alertCtrl: AlertController) {
  }

  ngOnInit() {
    this.getRecent();
  }

  ionViewDidEnter() {
    this.getRecent();
  }

  getRecent() {
    this.usedReports = [];
    this.storage.get('frequentlyUsed').then((val) => {
      console.log(val);
      if (val != null) {
        this.frequentlyUsed = val;
      }

    });
    let opt = new RequestOptions({
      headers: this.getHeader()
    });
    for (let item of this.frequentlyUsed) {
      this.http.get('http://localhost:53673/api/Report/' + item, opt)
        .subscribe(response => {
          console.log('GETRESPONSE ' + response);
          this.usedReports.push(response.json() as Report);
          console.log(this.usedReports);
        });
    }
  }

  getHeader(): Headers {
    const headers = new Headers();
    headers.append('Content-Type', 'text/JSON');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');;
    headers.append("Authorization", "Basic " + btoa('admin:nimda'));
    return headers;
  }

  download(report: Report) {
    let opt = new RequestOptions({
      headers: this.getHeader()
    });
    pdfmake.vfs = pdfFonts.pdfMake.vfs;
    this.http.get('http://localhost:53673/api/Material/' + report.material, opt)
      .subscribe(response => {
        console.log(response);
        this.material = response.json() as Material;
        this.http.get('http://localhost:53673/api/Engine/' + report.engine, opt)
          .subscribe(response => {
            console.log(response);
            this.engine = response.json() as Engine;
            this.http.get('http://localhost:53673/api/Customer/' + report.customer, opt)
              .subscribe(response => {
                this.customer = response.json() as Customer;
                var docDefinition = this.createDocDefinition(report, this.engine[0], this.material[0], this.customer[0]);
                pdfmake.createPdf(docDefinition).download();
              });
          });
      });
  }

  view(report: Report) {
    console.log("VIEW REPORT ID: " + report.id);
    let opt = new RequestOptions({
      headers: this.getHeader()
    });
    pdfmake.vfs = pdfFonts.pdfMake.vfs;
    this.http.get('http://localhost:53673/api/Material/' + report.material, opt)
      .subscribe(response => {
        console.log(response);
        this.material = response.json() as Material;
        this.http.get('http://localhost:53673/api/Engine/' + report.engine, opt)
          .subscribe(response => {
            console.log(response);
            this.engine = response.json() as Engine;
            this.http.get('http://localhost:53673/api/Customer/' + report.customer, opt)
              .subscribe(response => {
                this.customer = response.json() as Customer;
                var docDefinition = this.createDocDefinition(report, this.engine[0], this.material[0], this.customer[0]);
                pdfmake.createPdf(docDefinition).open();
              });
          });
      });

  }

  finalDeleteStatement(report: Report) {
    let opt = new RequestOptions({
      headers: this.getHeader()
    });
    const index: number = this.selectedReports.indexOf(report);
    if (index !== -1) {
      this.selectedReports.splice(index, 1);
    }

    this.http.delete('http://localhost:53673/api/Report/' + report.id, opt)
      .subscribe(response => {
        console.log(response);
      });
  }

  createDocDefinition(report: Report, engine: Engine, material: Material, customer: Customer): any {
    console.log("REPORT ID: " + report.id);
    console.log("ENGINE ID: " + engine.id);
    console.log("MATERIAL ID: " + material.id);

    let dincode: string = material.dincode.toString();
    let encode: string = material.dincode.toString();
    if (dincode == '0.0' || dincode == '0') {
      dincode = '-';
    }
    if (encode == '0.0' || encode == '0') {
      encode = '-';
    }

    var docDefinition = {
      content: [
        { text: 'Datasheet Report Nr.' + report.id, style: 'header' },
        { text: 'Basic Information', style: 'sub_header' },
        {
          style: 'table',
          table: {
            body: [
              ['Date: ', report.date],
              ['Projectname: ', report.projectname],
              ['Employeeshort: ', report.employeeshort],
              ['Buildingcomponent: ', report.buildingcomponent],
              ['Workpiececlamping: ', report.workpiececlamping],
              ['Strength: ', report.strength],
              ['Information: ', report.information],
              ['Summary: ', report.summary]
            ]
          }
        },
        { text: 'Customer', style: 'sub_header' },
        {
          style: 'table',
          table: {
            body: [
              ['Name: ', customer.name],
              ['Branch: ', customer.branch],
              ['Contact: ', customer.contactpartner]
            ]
          }
        },
        { text: 'Material', style: 'sub_header' },
        {
          style: 'table',
          layout: 'noBorders',
          table: {
            body: [
              [
                {
                  style: 'table',
                  table: {
                    body: [
                      ['Dincode: ', dincode],
                      ['Encode: ', encode],
                      ['Designation: ', material.designation],
                      ['Hardness: ', material.hardness],
                      ['Material-Group: ', material.materialgroup],
                      ['CrPercent: ', material.crpercent],
                      ['SiPercent: ', material.sipercent],
                      ['NiPercent: ', material.nipercent],
                      ['CuPercent: ', material.cupercent],
                      ['NbPercent: ', material.nbpercent],
                      ['CePercent: ', material.cepercent],
                      ['ZnPercent: ', material.znpercent],
                      ['WPercent: ', material.wpercent]
                    ]
                  }
                },
                {
                  style: 'table',
                  table: {
                    body: [
                      ['MnPercent: ', material.mnpercent],
                      ['VPercent: ', material.vpercent],
                      ['TiPercent: ', material.tipercent],
                      ['CoPercent: ', material.copercent],
                      ['AlPercent: ', material.alpercent],
                      ['BPercent: ', material.bpercent],
                      ['SPercent: ', material.spercent],
                      ['NPercent: ', material.npercent],
                      ['MoPercent: ', material.mopercent],
                      ['PbPercent: ', material.pbpercent],
                      ['FePercent: ', material.fepercent],
                      ['CPercent: ', material.cpercent],
                      ['MgPercent: ', material.mgpercent]
                    ]
                  }
                }
              ]
            ]
          }
        },
        { text: 'Engine', style: 'sub_header' },
        {
          style: 'table',
          layout: 'noBorders',
          table: {
            body: [
              [
                {
                  style: 'table',
                  table: {
                    body: [
                      ['Manufacturer: ', engine.manufacture],
                      ['Type: ', engine.typ],
                      ['Spindle: ', engine.spindel],
                      ['Coolant Outside: ', engine.coolantoutside],
                      ['Coolant Inside: ', engine.coolantinside],
                      ['Power: ', engine.power],
                      ['Rotationalspeed: ', engine.rotationalspeed],
                    ]
                  }
                },
                {
                  style: 'table',
                  table: {
                    body: [
                      ['Feed: ', engine.feed],
                      ['Year of activation: ', engine.activationyear],
                      ['Rate/Hour: ', engine.hourlyrate],
                      ['Programming: ', engine.programming],
                      ['Controlling: ', engine.controlling],
                      ['Path: ', engine.path],
                      ['Picturename: ', engine.picturename],
                    ]
                  }
                }
              ]
            ]
          }
        },
      ],
      styles: {
        header: {
          bold: true,
          margin: 5,
          fontSize: 20,
          alignment: 'center'
        },
        sub_header: {
          padding: 5,
          fontSize: 18,
          alignment: 'left'
        },
        text: {
          fontSize: 14,
          alignment: 'left'
        },
        table: {
          margin: [0, 5, 0, 15]
        },
        tableHeader: {
          bold: true,
          fontSize: 13,
          color: 'black'
        }
      },
      pageSize: 'A4',
      pageOrientation: 'portrait'
    };
    return docDefinition;
  }


}
