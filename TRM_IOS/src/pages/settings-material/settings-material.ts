import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AlertController } from 'ionic-angular';
import { Material } from '../../models/material';

/**
 * Generated class for the SettingsMaterialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings-material',
  templateUrl: 'settings-material.html',
})
export class SettingsMaterialPage {

  allMaterials: Material[];

  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsMaterialPage');
    this.refresh();
  }

  refresh() {
    console.log("--------------allEngines-------------");
    let opt = new RequestOptions({
      headers: this.getHeader()
    });
    this.http.get('http://localhost:53673/api/Material', opt)
      .subscribe(response => {
        console.log("RESPONSE IM HTTP: ");
        console.log(response);
        this.allMaterials = response.json() as Material[];
      });
  }

  getHeader(): Headers {
    const headers = new Headers();
    headers.append('Content-Type', 'text/JSON');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET ,PUT ,POST , PATCH, DELETE, OPTIONS');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');;
    headers.append("Authorization", "Basic " + btoa('admin:nimda'));
    return headers;
  }

  edit(material: Material) {
    let alert = this.alertCtrl.create({
      title: 'Material EDIT',
      inputs: [
        {
          name: 'dincode',
          value: material.dincode+""
        },{
          name: 'encode',
          value: material.encode+""
        },{
          name: 'designation',
          value: material.designation
        },{
          name: 'hardness',
          value: material.hardness
        },{
          name: 'materialgroup',
          value: material.materialgroup
        },{
          name: 'crpercent',
          value: material.crpercent+""
        },{
          name: 'sipercent',
          value: material.sipercent+""
        },{
          name: 'nipercent',
          value: material.nipercent+""
        },{
          name: 'cupercent',
          value: material.cupercent+""
        },{
          name: 'nbpercent',
          value: material.nbpercent+""
        },{
          name: 'cepercent',
          value: material.cepercent+""
        },{
          name: 'znpercent',
          value: material.znpercent+""
        },{
          name: 'wpercent',
          value: material.wpercent+""
        },{
          name: 'mnpercent',
          value: material.mnpercent+""
        },{
          name: 'vpercent',
          value: material.vpercent+""
        },{
          name: 'tipercent',
          value: material.tipercent+""
        },{
          name: 'copercent',
          value: material.copercent+""
        },{
          name: 'alpercent',
          value: material.alpercent+""
        },{
          name: 'bpercent',
          value: material.bpercent+""
        },{
          name: 'spercent',
          value: material.spercent+""
        },{
          name: 'npercent',
          value: material.npercent+""
        },{
          name: 'mopercent',
          value: material.mopercent+""
        },{
          name: 'pbpercent',
          value: material.pbpercent+""
        },{
          name: 'fepercent',
          value: material.fepercent+""
        },{
          name: 'cpercent',
          value: material.cpercent+""
        },{
          name: 'mgpercent',
          value: material.mgpercent+""
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Edit',
          handler: data => {
            let updated: Material = new Material();
            updated.id = material.id;
            updated.dincode = data.dincode;
            updated.encode = data.encode;
            updated.designation = data.designation;
            updated.hardness = data.hardness;
            updated.materialgroup = data.materialgroup;
            updated.crpercent = data.crpercent;
            updated.sipercent = data.sipercent;
            updated.nipercent = data.nipercent;
            updated.cupercent = data.cupercent;
            updated.nbpercent = data.nbpercent;
            updated.cepercent = data.cepercent;
            updated.znpercent = data.znpercent;
            updated.wpercent = data.wpercent;
            updated.mnpercent = data.mnpercent;
            updated.vpercent = data.vpercent;
            updated.tipercent = data.tipercent;
            updated.copercent = data.copercent;
            updated.alpercent = data.alpercent;
            updated.bpercent = data.bpercent;
            updated.spercent = data.spercent;
            updated.npercent = data.npercent;
            updated.mopercent = data.mopercent;
            updated.pbpercent = data.pbpercent;
            updated.fepercent = data.fepercent;
            updated.cpercent = data.cpercent;
            updated.mgpercent = data.mgpercent;

            let opt = new RequestOptions({
              headers: this.getHeader()
            });
            const body = JSON.stringify(updated);
            console.log('BODY: ' + body);
            const headers = this.getHeader();
            this.http.put('http://localhost:53673/api/Material/' + material.id, body, { headers: headers })
              .subscribe(response => {
                console.log(response);
                this.refresh();
              });
          }
        }
      ]
    });
    alert.present();
  }

}
