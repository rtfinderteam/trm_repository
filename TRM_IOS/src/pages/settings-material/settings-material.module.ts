import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingsMaterialPage } from './settings-material';

@NgModule({
  declarations: [
    SettingsMaterialPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsMaterialPage),
  ],
})
export class SettingsMaterialPageModule {}
