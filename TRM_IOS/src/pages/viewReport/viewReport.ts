import { Component, OnInit, ViewChild, QueryList, ElementRef } from '@angular/core';
import { NavController, Card, List } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';


import { Search } from '../../models/search';
import { Report } from '../../models/report';


import { Material } from '../../models/material';
import { Engine } from '../../models/engine';


@Component({
    selector: 'page-viewReport',
    templateUrl: 'viewReport.html'
})
export class ViewReport implements OnInit {
    materialGroup: string = "";
    description: string = "";
    dincode: string = "";
    encode: string = "";
    strenght: string = "";
    hardness: string = "";

    manufacturer: string = "";
    model: string = "";
    spindle: string = "";

    company: string = "";
    branch: string = "";

    employeeshort: string = "";


    hardnesses: string[];
    spindles: string[];
    suppliers: string[];
    groups: string[];
    materialgroups: string[];
    branches: string[];
    usages: string[];
    competitions: string[];
    initials: string[];
    strengths: string[];

    selectedReports: Report[] = [];
    params: Search = new Search;

    data: string[];
    showCard = true;

    frequentlyUsed: string[] = [];
    constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, private storage: Storage) {
    }

    ngOnInit() {
        console.log("-------------ONINIT VIEW REPORT----------------")
        this.fillAllComboBoxes();
    }

    getHeader(): Headers{
        const headers = new Headers();
        headers.append('Content-Type', 'text/JSON');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        headers.append('Access-Control-Allow-Headers', 'Content-Type');;
        headers.append("Authorization", "Basic "+btoa('admin:nimda'));
        return headers;
    }

    searchReports() {
        console.log("--------------searchReports-------------");
        this.fillParams();
        console.log(JSON.stringify(this.params));
        this.searchCall();
    }

    searchCall() {
        const body = JSON.stringify(this.params);
        
        this.http.post('http://localhost:53673/api/search/getReport', body, { headers: this.getHeader() })
            .subscribe(response => {
                console.log(response);
                this.selectedReports = response.json() as Report[];
                this.navCtrl.push('searchResults', {selectedReports:this.selectedReports});
            });
    }

    fillParams() {
        console.log("FILL PARAMS");
        this.params.S_Material.materialgroup = (this.materialGroup == "Select a value") ? "" : this.materialGroup;
        this.params.S_Material.designation = (this.description == "Select a value") ? "" : this.description;
        this.params.S_Material.dincode = (this.dincode == "Select a value") ? "" : this.dincode;
        this.params.S_Material.encode = (this.encode == "Select a value") ? "" : this.encode;
        this.params.S_Material.hardness = (this.hardness == "Select a value") ? "" : this.hardness;
        this.params.S_Engine.manufacture = (this.manufacturer == "Select a value") ? "" : this.manufacturer;
        this.params.S_Engine.typ = (this.model == "Select a value") ? "" : this.model;
        this.params.S_Engine.spindel = (this.spindle == "Select a value") ? "" : this.spindle;
        this.params.S_Customer.name = (this.company == "Select a value") ? "" : this.company;
        this.params.S_Customer.branch = (this.branch == "Select a value") ? "" : this.branch;
        this.params.S_Report.employeeshort = (this.employeeshort == "Select a value") ? "" : this.employeeshort;
        this.params.S_Report.strength = (this.strenght == "Select a value") ? "" : this.strenght;
        console.log("FILL PARAMS END");
    }

    fillAllComboBoxes() {
        console.log("--------------hardness-------------");
        let opt = new RequestOptions({
            headers: this.getHeader()
          });
        this.http.get('http://localhost:53673/api/searchboxes/getHardness', opt)
            .subscribe(response => {
                console.log(response);
                this.hardnesses = response.json() as string[];
            });

        console.log("--------------spindles-------------");
        this.http.get('http://localhost:53673/api/searchboxes/getSpindle', opt)
            .subscribe(response => {
                console.log(response);
                this.spindles = response.json() as string[];
            });

        console.log("--------------suppliers-------------");
        this.http.get('http://localhost:53673/api/searchboxes/getSupplier', opt)
            .subscribe(response => {
                console.log(response);
                this.suppliers = response.json() as string[];
            });

        console.log("--------------groups-------------");
        this.http.get('http://localhost:53673/api/searchboxes/getGroup', opt)
            .subscribe(response => {
                console.log(response);
                this.groups = response.json() as string[];
            });

        console.log("--------------materialgroups-------------");
        this.http.get('http://localhost:53673/api/searchboxes/getMaterialGroup', opt)
            .subscribe(response => {
                console.log(response);
                this.materialgroups = response.json() as string[];
            });

        console.log("--------------branches-------------");
        this.http.get('http://localhost:53673/api/searchboxes/getBranch', opt)
            .subscribe(response => {
                console.log(response);
                this.branches = response.json() as string[];
            });

        console.log("--------------usages-------------");
        this.http.get('http://localhost:53673/api/searchboxes/getUsage', opt)
            .subscribe(response => {
                console.log(response);
                this.usages = response.json() as string[];
            });

        console.log("--------------competitions-------------");
        this.http.get('http://localhost:53673/api/searchboxes/getCompetition', opt)
            .subscribe(response => {
                console.log(response);
                this.competitions = response.json() as string[];
            });

        console.log("--------------initals-------------");
        this.http.get('http://localhost:53673/api/searchboxes/getInitials', opt)
            .subscribe(response => {
                console.log(response);
                this.initials = response.json() as string[];
            });

        console.log("-------------strength-----------------");
        this.http.get('http://localhost:53673/api/searchboxes/getStrength', opt)
            .subscribe(response => {
                console.log(response);
                this.strengths = response.json() as string[];
            })

    }


}