import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingsCustomerPage } from './settings-customer';

@NgModule({
  declarations: [
    SettingsCustomerPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsCustomerPage),
  ],
})
export class SettingsCustomerPageModule {}
