import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Customer } from '../../models/customer';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the SettingsCustomerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings-customer',
  templateUrl: 'settings-customer.html',
})
export class SettingsCustomerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public alertCtrl: AlertController) {
  }

  allCustomer: Customer[];

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsCustomerPage');
    this.refresh();
  }

  refresh(){
    console.log("--------------allCustomer-------------");
    let opt = new RequestOptions({
      headers: this.getHeader()
    });
    this.http.get('http://localhost:53673/api/Customer', opt)
      .subscribe(response => {
        console.log("RESPONSE IM HTTP: ");
        console.log(response);
        this.allCustomer = response.json() as Customer[];
      });
  }

  getHeader(): Headers {
    const headers = new Headers();
    headers.append('Content-Type', 'text/JSON');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET ,PUT ,POST , PATCH, DELETE, OPTIONS');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');;
    headers.append("Authorization", "Basic " + btoa('admin:nimda'));
    return headers;
  }

  edit(customer: Customer) {
    let alert = this.alertCtrl.create({
      title: 'Customer EDIT',
      inputs: [
        {
          name: 'name',
          placeholder: customer.name,
          value: customer.name
        },
        {
          name: 'branch',
          placeholder: customer.branch,
          value: customer.branch
        },
        {
          name: 'contactpartner',
          placeholder: customer.contactpartner,
          value: customer.contactpartner
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Edit',
          handler: data => {
            let updated: Customer = new Customer();
            updated.id = customer.id; 
            updated.name = data.name;
            updated.branch = data.branch;
            updated.contactpartner = data.contactpartner;

            let opt = new RequestOptions({
              headers: this.getHeader()
            });
            const body = JSON.stringify(updated);
            console.log('BODY: '+body);
            const headers = this.getHeader();
            this.http.put('http://localhost:53673/api/Customer/'+customer.id, body, { headers: headers })
                .subscribe(response => {
                    console.log(response);
                    this.refresh();
                });
          }
        }
      ]
    });
    alert.present();
  }
}
