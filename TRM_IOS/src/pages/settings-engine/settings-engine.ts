import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AlertController } from 'ionic-angular';
import { Engine } from '../../models/engine';

/**
 * Generated class for the SettingsEnginePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings-engine',
  templateUrl: 'settings-engine.html',
})
export class SettingsEnginePage {

  allEngines: Engine[] = [];

  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsEnginePage');
    this.refresh();
  }

  refresh() {
    console.log("--------------allEngines-------------");
    let opt = new RequestOptions({
      headers: this.getHeader()
    });
    this.http.get('http://localhost:53673/api/Engine', opt)
      .subscribe(response => {
        console.log("RESPONSE IM HTTP: ");
        console.log(response);
        this.allEngines = response.json() as Engine[];
      });
  }

  getHeader(): Headers {
    const headers = new Headers();
    headers.append('Content-Type', 'text/JSON');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET ,PUT ,POST , PATCH, DELETE, OPTIONS');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');;
    headers.append("Authorization", "Basic " + btoa('admin:nimda'));
    return headers;
  }

  edit(engine: Engine) {
    let alert = this.alertCtrl.create({
      title: 'Engine EDIT',
      inputs: [
        {
          name: 'manufacture',
          value: engine.manufacture
        },
        {
          name: 'typ',
          value: engine.typ
        },
        {
          name: 'spindel',
          value: engine.spindel
        },
        {
          name: 'coolantout',
          value: engine.coolantoutside + ""
        },


        {
          name: 'coolantin',
          value: engine.coolantinside + ""
        },
        {
          name: 'power',
          value: engine.power+""
        },

        {
          name: 'rotationalspeed',
          value: engine.rotationalspeed+""
        },

        {
          name: 'feed',
          value: engine.feed+""
        },

        {
          name: 'activationyear',
          value: engine.activationyear+""
        },

        {
          name: 'hourlyrate',
          value: engine.hourlyrate+""
        },

        {
          name: 'programming',
          value: engine.programming
        },
        {
          name: 'controlling',
          value: engine.controlling
        },
        {
          name: 'path',
          value: engine.path
        },
        {
          name: 'picturename',
          value: engine.picturename
        }

      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Edit',
          handler: data => {
            let updated: Engine = new Engine();
            updated.id = engine.id;
            updated.manufacture = data.manufacture,
            updated.typ = data.typ,
            updated.spindel = data.typ,
            updated.coolantoutside = data.coolantout,
            updated.coolantinside = data.coolantin,
            updated.power = data.power,
            updated.rotationalspeed = data.rotationalspeed,
            updated.feed = data.feed,
            updated.activationyear = data.activationyear,
            updated.hourlyrate = data.hourlyrate,
            updated.programming = data.programming,
            updated.controlling = data.controlling,
            updated.path = data.path,
            updated.picturename = data.picturename

            let opt = new RequestOptions({
              headers: this.getHeader()
            });
            const body = JSON.stringify(updated);
            console.log('BODY: ' + body);
            const headers = this.getHeader();
            this.http.put('http://localhost:53673/api/Engine/' + engine.id, body, { headers: headers })
              .subscribe(response => {
                console.log(response);
                this.refresh();
              });
          }
        }
      ]
    });
    alert.present();
  }

}
