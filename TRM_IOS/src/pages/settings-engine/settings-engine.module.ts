import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingsEnginePage } from './settings-engine';

@NgModule({
  declarations: [
    SettingsEnginePage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsEnginePage),
  ],
})
export class SettingsEnginePageModule {}
