export class Search{
    S_Material: S_Material = new S_Material;
    S_Engine: S_Engine = new S_Engine;
    S_Customer: S_Customer = new S_Customer;
    S_Report: S_Report = new S_Report;    
}

class S_Material{
    materialgroup: string;
    designation: string;
    dincode: string;
    encode: string;
    hardness: string;   
}

class S_Engine{
    manufacture: string;
    typ: string;
    spindel: string;
}

class S_Customer {
    name: string;
    branch: string;
}

class S_Report {
    strength: string;
    employeeshort: string;
}