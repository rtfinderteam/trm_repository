export class Engine {
    id: number;
    manufacture: string;
    typ: string;
    spindel: string;
    coolantoutside: number;
    coolantinside: number;
    power: number;
    rotationalspeed: number
    feed: number;
    activationyear: number;
    hourlyrate: number;
    programming: string;
    controlling: string;
    path: string;
    picturename: string;
}