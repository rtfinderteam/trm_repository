export class Toollist{
    id: number;
    manufacture_picture: number;
    manufacture: string;
    iscompentitor: boolean;
    toolholder: string;
    toolid: string;
    insert: string;
    coating: string;
    flutes: number;
    cuttingedges: number;
    diameter: number;
    necklength: number;
    overhang: number;
    processing: string;
    report: number;
}