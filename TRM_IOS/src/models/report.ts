export class Report{
    id: number;
    projectname: string;
    employeeshort: string;
    date: string;
    buildingcomponent: string;
    workpiececlamping: string;
    strength: string;
    information: string;
    summary: string;
    engine: number;
    customer: number;
    material: number;
}