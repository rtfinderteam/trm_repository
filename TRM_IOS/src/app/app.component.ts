import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CreateReport } from "../pages/createReport/createReport"
import { ViewReport } from '../pages/viewReport/viewReport';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { HomePage } from '../pages/home/home';
import { App } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { SettingsCustomerPage } from '../pages/settings-customer/settings-customer';
import { SettingsMaterialPage } from '../pages/settings-material/settings-material';
import { SettingsEnginePage } from '../pages/settings-engine/settings-engine';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  pages:Array<{title: string, component:any}>;

  constructor(private app: App, private menu: MenuController, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,  public toastCtrl: ToastController) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  createReportButton(){
    this.menu.close();
    this.app.getActiveNav().push(CreateReport);
  }

  viewReportButton(){
    this.menu.close();
    this.app.getActiveNav().push(ViewReport);
  }

  settingsCustomer(){
    this.menu.close();
    this.app.getActiveNav().push(SettingsCustomerPage);
  }

  settingsMaterial(){
    this.menu.close();
    this.app.getActiveNav().push(SettingsMaterialPage);
  }

  settingsEngine(){
    this.menu.close();
    this.app.getActiveNav().push(SettingsEnginePage);
  }
}

