import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { CreateReport } from '../pages/createReport/createReport';
import { ViewReport } from '../pages/viewReport/viewReport';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Storage } from '@ionic/storage';
import { IonicStorageModule } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { SettingsCustomerPage } from '../pages/settings-customer/settings-customer';
import { SettingsEnginePage } from '../pages/settings-engine/settings-engine';
import { SettingsMaterialPage } from '../pages/settings-material/settings-material';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CreateReport,
    ViewReport,
    SettingsCustomerPage,
    SettingsEnginePage,
    SettingsMaterialPage
  ],
  imports: [
    BrowserModule,
    HttpModule, 
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CreateReport,
    ViewReport,
    SettingsCustomerPage,
    SettingsEnginePage,
    SettingsMaterialPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePipe,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
