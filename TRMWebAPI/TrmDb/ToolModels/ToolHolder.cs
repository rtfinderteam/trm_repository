﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.ToolModels
{
    public class ToolHolder
    {
        public int Id { get; set; }
        public double InsideDiameter_d { get; set; }
        public string FactoryStandard { get; set; }
        public double Diameter_D1 { get; set; }
        public double Diameter_D { get; set; }
        public double Diameter_D2 { get; set; }
        public double Diameter_D3 { get; set; }
        public double Diameter_D4 { get; set; }
        public double Diameter_D5 { get; set; }
        public double FreeLengthL { get; set; }
        public double FreeLengthL1 { get; set; }
        public double FreeLengthL2 { get; set; }
        public double FreeLengthL3 { get; set; }
        public double FreeLengthL4 { get; set; }
        public double FreeLength_A { get; set; }
        public double FreeLength_l { get; set; }
        public double FreeLength_l1 { get; set; }
        public double FreeLength_l2 { get; set; }
        public double FreeLength_l3 { get; set; }
        public double FreeLength_H1 { get; set; }
        public double NeckAngleTheta { get; set; }
        public double PassDiameterB { get; set; }
        public double MaxClampingLength_H { get; set; }
        public double MaxClampingLength_l { get; set; }
        public double MinClampingLength { get; set; }
        public double LengthCompensation { get; set; }
        public string Spanner { get; set; }
        public string RollerSpanner { get; set; }
        public string TorqueSpanner { get; set; }
        public string ClampingSpanner { get; set; }
        public string KexKey { get; set; }
        public string AdjustmentScrew { get; set; }
        public string TappingRange { get; set; }
        public string FineBalancing { get; set; }
        public double RunoutAccuracy { get; set; }
        public double DiameterC1 { get; set; }
        public double DiameterC2 { get; set; }
        public double ThreadHoleW1 { get; set; }
        public string Thread { get; set; }
        public double TSlotHeightK1 { get; set; }
        public string Screw_G1 { get; set; }
        public string FixedBlockPosition_S { get; set; }
        public string GearRatio { get; set; }
        public double MaxSpindleSpeed { get; set; }
        public double AdjustMin { get; set; }
        public double AdjustMax { get; set; }
        public double PullStud_M { get; set; }
        public string CapNut { get; set; }
        public string CoolantTube { get; set; }
        public string WrenchCoolantTube { get; set; }

    }
}
