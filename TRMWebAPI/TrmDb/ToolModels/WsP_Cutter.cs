﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.ToolModels
{
    public class WsP_Cutter : Tool
    {
        public double WspC_EffAngleDegree { get; set; }
        public string WspC_ModularTorque { get; set; }
        public double WspC_ArborDiameter_d { get; set; }
        public double WspC_ArborLength_l { get; set; }
        public string WspC_Hole_d1 { get; set; }
        public string WspC_DistanceFlatWrench_T { get; set; }
        public string WspC_ClampingScrew { get; set; }
        public string WspC_ClampingScrew2 { get; set; }
        public string WspC_ClampingClaw { get; set; }
        public string WspC_BasePlate { get; set; }
        public string WspC_BasePlateScrew { get; set; }
        public string WspC_TorqueSpanner { get; set; }
        public string WspC_TorqueSpanner2 { get; set; }
        public string WspC_ClampingTorque { get; set; }
        public string WspC_Torxkey { get; set; }
        public string WspC_Torxkey2 { get; set; }
        public double WspC_CuttingLength_L1 { get; set; }
        public double WspC_CuttingLengthL2 { get; set; }
        public double WspC_DiameterD1 { get; set; }
        public double WspC_Diameter_Db { get; set; }
        public double WspC_Diameter_d1 { get; set; }
        public string WspC_ShankDs { get; set; }
        public string WspC_Shank_d { get; set; }
        public double WspC_ShankLength_ls { get; set; }
        public double WspC_TotalLength_L { get; set; }
        public double WspC_TotalLength_L6 { get; set; }
        public int WspC_FluteZ { get; set; }
        public string WspC_Thread_MD { get; set; }
        public string WspC_Thread_TS { get; set; }
        public string WspC_TSlotWidth_a { get; set; }
        public string WspC_TSlotDepth_b { get; set; }
        public double WspC_CuttingDiameter_Dc { get; set; }
        public double WspC_CuttingDiameter_D { get; set; }
        public double WspC_CuttingDiameter_Dc1 { get; set; }
        public double WspC_CuttingDiameter_d2 { get; set; }
        public double WspC_Radius_R { get; set; }
        public double WspC_MaxCuttingDepth_ae { get; set; }
        public double WspC_CuttingWidth_W { get; set; }
        public double WspC_AngleThetaDegree { get; set; }
        public double WspC_AngleAlphaDegree { get; set; }
        public double WspC_FreeLength_l1 { get; set; }
        public double WspC_FreeLength_Lf { get; set; }
        public double WspC_CuttingLength_l2 { get; set; }
    }
}
