﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.ToolModels
{
    public class VHM_Cutter : Tool
    {
        //Vhm Cutter stuff
        public string VhmC_HelixAngle { get; set; }
        public string VhmC_ThreadPitch_p { get; set; }//former vhmc_threatpitch_p
        public string VhmC_ThreadPitch_g1 { get; set; }//former vhmc_threatpitch_g_1
        public string VhmC_CuttingDirection { get; set; }
        public double? VhmC_CuttingDiameterD { get; set; }
        public double? VhmC_RadiusR { get; set; }
        public string VhmC_AngleThetaDegree { get; set; } //former vhmc_angletheatadegree
        public double? VhmC_FreeLength_l1 { get; set; }
        public double? VhmC_FreeLength_l2 { get; set; }
        public double? VhmC_FreeLength_l3 { get; set; }
        public double? VhmC_CuttingLength_l { get; set; } //former vhmC_cuttingLenth_l
        public double? VhmC_Diameter_d1 { get; set; }
        public double? VhmC_Diameter_d2 { get; set; }
        public string VhmC_AngleGammaDegree { get; set; }
        public string VhmC_Shank_d { get; set; }
        public double? VhmC_TotalLength_L { get; set; }
        public double? VhmC_Flute_Z { get; set; }
        public string VhmC_EffAngleAlphaDegree { get; set; }
        public string VhmC_AngleBetaDegree { get; set; }
        public string VhmC_PossibleWorklength30min { get; set; }
        public string VhmC_PossibleWorklength1Degree { get; set; }
        public string VhmC_PossibleWorklength1Degree30min { get; set; }
        public string VhmC_PossibleWorklength2Degree { get; set; }
        public string VhmC_PossibleWorklength3Degree { get; set; }

    }
}
