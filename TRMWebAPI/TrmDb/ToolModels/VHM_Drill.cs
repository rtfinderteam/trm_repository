﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.ToolModels
{
    public class VHM_Drill : Tool
    {
        //VHM Drill stuff

        public double? VhmD_ShankDiameter_Ds { get; set; }
        public double? VhmD_ShankDiameter_D2 { get; set; }
        public string VhmD_ShankDesign_HA { get; set; }
        public string VhmD_ShankdDesignHB { get; set; }
        public double? VhmD_DrillDepth_L3 { get; set; }
        public string VhmD_DrillDiameterTolerance { get; set; }
        public int? VhmD_Flutes { get; set; }
        public string VhmD_DinNorm { get; set; }
        public double? VhmD_CuttingDiameter_d1 { get; set; }
        public double? VhmD_CuttingDiameterD1 { get; set; } 
        public double? VhmD_CuttingDiameter_Dc { get; set; }
        public double? VhmD_Radius_r { get; set; }
        public double? VhmD_FluteLength_l2 { get; set; }
        public double? VhmD_FluteLength_l { get; set; }
        public string VhmD_MargindesignNumber { get; set; }
        public string VhmD_MargindesignLength { get; set; }
        public string VhmD_HelixAngle { get; set; }
        public double? VhmD_TotalLength_l1 { get; set; }
        public double? VhmD_TotalLength_L { get; set; }
        public double VhmD_TotalLengthL1 { get; set; } 
        public string VhmD_PointAngleSigma { get; set; }
        public string VhmD_Length_L5 { get; set; }
        public double? VhmD_ShankdDiameter_d2 { get; set; }
    }
}
