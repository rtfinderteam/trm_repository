﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.ToolModels
{
    public abstract class Tool
    {
        public Tool()
        {

        }
        public int Id { get; set; }
        public string ItemNumber { get; set; }
        public string Designation { get; set; } //former desigantion
        public double Price { get; set; }
        public int Stock { get; set; }
        public string ProductType { get; set; }
        public string ProductGroup { get; set; }
        public string Product { get; set; } //whats this? sounds like other table...
        public string Supplier { get; set; }//same thing, other table?
        public string ToolMaterial { get; set; }
        public string Coating { get; set; }
        //public string ClassType { get; set; } //nur in pgAdmin-->wahrscheinlich wegen is-a





    }
}
