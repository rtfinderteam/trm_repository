﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.ToolModels
{
    public class Reamer : Tool
    {
        public double Rm_CuttingDiameter_D { get; set; }
        public double Rm_EffLength_l2 { get; set; }
        public double Rm_FreeLength_b { get; set; }
        public double Rm_LeadLength { get; set; }
        public string Rm_LeadForm { get; set; }
        public string Rm_Shank_d { get; set; }
        public string Rm_ShankLength_c { get; set; }
        public double Rm_TotalLength_L { get; set; }
        public int Rm_FluteZ { get; set; }
        public double Rm_ReamerDepth { get; set; }
        public string Rm_Handscrew { get; set; }
        public string Rm_Key { get; set; }
        public string Rm_HelixAngle { get; set; }

    }
}
