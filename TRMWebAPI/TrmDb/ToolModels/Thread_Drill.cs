﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.ToolModels
{
    public class Thread_Drill : Tool
    {
        public double Td_Undercut_d2 { get; set; }
        public string Td_AngleGammaDegree { get; set; }
        public string Td_ShankLength_ls { get; set; }
        public double Td_TotalLength_l { get; set; }
        public double Td_ThreadSize { get; set; }
        public double Td_CuttingDiameter_d1 { get; set; }
        public string Td_ThreadPitch_p { get; set; }
        public double Td_TotalLength_l1 { get; set; }
        public double Td_CuttingLength_l2 { get; set; }
        public double Td_ShankDiameter_d2 { get; set; }
        public string Td_Square { get; set; }
        public int Td_FluteZ { get; set; }
        public string Td_Corehole { get; set; }
        public string Td_HelixAngle { get; set; }
        public string Td_Design { get; set; }
        public string Td_Tolerance { get; set; }
        public string ThreadType { get; set; }
        public string Td_DinNorm { get; set; }
        public string Td_LeadForm { get; set; }
    }
}
