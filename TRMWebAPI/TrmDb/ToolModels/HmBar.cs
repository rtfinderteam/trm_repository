﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.ToolModels
{
    public class HmBar
    {
        public int Id { get; set; }
        public double ShankDiameter_Ds { get; set; }
        public double FreeLength_l1 { get; set; }
        public double Diameter_D1 { get; set; }
        public double PassDiameter_D2 { get; set; }
        public double NeckAngleTheta { get; set; }
        public double TotalLength_L { get; set; }
        public string Thread { get; set; }
    }
}
