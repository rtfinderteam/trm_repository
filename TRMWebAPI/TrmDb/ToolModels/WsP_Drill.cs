﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.ToolModels
{
    public class WsP_Drill : Tool
    {

        public string WspD_ShankdDesignABS { get; set; }
        public double WspD_DrillDepth { get; set; }
        public double WspD_DrillDiameterTolerance { get; set; }
        public string WspD_Wormscrew { get; set; }
        public string WspD_Capscrew { get; set; }
        public string WspD_ClampingScrew { get; set; }
        public string WspD_Hexkey { get; set; }
        public string WspD_Torxkey { get; set; }
        public string WspD_TorqueSpanner { get; set; }
        public string WspD_ClampingTorque { get; set; }
        public double WspD_CuttingDiameter_D { get; set; }
        public double WspD_CuttingDiameter_Dc { get; set; }
        public double WspD_DrillDepth_l { get; set; }
        public double WspD_DrillDepth_N { get; set; }
        public string WspD_Overhang_l2 { get; set; }
        public string WspD_Overhang_Lf { get; set; }
        public double WspD_TotalLength_L { get; set; }
        public double WspD_ShankDiameter_d { get; set; }
        public double WspD_ShankDiameter_Ds { get; set; }
        public double WspD_FlangeDiameter_d3 { get; set; }
        public double WspD_FlangeDiameter_D { get; set; } 
        public string WspD_ShankDesign_HB { get; set; }
        public string WspD_ShankDesign_HE { get; set; }


    }
}
