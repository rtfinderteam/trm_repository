﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.ToolModels
{
    public class Collet
    {
        public int Id { get; set; }
        public string FactoryStandardType { get; set; } 
        public string ProductAlloction { get; set; } 
        public double OutsideDiameter_D { get; set; }
        public double OutsideDiameter_D1 { get; set; }
        public double OutsideDiameter_D2 { get; set; }
        public double OutsideDiameter_D3 { get; set; }
        public double InsideDiameter_d { get; set; } 
        public double InsideDiameter_D2 { get; set; }
        public string RunoutAccuracy { get; set; }
        public double Length_L { get; set; }
        public double Length_L1 { get; set; }
        public double Length_L2 { get; set; }
        public string HoldingCapacity { get; set; }
        public string Square{ get; set; }
        public double MaxClampingLength { get; set; }
        public double MinClampingLength { get; set; }
    }
}
