﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.ToolModels
{
    public class PatternData
    {
        public int Id { get; set; }
        public virtual Tool Tool { get; set; }
        public virtual ToolHolder ToolHolder { get; set; }
        public virtual Collet Collet { get; set; }
        public virtual HmBar HmBar { get; set; }
    }
}
