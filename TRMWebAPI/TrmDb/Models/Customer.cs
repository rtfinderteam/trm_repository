﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrmDb.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Branch { get; set; }
        public string Contactpartner { get; set; }
    }
}