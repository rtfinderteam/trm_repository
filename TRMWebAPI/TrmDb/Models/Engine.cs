﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrmDb.Models
{
    public class Engine 
    {
        public int Id { get; set; }
        public string Manufacturer { get; set; }
        public string Type { get; set; }
        public string Spindle { get; set; } 
        public double Coolantoutside { get; set; }
        public double Coolantinside { get; set; }
        public int Power { get; set; }
        public int Rotationalspeed { get; set; }
        public int Feed { get; set; }
        public int Activationyear { get; set; }
        public int Hourlyrate { get; set; }
        public string Programming { get; set; }
        public string Controlling { get; set; }
        public string EnginePicturename { get; set; }

    }
}