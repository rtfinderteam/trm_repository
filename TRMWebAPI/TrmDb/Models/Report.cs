﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TrmDb.Models
{
    public class Report
    {
        public Report()
        {
            this.Date = new DateTime();
        }
        public int Id { get; set; }
        public string Projectname { get; set; }
        public string Employeeshort { get; set; }
        [Column(TypeName="datetime2")]
        public DateTime? Date { get; set; } = new DateTime();
        public string Buildingcomponent { get; set; }
        public string Workpiececlamping { get; set; }
        public string Strength { get; set; }
        public string EngineInfo { get; set; }
        public string MaterialInfo { get; set; }
        public string ClampingInfo { get; set; }
        public virtual Engine Engine { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Material Material { get; set; }
        public string ReportPicturename1 { get; set; }
        public string ReportPicturename2 { get; set; }
        public string ReportPicturename3 { get; set; }
    }
}