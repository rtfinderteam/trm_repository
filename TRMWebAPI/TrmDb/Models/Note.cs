﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrmDb.Models
{
    public class Note
    {
        public Note()
        {
            Date = new DateTime();
        }
        public int Id { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Date { get; set; } = new DateTime();
        public string Employeeshort { get; set; }
        public string Text { get; set; }
    }
}
