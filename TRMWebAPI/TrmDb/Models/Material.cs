﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrmDb.Models
{
    public class Material
    {
        public int Id { get; set; }
        public string Dincode { get; set; }
        public string Encode { get; set; }
        public string Designation { get; set; }
        public string HardnessHRC { get; set; }
        public string HardnessHB { get; set; }
        public string Materialgroup { get; set; }
        public double CRpercent { get; set; }
        public double SIpercent { get; set; }
        public double NIpercent { get; set; }
        public double CUpercent { get; set; }
        public double NBpercent { get; set; }
        public double CEpercent { get; set; }
        public double ZNpercent { get; set; }
        public double Wpercent { get; set; }
        public double MNpercent { get; set; }
        public double Vpercent { get; set; }
        public double TIpercent { get; set; }
        public double COpercent { get; set; }
        public double ALpercent { get; set; }
        public double Bpercent { get; set; }
        public double Spercent { get; set; }
        public double Npercent { get; set; }
        public double MOpercent { get; set; }
        public double PBpercent { get; set; }
        public double FEpercent { get; set; }
        public double Cpercent { get; set; }
        public double MGpercent { get; set; }


    }
}