﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrmDb.ToolModels;

namespace TrmDb.Models
{
    public class ToollistItem 
    {
        public int Id { get; set; }
        public string ToolPicturename { get; set; }
        public string Manufacturer { get; set; } 
        public bool IsCompetitor { get; set; }
        public string Toolholder { get; set; }
        public string ToolId { get; set; }
        public string Insert { get; set; }
        public string Coating { get; set; }
        public int? Flutes { get; set; }
        public int? Cuttingedges { get; set; }
        public int? Diameter { get; set; }
        public int? Necklength { get; set; }
        public int? Overhang { get; set; }
        public string Processing { get; set; }
        public virtual Report Report { get; set; }
    }
}