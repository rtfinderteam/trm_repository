﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrmDb.Models
{
    public class Toolvaluepage
    {
        public int Id { get; set; }
        public string Processing { get; set; }
        public string Noise { get; set; }
        public string Cooling { get; set; }
        public int? Operationtime { get; set; }
        public string Ofwear { get; set; }
        public string Wearintensity { get; set; }
        public string Strategy { get; set; }
        public string MaterialRemovalrate { get; set; }
        public double? RampingAngle { get; set; }
        public string Application { get; set; }
        public double? ActualCuttingspeed { get; set; }
        public double? ActualSpindlespeed { get; set; }
        public double? ActualFeedPerMinute { get; set; }
        public double? ActualFeedPerTooth { get; set; }
        public double? ActualFeedPerRotation { get; set; } //former revolution
        public double? ActualAxial { get; set; }
        public double? ActualRadial { get; set; }
        public double? ExpectedCuttingspeed { get; set; }
        public double? ExpectedSpindlespeed { get; set; }
        public double? ExpectedFeedPerMinute { get; set; }
        public double? ExpectedFeedPerTooth { get; set; }
        public double? ExpectedFeedPerRotation { get; set; } //former revolution
        public double? ExpectedAxial { get; set; }
        public double? ExpectedRadial { get; set; }
        public int? Resolution { get; set; } //former resulotion
        public bool Hasavideo { get; set; }
        public string Applicationtype { get; set; } //former aplicationtype
        public string SpindleLoadingCapacity { get; set; }
        public string ToolLife { get; set; }
        public string ToolInfo { get; set; }
        public virtual ToollistItem ToollistItem { get; set; } //former Tool_list
        public string ToolvaluepagePicturename1 { get; set; }
        public string ToolvaluepagePicturename2 { get; set; }
        public string ToolvaluepagePicturename3 { get; set; }
        public string ToolvaluepagePicturename4 { get; set; }
        public string ToolvaluepagePicturename5 { get; set; }
    }
}