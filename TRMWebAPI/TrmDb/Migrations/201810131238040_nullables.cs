namespace TrmDb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullables : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ToollistItems", "Flutes", c => c.Int());
            AlterColumn("dbo.ToollistItems", "Cuttingedges", c => c.Int());
            AlterColumn("dbo.ToollistItems", "Diameter", c => c.Int());
            AlterColumn("dbo.ToollistItems", "Necklength", c => c.Int());
            AlterColumn("dbo.ToollistItems", "Overhang", c => c.Int());
            AlterColumn("dbo.Toolvaluepages", "Operationtime", c => c.Int());
            AlterColumn("dbo.Toolvaluepages", "RampingAngle", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ActualCuttingspeed", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ActualSpindlespeed", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ActualFeedPerMinute", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ActualFeedPerTooth", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ActualFeedPerRotation", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ActualAxial", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ActualRadial", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ExpectedCuttingspeed", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ExpectedSpindlespeed", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ExpectedFeedPerMinute", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ExpectedFeedPerTooth", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ExpectedFeedPerRotation", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ExpectedAxial", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "ExpectedRadial", c => c.Double());
            AlterColumn("dbo.Toolvaluepages", "Resolution", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Toolvaluepages", "Resolution", c => c.Int(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ExpectedRadial", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ExpectedAxial", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ExpectedFeedPerRotation", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ExpectedFeedPerTooth", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ExpectedFeedPerMinute", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ExpectedSpindlespeed", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ExpectedCuttingspeed", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ActualRadial", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ActualAxial", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ActualFeedPerRotation", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ActualFeedPerTooth", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ActualFeedPerMinute", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ActualSpindlespeed", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "ActualCuttingspeed", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "RampingAngle", c => c.Double(nullable: false));
            AlterColumn("dbo.Toolvaluepages", "Operationtime", c => c.Int(nullable: false));
            AlterColumn("dbo.ToollistItems", "Overhang", c => c.Int(nullable: false));
            AlterColumn("dbo.ToollistItems", "Necklength", c => c.Int(nullable: false));
            AlterColumn("dbo.ToollistItems", "Diameter", c => c.Int(nullable: false));
            AlterColumn("dbo.ToollistItems", "Cuttingedges", c => c.Int(nullable: false));
            AlterColumn("dbo.ToollistItems", "Flutes", c => c.Int(nullable: false));
        }
    }
}
