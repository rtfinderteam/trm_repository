namespace TrmDb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Collets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FactoryStandardType = c.String(),
                        ProductAlloction = c.String(),
                        OutsideDiameter_D = c.Double(nullable: false),
                        OutsideDiameter_D1 = c.Double(nullable: false),
                        OutsideDiameter_D2 = c.Double(nullable: false),
                        OutsideDiameter_D3 = c.Double(nullable: false),
                        InsideDiameter_d = c.Double(nullable: false),
                        InsideDiameter_D2 = c.Double(nullable: false),
                        RunoutAccuracy = c.String(),
                        Length_L = c.Double(nullable: false),
                        Length_L1 = c.Double(nullable: false),
                        Length_L2 = c.Double(nullable: false),
                        HoldingCapacity = c.String(),
                        Square = c.String(),
                        MaxClampingLength = c.Double(nullable: false),
                        MinClampingLength = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Branch = c.String(),
                        Contactpartner = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Engines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Manufacturer = c.String(),
                        Type = c.String(),
                        Spindle = c.String(),
                        Coolantoutside = c.Double(nullable: false),
                        Coolantinside = c.Double(nullable: false),
                        Power = c.Int(nullable: false),
                        Rotationalspeed = c.Int(nullable: false),
                        Feed = c.Int(nullable: false),
                        Activationyear = c.Int(nullable: false),
                        Hourlyrate = c.Int(nullable: false),
                        Programming = c.String(),
                        Controlling = c.String(),
                        EnginePicturename = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HmBars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShankDiameter_Ds = c.Double(nullable: false),
                        FreeLength_l1 = c.Double(nullable: false),
                        Diameter_D1 = c.Double(nullable: false),
                        PassDiameter_D2 = c.Double(nullable: false),
                        NeckAngleTheta = c.Double(nullable: false),
                        TotalLength_L = c.Double(nullable: false),
                        Thread = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Materials",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Dincode = c.String(),
                        Encode = c.String(),
                        Designation = c.String(),
                        HardnessHRC = c.String(),
                        HardnessHB = c.String(),
                        Materialgroup = c.String(),
                        CRpercent = c.Double(nullable: false),
                        SIpercent = c.Double(nullable: false),
                        NIpercent = c.Double(nullable: false),
                        CUpercent = c.Double(nullable: false),
                        NBpercent = c.Double(nullable: false),
                        CEpercent = c.Double(nullable: false),
                        ZNpercent = c.Double(nullable: false),
                        Wpercent = c.Double(nullable: false),
                        MNpercent = c.Double(nullable: false),
                        Vpercent = c.Double(nullable: false),
                        TIpercent = c.Double(nullable: false),
                        COpercent = c.Double(nullable: false),
                        ALpercent = c.Double(nullable: false),
                        Bpercent = c.Double(nullable: false),
                        Spercent = c.Double(nullable: false),
                        Npercent = c.Double(nullable: false),
                        MOpercent = c.Double(nullable: false),
                        PBpercent = c.Double(nullable: false),
                        FEpercent = c.Double(nullable: false),
                        Cpercent = c.Double(nullable: false),
                        MGpercent = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PatternDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Collet_Id = c.Int(),
                        HmBar_Id = c.Int(),
                        Tool_Id = c.Int(),
                        ToolHolder_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Collets", t => t.Collet_Id)
                .ForeignKey("dbo.HmBars", t => t.HmBar_Id)
                .ForeignKey("dbo.Tools", t => t.Tool_Id)
                .ForeignKey("dbo.ToolHolders", t => t.ToolHolder_Id)
                .Index(t => t.Collet_Id)
                .Index(t => t.HmBar_Id)
                .Index(t => t.Tool_Id)
                .Index(t => t.ToolHolder_Id);
            
            CreateTable(
                "dbo.Tools",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemNumber = c.String(),
                        Designation = c.String(),
                        Price = c.Double(nullable: false),
                        Stock = c.Int(nullable: false),
                        ProductType = c.String(),
                        ProductGroup = c.String(),
                        Product = c.String(),
                        Supplier = c.String(),
                        ToolMaterial = c.String(),
                        Coating = c.String(),
                        Rm_CuttingDiameter_D = c.Double(),
                        Rm_EffLength_l2 = c.Double(),
                        Rm_FreeLength_b = c.Double(),
                        Rm_LeadLength = c.Double(),
                        Rm_LeadForm = c.String(),
                        Rm_Shank_d = c.String(),
                        Rm_ShankLength_c = c.String(),
                        Rm_TotalLength_L = c.Double(),
                        Rm_FluteZ = c.Int(),
                        Rm_ReamerDepth = c.Double(),
                        Rm_Handscrew = c.String(),
                        Rm_Key = c.String(),
                        Rm_HelixAngle = c.String(),
                        Td_Undercut_d2 = c.Double(),
                        Td_AngleGammaDegree = c.String(),
                        Td_ShankLength_ls = c.String(),
                        Td_TotalLength_l = c.Double(),
                        Td_ThreadSize = c.Double(),
                        Td_CuttingDiameter_d1 = c.Double(),
                        Td_ThreadPitch_p = c.String(),
                        Td_TotalLength_l1 = c.Double(),
                        Td_CuttingLength_l2 = c.Double(),
                        Td_ShankDiameter_d2 = c.Double(),
                        Td_Square = c.String(),
                        Td_FluteZ = c.Int(),
                        Td_Corehole = c.String(),
                        Td_HelixAngle = c.String(),
                        Td_Design = c.String(),
                        Td_Tolerance = c.String(),
                        ThreadType = c.String(),
                        Td_DinNorm = c.String(),
                        Td_LeadForm = c.String(),
                        VhmC_HelixAngle = c.String(),
                        VhmC_ThreadPitch_p = c.String(),
                        VhmC_ThreadPitch_g1 = c.String(),
                        VhmC_CuttingDirection = c.String(),
                        VhmC_CuttingDiameterD = c.Double(),
                        VhmC_RadiusR = c.Double(),
                        VhmC_AngleThetaDegree = c.String(),
                        VhmC_FreeLength_l1 = c.Double(),
                        VhmC_FreeLength_l2 = c.Double(),
                        VhmC_FreeLength_l3 = c.Double(),
                        VhmC_CuttingLength_l = c.Double(),
                        VhmC_Diameter_d1 = c.Double(),
                        VhmC_Diameter_d2 = c.Double(),
                        VhmC_AngleGammaDegree = c.String(),
                        VhmC_Shank_d = c.String(),
                        VhmC_TotalLength_L = c.Double(),
                        VhmC_Flute_Z = c.Double(),
                        VhmC_EffAngleAlphaDegree = c.String(),
                        VhmC_AngleBetaDegree = c.String(),
                        VhmC_PossibleWorklength30min = c.String(),
                        VhmC_PossibleWorklength1Degree = c.String(),
                        VhmC_PossibleWorklength1Degree30min = c.String(),
                        VhmC_PossibleWorklength2Degree = c.String(),
                        VhmC_PossibleWorklength3Degree = c.String(),
                        VhmD_ShankDiameter_Ds = c.Double(),
                        VhmD_ShankDiameter_D2 = c.Double(),
                        VhmD_ShankDesign_HA = c.String(),
                        VhmD_ShankdDesignHB = c.String(),
                        VhmD_DrillDepth_L3 = c.Double(),
                        VhmD_DrillDiameterTolerance = c.String(),
                        VhmD_Flutes = c.Int(),
                        VhmD_DinNorm = c.String(),
                        VhmD_CuttingDiameter_d1 = c.Double(),
                        VhmD_CuttingDiameterD1 = c.Double(),
                        VhmD_CuttingDiameter_Dc = c.Double(),
                        VhmD_Radius_r = c.Double(),
                        VhmD_FluteLength_l2 = c.Double(),
                        VhmD_FluteLength_l = c.Double(),
                        VhmD_MargindesignNumber = c.String(),
                        VhmD_MargindesignLength = c.String(),
                        VhmD_HelixAngle = c.String(),
                        VhmD_TotalLength_l1 = c.Double(),
                        VhmD_TotalLength_L = c.Double(),
                        VhmD_TotalLengthL1 = c.Double(),
                        VhmD_PointAngleSigma = c.String(),
                        VhmD_Length_L5 = c.String(),
                        VhmD_ShankdDiameter_d2 = c.Double(),
                        WspC_EffAngleDegree = c.Double(),
                        WspC_ModularTorque = c.String(),
                        WspC_ArborDiameter_d = c.Double(),
                        WspC_ArborLength_l = c.Double(),
                        WspC_Hole_d1 = c.String(),
                        WspC_DistanceFlatWrench_T = c.String(),
                        WspC_ClampingScrew = c.String(),
                        WspC_ClampingScrew2 = c.String(),
                        WspC_ClampingClaw = c.String(),
                        WspC_BasePlate = c.String(),
                        WspC_BasePlateScrew = c.String(),
                        WspC_TorqueSpanner = c.String(),
                        WspC_TorqueSpanner2 = c.String(),
                        WspC_ClampingTorque = c.String(),
                        WspC_Torxkey = c.String(),
                        WspC_Torxkey2 = c.String(),
                        WspC_CuttingLength_L1 = c.Double(),
                        WspC_CuttingLengthL2 = c.Double(),
                        WspC_DiameterD1 = c.Double(),
                        WspC_Diameter_Db = c.Double(),
                        WspC_Diameter_d1 = c.Double(),
                        WspC_ShankDs = c.String(),
                        WspC_Shank_d = c.String(),
                        WspC_ShankLength_ls = c.Double(),
                        WspC_TotalLength_L = c.Double(),
                        WspC_TotalLength_L6 = c.Double(),
                        WspC_FluteZ = c.Int(),
                        WspC_Thread_MD = c.String(),
                        WspC_Thread_TS = c.String(),
                        WspC_TSlotWidth_a = c.String(),
                        WspC_TSlotDepth_b = c.String(),
                        WspC_CuttingDiameter_Dc = c.Double(),
                        WspC_CuttingDiameter_D = c.Double(),
                        WspC_CuttingDiameter_Dc1 = c.Double(),
                        WspC_CuttingDiameter_d2 = c.Double(),
                        WspC_Radius_R = c.Double(),
                        WspC_MaxCuttingDepth_ae = c.Double(),
                        WspC_CuttingWidth_W = c.Double(),
                        WspC_AngleThetaDegree = c.Double(),
                        WspC_AngleAlphaDegree = c.Double(),
                        WspC_FreeLength_l1 = c.Double(),
                        WspC_FreeLength_Lf = c.Double(),
                        WspC_CuttingLength_l2 = c.Double(),
                        WspD_ShankdDesignABS = c.String(),
                        WspD_DrillDepth = c.Double(),
                        WspD_DrillDiameterTolerance = c.Double(),
                        WspD_Wormscrew = c.String(),
                        WspD_Capscrew = c.String(),
                        WspD_ClampingScrew = c.String(),
                        WspD_Hexkey = c.String(),
                        WspD_Torxkey = c.String(),
                        WspD_TorqueSpanner = c.String(),
                        WspD_ClampingTorque = c.String(),
                        WspD_CuttingDiameter_D = c.Double(),
                        WspD_CuttingDiameter_Dc = c.Double(),
                        WspD_DrillDepth_l = c.Double(),
                        WspD_DrillDepth_N = c.Double(),
                        WspD_Overhang_l2 = c.String(),
                        WspD_Overhang_Lf = c.String(),
                        WspD_TotalLength_L = c.Double(),
                        WspD_ShankDiameter_d = c.Double(),
                        WspD_ShankDiameter_Ds = c.Double(),
                        WspD_FlangeDiameter_d3 = c.Double(),
                        WspD_FlangeDiameter_D = c.Double(),
                        WspD_ShankDesign_HB = c.String(),
                        WspD_ShankDesign_HE = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ToolHolders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InsideDiameter_d = c.Double(nullable: false),
                        FactoryStandard = c.String(),
                        Diameter_D1 = c.Double(nullable: false),
                        Diameter_D = c.Double(nullable: false),
                        Diameter_D2 = c.Double(nullable: false),
                        Diameter_D3 = c.Double(nullable: false),
                        Diameter_D4 = c.Double(nullable: false),
                        Diameter_D5 = c.Double(nullable: false),
                        FreeLengthL = c.Double(nullable: false),
                        FreeLengthL1 = c.Double(nullable: false),
                        FreeLengthL2 = c.Double(nullable: false),
                        FreeLengthL3 = c.Double(nullable: false),
                        FreeLengthL4 = c.Double(nullable: false),
                        FreeLength_A = c.Double(nullable: false),
                        FreeLength_l = c.Double(nullable: false),
                        FreeLength_l1 = c.Double(nullable: false),
                        FreeLength_l2 = c.Double(nullable: false),
                        FreeLength_l3 = c.Double(nullable: false),
                        FreeLength_H1 = c.Double(nullable: false),
                        NeckAngleTheta = c.Double(nullable: false),
                        PassDiameterB = c.Double(nullable: false),
                        MaxClampingLength_H = c.Double(nullable: false),
                        MaxClampingLength_l = c.Double(nullable: false),
                        MinClampingLength = c.Double(nullable: false),
                        LengthCompensation = c.Double(nullable: false),
                        Spanner = c.String(),
                        RollerSpanner = c.String(),
                        TorqueSpanner = c.String(),
                        ClampingSpanner = c.String(),
                        KexKey = c.String(),
                        AdjustmentScrew = c.String(),
                        TappingRange = c.String(),
                        FineBalancing = c.String(),
                        RunoutAccuracy = c.Double(nullable: false),
                        DiameterC1 = c.Double(nullable: false),
                        DiameterC2 = c.Double(nullable: false),
                        ThreadHoleW1 = c.Double(nullable: false),
                        Thread = c.String(),
                        TSlotHeightK1 = c.Double(nullable: false),
                        Screw_G1 = c.String(),
                        FixedBlockPosition_S = c.String(),
                        GearRatio = c.String(),
                        MaxSpindleSpeed = c.Double(nullable: false),
                        AdjustMin = c.Double(nullable: false),
                        AdjustMax = c.Double(nullable: false),
                        PullStud_M = c.Double(nullable: false),
                        CapNut = c.String(),
                        CoolantTube = c.String(),
                        WrenchCoolantTube = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Projectname = c.String(),
                        Employeeshort = c.String(),
                        Date = c.DateTime(nullable: false),
                        Buildingcomponent = c.String(),
                        Workpiececlamping = c.String(),
                        Strength = c.String(),
                        EngineInfo = c.String(),
                        MaterialInfo = c.String(),
                        ClampingInfo = c.String(),
                        ReportPicturename1 = c.String(),
                        ReportPicturename2 = c.String(),
                        ReportPicturename3 = c.String(),
                        Customer_Id = c.Int(),
                        Engine_Id = c.Int(),
                        Material_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.Customer_Id)
                .ForeignKey("dbo.Engines", t => t.Engine_Id)
                .ForeignKey("dbo.Materials", t => t.Material_Id)
                .Index(t => t.Customer_Id)
                .Index(t => t.Engine_Id)
                .Index(t => t.Material_Id);
            
            CreateTable(
                "dbo.ToollistItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ToolPicturename = c.String(),
                        Manufacturer = c.String(),
                        IsCompentitor = c.Boolean(nullable: false),
                        Toolholder = c.String(),
                        ToolId = c.String(),
                        Insert = c.String(),
                        Coating = c.String(),
                        Flutes = c.Int(nullable: false),
                        Cuttingedges = c.Int(nullable: false),
                        Diameter = c.Int(nullable: false),
                        Necklength = c.Int(nullable: false),
                        Overhang = c.Int(nullable: false),
                        Processing = c.String(),
                        Report_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Reports", t => t.Report_Id)
                .Index(t => t.Report_Id);
            
            CreateTable(
                "dbo.Toolvaluepages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Processing = c.String(),
                        Noise = c.String(),
                        Cooling = c.String(),
                        Operationtime = c.Int(nullable: false),
                        Ofwear = c.String(),
                        Wearintensity = c.String(),
                        Strategy = c.String(),
                        MaterialRemovalrate = c.String(),
                        RampingAngle = c.Double(nullable: false),
                        Application = c.String(),
                        ActualCuttingspeed = c.Double(nullable: false),
                        ActualSpindlespeed = c.Double(nullable: false),
                        ActualFeedPerMinute = c.Double(nullable: false),
                        ActualFeedPerTooth = c.Double(nullable: false),
                        ActualFeedPerRotation = c.Double(nullable: false),
                        ActualAxial = c.Double(nullable: false),
                        ActualRadial = c.Double(nullable: false),
                        ExpectedCuttingspeed = c.Double(nullable: false),
                        ExpectedSpindlespeed = c.Double(nullable: false),
                        ExpectedFeedPerMinute = c.Double(nullable: false),
                        ExpectedFeedPerTooth = c.Double(nullable: false),
                        ExpectedFeedPerRotation = c.Double(nullable: false),
                        ExpectedAxial = c.Double(nullable: false),
                        ExpectedRadial = c.Double(nullable: false),
                        Resolution = c.Int(nullable: false),
                        Hasavideo = c.Boolean(nullable: false),
                        Applicationtype = c.String(),
                        SpindleLoadingCapacity = c.String(),
                        ToolLife = c.String(),
                        ToolInfo = c.String(),
                        ToolvaluepagePicturename1 = c.String(),
                        ToolvaluepagePicturename2 = c.String(),
                        ToolvaluepagePicturename3 = c.String(),
                        ToolvaluepagePicturename4 = c.String(),
                        ToolvaluepagePicturename5 = c.String(),
                        ToollistItem_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ToollistItems", t => t.ToollistItem_Id)
                .Index(t => t.ToollistItem_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Toolvaluepages", "ToollistItem_Id", "dbo.ToollistItems");
            DropForeignKey("dbo.ToollistItems", "Report_Id", "dbo.Reports");
            DropForeignKey("dbo.Reports", "Material_Id", "dbo.Materials");
            DropForeignKey("dbo.Reports", "Engine_Id", "dbo.Engines");
            DropForeignKey("dbo.Reports", "Customer_Id", "dbo.Customers");
            DropForeignKey("dbo.PatternDatas", "ToolHolder_Id", "dbo.ToolHolders");
            DropForeignKey("dbo.PatternDatas", "Tool_Id", "dbo.Tools");
            DropForeignKey("dbo.PatternDatas", "HmBar_Id", "dbo.HmBars");
            DropForeignKey("dbo.PatternDatas", "Collet_Id", "dbo.Collets");
            DropIndex("dbo.Toolvaluepages", new[] { "ToollistItem_Id" });
            DropIndex("dbo.ToollistItems", new[] { "Report_Id" });
            DropIndex("dbo.Reports", new[] { "Material_Id" });
            DropIndex("dbo.Reports", new[] { "Engine_Id" });
            DropIndex("dbo.Reports", new[] { "Customer_Id" });
            DropIndex("dbo.PatternDatas", new[] { "ToolHolder_Id" });
            DropIndex("dbo.PatternDatas", new[] { "Tool_Id" });
            DropIndex("dbo.PatternDatas", new[] { "HmBar_Id" });
            DropIndex("dbo.PatternDatas", new[] { "Collet_Id" });
            DropTable("dbo.Toolvaluepages");
            DropTable("dbo.ToollistItems");
            DropTable("dbo.Reports");
            DropTable("dbo.ToolHolders");
            DropTable("dbo.Tools");
            DropTable("dbo.PatternDatas");
            DropTable("dbo.Materials");
            DropTable("dbo.HmBars");
            DropTable("dbo.Engines");
            DropTable("dbo.Customers");
            DropTable("dbo.Collets");
        }
    }
}
