namespace TrmDb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ToollistItems", "IsCompetitor", c => c.Boolean(nullable: false));
            DropColumn("dbo.ToollistItems", "IsCompentitor");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ToollistItems", "IsCompentitor", c => c.Boolean(nullable: false));
            DropColumn("dbo.ToollistItems", "IsCompetitor");
        }
    }
}
