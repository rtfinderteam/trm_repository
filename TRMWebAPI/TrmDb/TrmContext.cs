﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrmDb.Models;
using TrmDb.ToolModels;

namespace TrmDb
{
    public class TrmContext : DbContext
    {
        public TrmContext() : base("TrmDb")
        //public TrmContext()
        {
            Debug.Write(Database.Connection.ConnectionString);
        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Engine> Engines { get; set; }
        //public DbSet<Manufacturepicture> Manufacturepictures { get; set; }
        public DbSet<Material> Materials { get; set; }
        //public DbSet<Picture> Pictures { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<ToollistItem> ToollistItems { get; set; }
        //public DbSet<Toolpicture> Toolpictures { get; set; }
        public DbSet<Toolvaluepage> Toolvaluepages { get; set; }
        //public DbSet<Toolvaluepicture> Toolvaluepictures { get; set; }

        public DbSet<Note> Notes { get; set; }

        //tool-stuff

        public DbSet<PatternData> PatternData { get; set; }
        public DbSet<Collet> Collets { get; set; }
        public DbSet<HmBar> HmBars { get; set; }
        public DbSet<ToolHolder> ToolHolders { get; set; }
        public DbSet<Reamer> Reamers { get; set; }
        public DbSet<Thread_Drill> Thread_Drills { get; set; }
        public DbSet<VHM_Cutter> VHM_Cutters { get; set; }
        public DbSet<VHM_Drill> VHM_Drills { get; set; }
        public DbSet<WsP_Cutter> WsP_Cutters { get; set; }
        public DbSet<WsP_Drill> WsP_Drills { get; set; }




    }
}
