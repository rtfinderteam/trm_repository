﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
      <fo:simple-page-master master-name="simple"
                      page-height="29.7cm"
                      page-width="21cm"
                      margin-top="1cm"
                      margin-bottom="2cm"
                      margin-left="2.5cm"
                      margin-right="2.5cm">
        <fo:region-body margin-top="3cm"/>
        <fo:region-before extent="3cm"/>
        <fo:region-after extent="1.5cm"/>
      </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-reference="simple">
      <fo:flow flow-name="xsl-region-body">
        <fo:table>
          <fo:table-body>
            <fo:table-row>
              <fo:table-cell>
                <fo:block>Title</fo:block>
              </fo:table-cell>
              <fo:table-cell>
                <fo:block>Author</fo:block>
              </fo:table-cell>
              <fo:table-cell>
                <fo:block>Price</fo:block>
              </fo:table-cell>
            </fo:table-row>
            <xsl:for-each select="Books/Book">
              <fo:table-row>
                <fo:table-cell>
                  <fo:block>
                    <xsl:value-of select=".//Title" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block>
                    <xsl:value-of select=".//Author" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block>
                    <xsl:value-of select=".//Price" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </xsl:for-each>
          </fo:table-body>
        </fo:table>
      </fo:flow>
    </fo:page-sequence>



    <fo:table>
      <fo:table-body>
        <fo:table-row>
          <fo:table-cell>
            <fo:block>Title</fo:block>
          </fo:table-cell>
          <fo:table-cell>
            <fo:block>Author</fo:block>
          </fo:table-cell>
          <fo:table-cell>
            <fo:block>Price</fo:block>
          </fo:table-cell>
        </fo:table-row>
        <xsl:for-each select="Books/Book">
          <fo:table-row>
            <fo:table-cell>
              <fo:block>
                <xsl:value-of select=".//Title" />
              </fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>
                <xsl:value-of select=".//Author" />
              </fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>
                <xsl:value-of select=".//Price" />
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
        </xsl:for-each>
      </fo:table-body>
    </fo:table>
  </fo:root>

</xsl:stylesheet>