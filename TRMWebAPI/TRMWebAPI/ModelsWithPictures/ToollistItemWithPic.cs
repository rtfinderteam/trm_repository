﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using TrmDb.Models;
using TRMWebAPI.HelperClasses;

namespace TRMWebAPI.ModelsWithPictures
{
    public class ToollistItemWithPic
    {
        public ToollistItemWithPic()
        {

        }
        public ToollistItemWithPic(ToollistItem t)
        {
            //constructor to make a engine to a EnginWithPic
            //using reflection to compare the properties and fill in the same ones
            var tiiProps = t.GetType().GetProperties();
            var tiiPicProps = this.GetType().GetProperties();

            foreach (var prop in tiiProps)
            {
                foreach (var picProp in tiiPicProps)
                {
                    if (picProp.Name == prop.Name && picProp.PropertyType == prop.PropertyType)
                    {
                        picProp.SetValue(this, prop.GetValue(t));
                        break;
                    }
                }
            }
            //replacing of the pictures
            //tool pic
            try
            {
                if (t.ToolPicturename != "" && t.ToolPicturename != null)
                {
                    //this.ToolPicture = Image.FromFile(Globals.PicturePath + t.ToolPicturename);
                    this.ToolPicture = File.ReadAllBytes(Globals.PicturePath + t.ToolPicturename);
                }
            }
            catch (FileNotFoundException exc)
            {
                var replacement = TextToImageConvert.ConvertTextToImage("Image not found.", "Arial", 14, System.Drawing.Color.SkyBlue, System.Drawing.Color.Red, 200, 50);
                this.ToolPicture = replacement;
                //using (var ms = new MemoryStream(replacement))
                //{
                //    this.ToolPicture = Image.FromStream(ms);
                //}
            }
            //reportWithPic stuff
            this.ReportWithPic = new ReportWithPic(t.Report);
        }

        public ToollistItem ParseToToollistItem()
        {
            ToollistItem t = new ToollistItem();

            //using reflection to compare the properties and fill in the same ones
            var engineProps = t.GetType().GetProperties();
            var enginePicProps = this.GetType().GetProperties();

            foreach (var prop in engineProps)
            {
                foreach (var picProp in enginePicProps)
                {
                    if (picProp.Name == prop.Name && picProp.PropertyType == prop.PropertyType)
                    {
                        prop.SetValue(t, picProp.GetValue(this));
                        break;
                    }
                }
            }
            //picture business-->check if there is a pic then saving the picture and putting the name into the model
            //ToolPicture
            if (ToolPicture != null)
            {
                //string picName = $@"{Guid.NewGuid()}.{ToolPicture.RawFormat}";
                string picName = $@"{Guid.NewGuid()}.jpg";
                //ToolPicture.Save(Globals.PicturePath + picName);
                Image.FromStream(new MemoryStream(ToolPicture)).Save(Globals.PicturePath + picName);
                t.ToolPicturename = picName;
            }
            //ReportWithPic stuff
            if (this.ReportWithPic != null)
            {
                t.Report = this.ReportWithPic.ParseToReport();
            }

            return t;
        }

        public bool CheckIfNull()
        {
            var props = this.GetType().GetProperties();

            foreach (var p in props)
            {
                if (p.Name == "Id") continue;
                if (p.PropertyType == typeof(bool)) continue;
                if (p.GetValue(this) != null) return false;

            }
            return true;
        }
        public int? Id { get; set; }
        public byte[] ToolPicture { get; set; }
        public string Manufacturer { get; set; }
        public bool IsCompetitor { get; set; }
        public string Toolholder { get; set; }
        public string ToolId { get; set; }
        public string Insert { get; set; }
        public string Coating { get; set; }
        public int? Flutes { get; set; }
        public int? Cuttingedges { get; set; }
        public int? Diameter { get; set; }
        public int? Necklength { get; set; }
        public int? Overhang { get; set; }
        public string Processing { get; set; }
        public ReportWithPic ReportWithPic { get; set; }
    }
}