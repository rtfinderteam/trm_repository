﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using TrmDb.Models;
using TRMWebAPI.HelperClasses;

namespace TRMWebAPI.ModelsWithPictures
{
    public class ToolvaluepageWithPic
    {
        public ToolvaluepageWithPic()
        {

        }
        public ToolvaluepageWithPic(Toolvaluepage t)
        {
            //constructor to make a engine to a EnginWithPic
            //using reflection to compare the properties and fill in the same ones
            var tvpProps = t.GetType().GetProperties();
            var tvpPicProps = this.GetType().GetProperties();

            foreach (var prop in tvpProps)
            {
                foreach (var picProp in tvpPicProps)
                {
                    if (picProp.Name == prop.Name && picProp.PropertyType == prop.PropertyType)
                    {
                        picProp.SetValue(this, prop.GetValue(t));
                        break;
                    }
                }
            }
            //replacing of the pictures
            //pic1
            PictureReplace(t, t.GetType().GetProperty("ToolvaluepagePicturename1"), this.GetType().GetProperty("ToolvaluepagePicture1"));
            //pic2
            PictureReplace(t, t.GetType().GetProperty("ToolvaluepagePicturename2"), this.GetType().GetProperty("ToolvaluepagePicture2"));
            //pic3
            PictureReplace(t, t.GetType().GetProperty("ToolvaluepagePicturename3"), this.GetType().GetProperty("ToolvaluepagePicture3"));
            //pic4
            PictureReplace(t, t.GetType().GetProperty("ToolvaluepagePicturename4"), this.GetType().GetProperty("ToolvaluepagePicture4"));
            //pic5
            PictureReplace(t, t.GetType().GetProperty("ToolvaluepagePicturename5"), this.GetType().GetProperty("ToolvaluepagePicture5"));

            //changing Engine to EngineWithPic
            this.ToollistItemWithPic = new ToollistItemWithPic(t.ToollistItem);
        }
        private void PictureReplace(Toolvaluepage r, PropertyInfo picName, PropertyInfo realPic)
        {
            try
            {
                if (picName.GetValue(r) != null)
                {
                    if (picName.GetValue(r).ToString() != "")
                    {
                        //realPic.SetValue(this, Image.FromFile(Globals.PicturePath + picName.GetValue(r).ToString()));
                        realPic.SetValue(this, File.ReadAllBytes(Globals.PicturePath + picName.GetValue(r)));
                    }
                }
            }
            catch (FileNotFoundException exc)
            {
                var replacement = TextToImageConvert.ConvertTextToImage("Image not found.", "Arial", 14, System.Drawing.Color.SkyBlue, System.Drawing.Color.Red, 200, 50);
                realPic.SetValue(this, replacement);
                //using (var ms = new MemoryStream(replacement))
                //{
                //    realPic.SetValue(this, Image.FromStream(ms));
                //}

            }
        }

        public bool CheckIfNull()
        {
            var props = this.GetType().GetProperties();

            foreach (var p in props)
            {
                if (p.Name == "Id") continue;
                if (p.PropertyType == typeof(bool)) continue;
                if (p.GetValue(this) != null) return false;

            }
            return true;
        }

        public Toolvaluepage ParseToToolvaluepage()
        {
            Toolvaluepage t = new Toolvaluepage();

            //using reflection to compare the properties and fill in the same ones
            var tvpProps = t.GetType().GetProperties();
            var tvpPicProps = this.GetType().GetProperties();

            foreach (var prop in tvpProps)
            {
                foreach (var picProp in tvpPicProps)
                {
                    if (picProp.Name == prop.Name && picProp.PropertyType == prop.PropertyType)
                    {
                        prop.SetValue(t, picProp.GetValue(this));
                        break;
                    }
                }
            }
            //picture business-->check if pic is not null then saving the picture and putting the name into the model
            string picName;
            //pic1
            if (ToolvaluepagePicture1 != null)
            {
                picName = $@"{Guid.NewGuid()}.jpg";
                Image.FromStream(new MemoryStream(ToolvaluepagePicture1)).Save(Globals.PicturePath + picName);
                t.ToolvaluepagePicturename1 = picName;
            }
            //pic2
            if (ToolvaluepagePicture2 != null)
            {
                picName = $@"{Guid.NewGuid()}.jpg";
                Image.FromStream(new MemoryStream(ToolvaluepagePicture2)).Save(Globals.PicturePath + picName);
                t.ToolvaluepagePicturename2 = picName;
            }
            //pic3
            if (ToolvaluepagePicture3 != null)
            {
                picName = $@"{Guid.NewGuid()}.jpg";
                Image.FromStream(new MemoryStream(ToolvaluepagePicture3)).Save(Globals.PicturePath + picName);
                t.ToolvaluepagePicturename3 = picName;
            }
            //pic4
            if (ToolvaluepagePicture4 != null)
            {
                picName = $@"{Guid.NewGuid()}.jpg";
                Image.FromStream(new MemoryStream(ToolvaluepagePicture4)).Save(Globals.PicturePath + picName);
                t.ToolvaluepagePicturename4 = picName;
            }
            //pic5
            if (ToolvaluepagePicture5 != null)
            {
                picName = $@"{Guid.NewGuid()}.jpg";
                Image.FromStream(new MemoryStream(ToolvaluepagePicture5)).Save(Globals.PicturePath + picName);
                t.ToolvaluepagePicturename5 = picName;
            }
            //ToollistItemWithPic Stuff
            if (this.ToollistItemWithPic != null)
            {
                t.ToollistItem = this.ToollistItemWithPic.ParseToToollistItem();
            }

            return t;
        }
        public int? Id { get; set; }
        public string Processing { get; set; }
        public string Noise { get; set; }
        public string Cooling { get; set; }
        public int? Operationtime { get; set; }
        public string Ofwear { get; set; }
        public string Wearintensity { get; set; }
        public string Strategy { get; set; }
        public string MaterialRemovalrate { get; set; }
        public double? RampingAngle { get; set; }
        public string Application { get; set; }
        public double? ActualCuttingspeed { get; set; }
        public double? ActualSpindlespeed { get; set; }
        public double? ActualFeedPerMinute { get; set; }
        public double? ActualFeedPerTooth { get; set; }
        public double? ActualFeedPerRotation { get; set; }
        public double? ActualAxial { get; set; }
        public double? ActualRadial { get; set; }
        public double? ExpectedCuttingspeed { get; set; }
        public double? ExpectedSpindelspeed { get; set; }
        public double? ExpectedFeedPerMinute { get; set; }
        public double? ExpectedFeedPerTooth { get; set; }
        public double? ExpectedFeedPerRotation { get; set; }
        public double? ExpectedAxial { get; set; }
        public double? ExpectedRadial { get; set; }
        public int? Resolution { get; set; }
        public bool Hasavideo { get; set; }
        public string Applicationtype { get; set; }
        public string SpindleLoadingCapacity { get; set; }
        public string ToolLife { get; set; }
        public string ToolInfo { get; set; }
        public ToollistItemWithPic ToollistItemWithPic { get; set; }
        public byte[] ToolvaluepagePicture1 { get; set; }
        public byte[] ToolvaluepagePicture2 { get; set; }
        public byte[] ToolvaluepagePicture3 { get; set; }
        public byte[] ToolvaluepagePicture4 { get; set; }
        public byte[] ToolvaluepagePicture5 { get; set; }
    }
}