﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TRMWebAPI.ModelsWithPictures
{
    public class Search
    {
        //TOOL erst wenn Schnittstelle fertig
        //TOOL erst wenn Schnittstelle fertig
        //public string supplier { get; set; } 
        //public string toolgroup { get; set; }
        //public int? diameter { get; set; }
        //public string articlenumber { get; set; }
        //public string wspquality { get; set; }

        public S_Material S_Material { get; set; }
        public S_Engine S_Engine { get; set; }
        public S_Customer S_Customer { get; set; }
        public S_Report S_Report { get; set; }
    }

    public class S_Material
    {
        public string materialgroup { get; set; } //materialgroup im Material
        public string designation { get; set; } //designation im Material
        public string dincode { get; set; }
        public string encode { get; set; }
        public string hardness { get; set; } //hardness im Material
    }

    public class S_Engine
    {
        public string manufacture { get; set; }
        public string typ { get; set; } //typ
        public string spindel { get; set; }
    }

    public class S_Customer
    {
        public string name { get; set; } //name
        public string branch { get; set; }
    }

    public class S_Report
    {
        public string strength { get; set; }
        public string employeeshort { get; set; }
    }
}