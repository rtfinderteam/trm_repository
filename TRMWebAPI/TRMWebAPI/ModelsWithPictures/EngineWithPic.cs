﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using TrmDb.Models;
using TRMWebAPI.HelperClasses;

namespace TRMWebAPI.ModelsWithPictures
{
    public class EngineWithPic
    {
        public EngineWithPic()
        {

        }
        public EngineWithPic(Engine e)
        {
            //constructor to make a engine to a EnginWithPic
            //using reflection to compare the properties and fill in the same ones
            var engineProps = e.GetType().GetProperties();
            var enginePicProps = this.GetType().GetProperties();

            foreach (var prop in engineProps)
            {
                foreach (var picProp in enginePicProps)
                {
                    if (picProp.Name == prop.Name && picProp.PropertyType == prop.PropertyType)
                    {
                        picProp.SetValue(this, prop.GetValue(e));
                        break;
                    }
                }
            }
            //replacing of the picture
            try
            {
                if (e.EnginePicturename == "" || e.EnginePicturename == null) return;
                this.EnginePicture = File.ReadAllBytes(Globals.PicturePath + e.EnginePicturename);
                //this.EnginePicture = Image.FromFile(Globals.PicturePath + e.EnginePicturename);
            }
            catch (System.IO.FileNotFoundException exc)
            {
                var replacement = TextToImageConvert.ConvertTextToImage("Image not found.", "Arial", 14, System.Drawing.Color.SkyBlue, System.Drawing.Color.Red, 200, 50);
                this.EnginePicture = replacement;
                //earlier Engine Picture was of the Image type, now it is a byte[]
                //using (var ms = new MemoryStream(replacement))
                //{
                //    this.EnginePicture = Image.FromStream(ms);
                //}

            }
        }

        public Engine ParseToEngine()
        {
            Engine e = new Engine();

            //using reflection to compare the properties and fill in the same ones
            var engineProps = e.GetType().GetProperties();
            var enginePicProps = this.GetType().GetProperties();

            foreach (var prop in engineProps)
            {
                foreach (var picProp in enginePicProps)
                {
                    if (picProp.Name == prop.Name && picProp.PropertyType == prop.PropertyType)
                    {
                        prop.SetValue(e, picProp.GetValue(this));
                        break;
                    }
                }
            }
            //check if there is a pic
            if (EnginePicture == null) return e;
            //picture business-->saving the picture and putting the name into the model
            //string picName = $@"{Guid.NewGuid()}.{EnginePicture.RawFormat}";
            string picName = $@"{Guid.NewGuid()}.jpg";
            //EnginePicture.Save(Globals.PicturePath + picName);
            Image.FromStream(new MemoryStream(EnginePicture)).Save(Globals.PicturePath + picName);
            e.EnginePicturename = picName;

            return e;
        }

        public int Id { get; set; }
        public string Manufacturer { get; set; }
        public string Type { get; set; }
        public string Spindle { get; set; }
        public double Coolantoutside { get; set; }
        public double Coolantinside { get; set; }
        public int Power { get; set; }
        public int Rotationalspeed { get; set; }
        public int Feed { get; set; }
        public int Activationyear { get; set; }
        public int Hourlyrate { get; set; }
        public string Programming { get; set; }
        public string Controlling { get; set; }
        public byte[] EnginePicture { get; set; }
    }
}