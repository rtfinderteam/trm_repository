﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using TrmDb.Models;
using TRMWebAPI.HelperClasses;

namespace TRMWebAPI.ModelsWithPictures
{
    public class ReportWithPic
    {
        public ReportWithPic()
        {

        }

        public ReportWithPic(Report r)
        {
            //constructor to make a engine to a EnginWithPic
            //using reflection to compare the properties and fill in the same ones
            var reportProps = r.GetType().GetProperties();
            var reportPicProps = this.GetType().GetProperties();

            foreach (var prop in reportProps)
            {
                foreach (var picProp in reportPicProps)
                {
                    if (picProp.Name == prop.Name && picProp.PropertyType == prop.PropertyType)
                    {
                        picProp.SetValue(this, prop.GetValue(r));
                        break;
                    }
                }
            }
            //replacing of the pictures
            //pic1
            PictureReplace(r, r.GetType().GetProperty("ReportPicturename1"), this.GetType().GetProperty("ReportPicture1"));
            //pic2
            PictureReplace(r, r.GetType().GetProperty("ReportPicturename2"), this.GetType().GetProperty("ReportPicture2"));
            //pic3
            PictureReplace(r, r.GetType().GetProperty("ReportPicturename3"), this.GetType().GetProperty("ReportPicture3"));

            //changing Engine to EngineWithPic
            this.Engine = new EngineWithPic(r.Engine);
        }

        private void PictureReplace(Report r, PropertyInfo picName, PropertyInfo realPic)
        {
            try
            {
                if (picName.GetValue(r) != null)
                {
                    if (picName.GetValue(r).ToString() != "")
                    {
                        //realPic.SetValue(this, Image.FromFile(Globals.PicturePath + picName.GetValue(r).ToString()));
                        realPic.SetValue(this, File.ReadAllBytes(Globals.PicturePath + picName.GetValue(r)));
                    }
                }
            }
            catch (FileNotFoundException exc)
            {
                var replacement = TextToImageConvert.ConvertTextToImage("Image not found.", "Arial", 14, System.Drawing.Color.SkyBlue, System.Drawing.Color.Red, 200, 50);
                realPic.SetValue(this, replacement);
                //using (var ms = new MemoryStream(replacement))
                //{
                //    realPic.SetValue(this, Image.FromStream(ms));
                //}

            }
        }
        public Report ParseToReport()
        {
            Report r = new Report();

            //using reflection to compare the properties and fill in the same ones
            var reportProps = r.GetType().GetProperties();
            var reportPicProps = this.GetType().GetProperties();

            foreach (var prop in reportProps)
            {
                foreach (var picProp in reportPicProps)
                {
                    if (picProp.Name == prop.Name && picProp.PropertyType == prop.PropertyType)
                    {
                        prop.SetValue(r, picProp.GetValue(this));
                        break;
                    }
                }
            }
            //picture business-->check if there is a pic then saving the picture and putting the name into the model
            //pic1
            string picName;
            if (ReportPicture1 != null)
            {
                picName = $@"{Guid.NewGuid()}.jpg";
                //ReportPicture1.Save(Globals.PicturePath + picName);
                Image.FromStream(new MemoryStream(ReportPicture1)).Save(Globals.PicturePath + picName);
                r.ReportPicturename1 = picName;
            }
            //pic2
            if (ReportPicture2 != null)
            {
                picName = $@"{Guid.NewGuid()}.jpg";
                Image.FromStream(new MemoryStream(ReportPicture2)).Save(Globals.PicturePath + picName);
                r.ReportPicturename2 = picName;
            }
            //pic3
            if (ReportPicture3 != null)
            {
                picName = $@"{Guid.NewGuid()}.jpg";
                Image.FromStream(new MemoryStream(ReportPicture3)).Save(Globals.PicturePath + picName);
                r.ReportPicturename3 = picName;
            }
            //EngineWithPic Stuff
            r.Engine = this.Engine.ParseToEngine();

            return r;
        }

        public int Id { get; set; }
        public string Projectname { get; set; }
        public string Employeeshort { get; set; }
        public DateTime Date { get; set; }
        public string Buildingcomponent { get; set; }
        public string Workpiececlamping { get; set; }
        public string Strength { get; set; }
        public string EngineInfo { get; set; }
        public string MaterialInfo { get; set; }
        public string ClampingInfo { get; set; }
        public EngineWithPic Engine { get; set; }
        public Customer Customer { get; set; }
        public Material Material { get; set; }
        public byte[] ReportPicture1 { get; set; }
        public byte[] ReportPicture2 { get; set; }
        public byte[] ReportPicture3 { get; set; }
    }
}