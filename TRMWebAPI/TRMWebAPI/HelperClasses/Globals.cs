﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TRMWebAPI.HelperClasses
{
    public static class Globals
    {
        public static readonly string PicturePath = AppDomain.CurrentDomain.BaseDirectory + "Pictures\\";
        //for pdf location use System.io.path.gettempfilename and replace the .tmp with .pdf
    }
}