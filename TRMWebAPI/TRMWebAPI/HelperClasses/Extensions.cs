﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TRMWebAPI.HelperClasses
{
    public static class Extensions
    {
        public static string ReplaceFirst(this string str, string oldValue, string newValue)
        {
            int i = str.IndexOf(oldValue);
            if (i == -1) return str;
            str = str.Remove(i, oldValue.Length);
            str = str.Insert(i, newValue);
            return str;
        }
        public static T Clone<T>(this T instance)
        {
            var json = JsonConvert.SerializeObject(instance);
            return JsonConvert.DeserializeObject<T>(json);
        }
        /// <summary>
        /// A String-Extension for a Contains that can ignore cases.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="toCheck"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public static bool ContainsNonCase(this string s, string toCheck, StringComparison comp)
        {
            return s?.IndexOf(toCheck, comp) >= 0;
        }
    }
}