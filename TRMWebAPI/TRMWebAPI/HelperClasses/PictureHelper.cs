﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace TRMWebAPI.HelperClasses
{
    public static class PictureHelper
    {
        /// <summary>
        /// Merges a number of pictures to one big picture. 
        /// </summary>
        /// <param name="pictureNames">Just a list of the picturenames. pictures will be loaded from the usual file location.</param>
        /// <returns>Path to the merged picture</returns>
        public static String MergePictures(List<String> pictureNames)
        {
            List<Image> images = new List<Image>();
            foreach (var item in pictureNames)
            {
                if (item != "" && item != null)
                {
                    images.Add(Image.FromFile(Globals.PicturePath + item));
                }
            }
            if (images.Count() > 0)
            {
                int width = images.Select(x => x.Width).Sum();
                int heigth = images.Select(x => x.Height).Max();
                Bitmap big = new Bitmap(width, heigth);
                Graphics g = Graphics.FromImage(big);
                g.Clear(Color.White);
                int startwidth = 0;
                foreach (var img in images)
                {
                    g.DrawImage(img, new Point(startwidth, 0));
                    startwidth += img.Width;
                }
                g.Dispose();
                images.ForEach(x => x.Dispose());

                string path = System.IO.Path.GetTempFileName();
                path=path.Replace(".tmp", ".jpg");
                big.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                return path;
            }
            return "";
        }
    }
}