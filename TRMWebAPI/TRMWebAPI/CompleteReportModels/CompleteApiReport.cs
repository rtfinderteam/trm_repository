﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TRMWebAPI.ModelsWithPictures;

namespace TRMWebAPI.CompleteReportModels
{
    public class CompleteApiReport
    {
        public int Id { get; set; }
        public ReportWithPic Report { get; set; }
        public ToollistItemWithPic Toollistitem1 { get; set; }
        public ToolvaluepageWithPic Toolvaluepage1 { get; set; }
        public ToollistItemWithPic Toollistitem2 { get; set; }
        public ToolvaluepageWithPic Toolvaluepage2 { get; set; }
        public ToollistItemWithPic Toollistitem3 { get; set; }
        public ToolvaluepageWithPic Toolvaluepage3 { get; set; }
        public ToollistItemWithPic Toollistitem4 { get; set; }
        public ToolvaluepageWithPic Toolvaluepage4 { get; set; }
        public ToollistItemWithPic Toollistitem5 { get; set; }
        public ToolvaluepageWithPic Toolvaluepage5 { get; set; }
        public ToollistItemWithPic Toollistitem6 { get; set; }
        public ToolvaluepageWithPic Toolvaluepage6 { get; set; }
        public ToollistItemWithPic Toollistitem7 { get; set; }
        public ToolvaluepageWithPic Toolvaluepage7 { get; set; }
    }
}