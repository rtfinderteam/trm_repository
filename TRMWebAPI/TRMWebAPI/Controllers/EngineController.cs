﻿using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using TrmDb;
using TRMWebAPI.ActionFilters;
using TRMWebAPI.HelperClasses;
using TRMWebAPI.ModelsWithPictures;
using TrmDb.Models;

namespace TRMWebAPI.Controllers
{
    [BasicAuthentication]
    public class EngineController : ApiController
    {
        // GET api/Engine
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                var e = db.Engines.ToList();
                List<EngineWithPic> r = new List<EngineWithPic>();
                foreach (var i in e)
                {
                    r.Add(new EngineWithPic(i));
                }
                return Ok(r);
            }
        }

        // GET api/Engine/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.Engines.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                EngineWithPic e = new EngineWithPic(c);
                return Ok(e);
            }
        }

        // POST api/Engine
        public IHttpActionResult Post([FromBody]EngineWithPic Engine)
        {
            using (var db = new TrmContext())
            {
                if (Engine == null) return BadRequest();
                Engine e = Engine.ParseToEngine();
                db.Engines.Add(e);
                db.SaveChanges();
                return Ok(e.Id);
            }

        }

        // PUT api/Engine/5
        public IHttpActionResult Put(int id, [FromBody]EngineWithPic Engine)
        {
            using (var db = new TrmContext())
            {
                if (Engine == null) return BadRequest();
                var e = db.Engines.Where(x => x.Id == id).First();
                if (e == null) return BadRequest();

                //might not be perfect way to do it since if the picture does not change it will be saved a second time
                //but i dont care right now ¯\_(ツ)_/¯
                Engine y = Engine.ParseToEngine();
                y.Id = id;
                e = y;
                db.SaveChanges();
                return Ok();
            }
        }

        // DELETE api/Engine/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var e = db.Engines.Where(x => x.Id == id).First();
                if (e == null) return BadRequest();

                db.Engines.Remove(e);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
