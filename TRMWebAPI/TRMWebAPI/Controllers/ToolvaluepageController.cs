﻿using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using TrmDb;
using TrmDb.Models;
using TRMWebAPI.ActionFilters;
using TRMWebAPI.HelperClasses;
using TRMWebAPI.ModelsWithPictures;

namespace TRMWebAPI.Controllers
{
    [BasicAuthentication]
    public class ToolvaluepageController : ApiController
    {
        // GET api/Toolvaluepage
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                var e = db.Toolvaluepages.ToList();
                List<ToolvaluepageWithPic> r = new List<ToolvaluepageWithPic>();
                foreach (var i in e)
                {
                    r.Add(new ToolvaluepageWithPic(i));
                }
                return Ok(r);
            }
        }

        // GET api/Toolvaluepage/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.Toolvaluepages.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                ToolvaluepageWithPic e = new ToolvaluepageWithPic(c);
                return Ok(e);
            }
        }

        // POST api/Toolvaluepage
        public IHttpActionResult Post([FromBody]ToolvaluepageWithPic toolvaluepage)
        {
            using (var db = new TrmContext())
            {
                if (toolvaluepage == null) return BadRequest();
                Toolvaluepage e = toolvaluepage.ParseToToolvaluepage();
                db.Toolvaluepages.Add(e);
                db.SaveChanges();
                return Ok(e.Id);
            }

        }

        // PUT api/Toolvaluepage/5
        public IHttpActionResult Put(int id, [FromBody]ToolvaluepageWithPic toolvaluepage)
        {
            using (var db = new TrmContext())
            {
                if (toolvaluepage == null) return BadRequest();
                var e = db.Toolvaluepages.Where(x => x.Id == id).First();
                if (e == null) return BadRequest();

                //might not be perfect way to do it since if the picture does not change it will be saved a second time
                //but i dont care right now ¯\_(ツ)_/¯
                Toolvaluepage y = toolvaluepage.ParseToToolvaluepage();
                y.Id = id;
                e = y;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/Toolvaluepage/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var e = db.Toolvaluepages.Where(x => x.Id == id).First();
                if (e == null) return BadRequest();

                db.Toolvaluepages.Remove(e);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
