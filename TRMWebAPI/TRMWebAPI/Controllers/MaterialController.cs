﻿using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using TrmDb;
using TrmDb.Models;
using TRMWebAPI.ActionFilters;
using TRMWebAPI.HelperClasses;

namespace TRMWebAPI.Controllers
{
    [BasicAuthentication]
    public class MaterialController : ApiController
    {
        // GET api/Material
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                return Ok(db.Materials.ToList());
            }
        }

        // GET api/Material/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                //get the Customer with the proper id
                var c = db.Materials.Where(x => x.Id == id).First();
                //check if Customer found, if not return badRequest otherwise return found Customer
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/Material
        public IHttpActionResult Post([FromBody]Material Material)
        {
            using (var db = new TrmContext())
            {
                if (Material == null) return BadRequest();
                db.Materials.Add(Material);
                db.SaveChanges();
                return Ok(Material.Id);
            }

        }

        // PUT api/Material/5
        public IHttpActionResult Put(int id, [FromBody]Material Material)
        {
            using (var db = new TrmContext())
            {
                if (Material == null) return BadRequest();
                var c = db.Materials.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                Material.Id = id;
                c = Material;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/Material/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.Materials.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                db.Materials.Remove(c);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
