﻿using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using TrmDb;
using TrmDb.Models;
using TRMWebAPI.ActionFilters;
using TRMWebAPI.HelperClasses;
using TRMWebAPI.ModelsWithPictures;

namespace TRMWebAPI.Controllers
{
    /*
     * To whomever decides to work with this API:
     * I am sorry, this is not a very clean project. There is pretty much no documentation and there is a lot of stuff which is already obsolete but still available.
     * Other than that it is not programmend very well either.
     * As already mentioned, I am sorry you have to work with this piece of shit. Hope you wont get to depressed with it also when worse comes to worse there is
     * always the possibility of making a new API (might even be the best way I should have done myself).
     * Best regards,
     * former dev of this API. 
     */

    [BasicAuthentication]
    public class ReportController : ApiController
    {
        // GET api/Report
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                var e = db.Reports.ToList();
                List<ReportWithPic> r = new List<ReportWithPic>();
                foreach (var i in e)
                {
                    r.Add(new ReportWithPic(i));
                }
                return Ok(r);
            }
        }

        // GET api/Report/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.Reports.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                ReportWithPic e = new ReportWithPic(c);
                return Ok(e);
            }
        }

        // POST api/Report
        public IHttpActionResult Post([FromBody]ReportWithPic report)
        {
            using (var db = new TrmContext())
            {
                if (report == null) return BadRequest();
                Report e = report.ParseToReport();
                db.Reports.Add(e);
                db.SaveChanges();
                return Ok(e.Id);
            }

        }

        // PUT api/Report/5
        public IHttpActionResult Put(int id, [FromBody]ReportWithPic report)
        {
            using (var db = new TrmContext())
            {
                if (report == null) return BadRequest();
                var e = db.Reports.Where(x => x.Id == id).First();
                if (e == null) return BadRequest();

                //might not be perfect way to do it since if the picture does not change it will be saved a second time
                //but i dont care right now ¯\_(ツ)_/¯
                Report y = report.ParseToReport();
                y.Id = id;
                e = y;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/Report/5
        public IHttpActionResult Delete(int id)
        {
            //string checkedId = CheckIdHelper.GetOkId(id + "");
            //if (checkedId != null)
            //{
            //    //delete tool pics
            //    var x = TrmConnection.DoStatement($"SELECT * FROM toollist WHERE report={id}");
            //    var tools = IterateDatatable.Iterate(x, new Toollist());

            //    foreach (var t in tools)
            //    {
            //        TrmConnection.DoStatementWithNoReturn($"DELETE FROM toolpicture WHERE tool_list={t.id}");
            //        var valuepages = IterateDatatable.Iterate(TrmConnection.DoStatement($"SELECT * FROM toolvaluepage WHERE tool_list={t.id}"), new Toolvaluepage());
            //        foreach (var v in valuepages)
            //        {
            //            TrmConnection.DoStatementWithNoReturn($"DELETE FROM toolvaluepicture WHERE tool_value_page={v.id}");
            //            TrmConnection.DoStatementWithNoReturn($"DELETE FROM toolvaluepage WHERE id={v.id}");
            //        }
            //    }

            //    TrmConnection.DoStatementWithNoReturn($"DELETE FROM toollist WHERE report={id}");
            //    TrmConnection.DoStatementWithNoReturn($"DELETE FROM picture WHERE report={id}");
            //    TrmConnection.DoStatementWithNoReturn(new Report().BuildStatement(Statement.DELETE, new Report(), Int32.Parse(checkedId)));


            //    return Ok();
            //}
            //return BadRequest();
            using (var db = new TrmContext())
            {
                //delete valuepages
                var v = db.Toolvaluepages.Where(x => x.ToollistItem.Report.Id == id);
                db.Toolvaluepages.RemoveRange(v);
                //delete toolitems
                var t = db.ToollistItems.Where(x => x.Report.Id == id);
                db.ToollistItems.RemoveRange(t);
                //delete report
                var e = db.Reports.Where(x => x.Id == id).First();
                if (e == null) return BadRequest();

                db.Reports.Remove(e);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
