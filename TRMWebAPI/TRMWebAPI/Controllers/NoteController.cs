﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using TrmDb;
using TrmDb.Models;
using TRMWebAPI.ActionFilters;

namespace TRMWebAPI.Controllers
{
    [BasicAuthentication]
    public class NoteController : ApiController
    {
        // GET api/Note
        public IHttpActionResult Get()
        {
            try
            {
                using (var db = new TrmContext())
                {
                    var e = db.Notes.ToList();
                    return Ok(e);
                }
            }
            catch (Exception e)
            {
                var x = new HttpRequestMessage();
                x.Content = new StringContent(e.Message);
                return new NotFoundResult(x);
            }
        }

        // GET api/Note/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                using (var db = new TrmContext())
                {
                    var c = db.Notes.Where(x => x.Id == id).First();
                    if (c == null) return BadRequest();
                    return Ok(c);
                }
            }
            catch (Exception e)
            {
                var x = new HttpRequestMessage();
                x.Content = new StringContent(e.Message);
                return new NotFoundResult(x);
            }
        }

        // POST api/Note
        public IHttpActionResult Post([FromBody]Note Note)
        {
            try
            {
                using (var db = new TrmContext())
                {
                    if (Note == null) return BadRequest();
                    db.Notes.Add(Note);
                    db.SaveChanges();
                    return Ok(Note.Id);
                }
            }
            catch (Exception e)
            {
                var x = new HttpRequestMessage();
                x.Content = new StringContent(e.Message);
                return new NotFoundResult(x);
            }

        }

        // PUT api/Note/5
        public IHttpActionResult Put(int id, [FromBody]Note Note)
        {
            try
            {
                using (var db = new TrmContext())
                {
                    if (Note == null) return BadRequest();
                    var e = db.Notes.Where(x => x.Id == id).First();
                    if (e == null) return BadRequest();

                    Note.Id = id;
                    e = Note;
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception e)
            {
                var x = new HttpRequestMessage();
                x.Content = new StringContent(e.Message);
                return new NotFoundResult(x);
            }
        }

        // DELETE api/Note/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                using (var db = new TrmContext())
                {
                    var e = db.Notes.Where(x => x.Id == id).First();
                    if (e == null) return BadRequest();

                    db.Notes.Remove(e);
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception e)
            {
                var x = new HttpRequestMessage();
                x.Content = new StringContent(e.Message);
                return new NotFoundResult(x);
            }
        }
    }
}
