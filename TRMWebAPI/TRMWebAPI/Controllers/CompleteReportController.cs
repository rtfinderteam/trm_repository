﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using TrmDb;
using TrmDb.Models;
using TRMWebAPI.CompleteReportModels;

namespace TRMWebAPI.Controllers
{
    public class CompleteReportController : ApiController
    {
        // POST api/CompleteReport
        public IHttpActionResult Post([FromBody]CompleteApiReport model)
        {
            if (!ModelState.IsValid) return BadRequest();
            try
            {
                using (var db = new TrmContext())
                {
                    /*
                     * plan-->report,customer,material,engine ought to be fine
                     * toollistitem and toolvaluepage need work-->first up: make stuff in db nullable,
                     * then check with the insert of the toollistitem and toolvaluepage if any of the objects from
                     * the model have null for all of their properties except for bools and the id 
                     * -->other importance: pictures as null need special work-->:check:
                    */
                    if (model == null) return BadRequest();
                    //first save Customer and add it to the report
                    db.Customers.Add(model.Report.Customer);
                    //then save Material and add it to the report
                    db.Materials.Add(model.Report.Material);
                    //then save Engine and add it to the report
                    var engine = model.Report.Engine.ParseToEngine();
                    db.Engines.Add(engine);
                    //then save the report
                    var report = model.Report.ParseToReport();
                    db.Reports.Add(report);
                    //then save the toollistitems and toolvaluepages
                    //check for all the nulls
                    if (!model.Toollistitem1.CheckIfNull() && !model.Toolvaluepage1.CheckIfNull())
                    {
                        var toollistitem1 = model.Toollistitem1.ParseToToollistItem();
                        toollistitem1.Report = report;
                        var toolvaluepage1 = model.Toolvaluepage1.ParseToToolvaluepage();
                        toolvaluepage1.ToollistItem = toollistitem1;
                        //save em
                        db.ToollistItems.Add(toollistitem1);
                        db.Toolvaluepages.Add(toolvaluepage1);
                    }
                    if (!model.Toollistitem2.CheckIfNull() && !model.Toolvaluepage2.CheckIfNull())
                    {
                        var toollistitem2 = model.Toollistitem2.ParseToToollistItem();
                        toollistitem2.Report = report;
                        var toolvaluepage2 = model.Toolvaluepage2.ParseToToolvaluepage();
                        toolvaluepage2.ToollistItem = toollistitem2;
                        //save em
                        db.ToollistItems.Add(toollistitem2);
                        db.Toolvaluepages.Add(toolvaluepage2);
                    }
                    if (!model.Toollistitem3.CheckIfNull() && !model.Toolvaluepage3.CheckIfNull())
                    {
                        var toollistitem3 = model.Toollistitem3.ParseToToollistItem();
                        toollistitem3.Report = report;
                        var toolvaluepage3 = model.Toolvaluepage3.ParseToToolvaluepage();
                        toolvaluepage3.ToollistItem = toollistitem3;
                        //save em
                        db.ToollistItems.Add(toollistitem3);
                        db.Toolvaluepages.Add(toolvaluepage3);
                    }
                    if (!model.Toollistitem4.CheckIfNull() && !model.Toolvaluepage4.CheckIfNull())
                    {
                        var toollistitem4 = model.Toollistitem4.ParseToToollistItem();
                        toollistitem4.Report = report;
                        var toolvaluepage4 = model.Toolvaluepage4.ParseToToolvaluepage();
                        toolvaluepage4.ToollistItem = toollistitem4;
                        //save em
                        db.ToollistItems.Add(toollistitem4);
                        db.Toolvaluepages.Add(toolvaluepage4);
                    }
                    if (!model.Toollistitem5.CheckIfNull() && !model.Toolvaluepage5.CheckIfNull())
                    {
                        var toollistitem5 = model.Toollistitem5.ParseToToollistItem();
                        toollistitem5.Report = report;
                        var toolvaluepage5 = model.Toolvaluepage5.ParseToToolvaluepage();
                        toolvaluepage5.ToollistItem = toollistitem5;
                        //save em
                        db.ToollistItems.Add(toollistitem5);
                        db.Toolvaluepages.Add(toolvaluepage5);
                    }
                    if (!model.Toollistitem6.CheckIfNull() && !model.Toolvaluepage6.CheckIfNull())
                    {
                        var toollistitem6 = model.Toollistitem6.ParseToToollistItem();
                        toollistitem6.Report = report;
                        var toolvaluepage6 = model.Toolvaluepage6.ParseToToolvaluepage();
                        toolvaluepage6.ToollistItem = toollistitem6;
                        //save em
                        db.ToollistItems.Add(toollistitem6);
                        db.Toolvaluepages.Add(toolvaluepage6);
                    }
                    if (!model.Toollistitem7.CheckIfNull() && !model.Toolvaluepage7.CheckIfNull())
                    {
                        var toollistitem7 = model.Toollistitem7.ParseToToollistItem();
                        toollistitem7.Report = report;
                        var toolvaluepage7 = model.Toolvaluepage7.ParseToToolvaluepage();
                        toolvaluepage7.ToollistItem = toollistitem7;
                        //save em
                        db.ToollistItems.Add(toollistitem7);
                        db.Toolvaluepages.Add(toolvaluepage7);
                    }

                    db.SaveChanges();

                    return Ok();
                }
            }
            catch (Exception e)
            {
                var x = new HttpRequestMessage();
                x.Content = new StringContent(e.Message);
                return new NotFoundResult(x);
            }

        }
    }
}
