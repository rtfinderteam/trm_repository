﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrmDb;
using TrmDb.ToolModels;
using TRMWebAPI.ActionFilters;

namespace TRMWebAPI.Controllers.ToolControllers
{
    [BasicAuthentication]
    public class VHM_DrillController : ApiController
    {
        // GET api/VHM_Drill
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                return Ok(db.VHM_Drills.ToList());
            }
        }

        // GET api/VHM_Drill/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.VHM_Drills.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/VHM_Drill
        public IHttpActionResult Post([FromBody]VHM_Drill VHM_Drill)
        {
            using (var db = new TrmContext())
            {
                if (VHM_Drill == null) return BadRequest();
                db.VHM_Drills.Add(VHM_Drill);
                db.SaveChanges();
                return Ok(VHM_Drill.Id);
            }

        }

        // PUT api/VHM_Drill/5
        public IHttpActionResult Put(int id, [FromBody]VHM_Drill VHM_Drill)
        {
            using (var db = new TrmContext())
            {
                if (VHM_Drill == null) return BadRequest();
                var c = db.VHM_Drills.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                VHM_Drill.Id = id;
                c = VHM_Drill;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/VHM_Drill/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.VHM_Drills.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                db.VHM_Drills.Remove(c);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
