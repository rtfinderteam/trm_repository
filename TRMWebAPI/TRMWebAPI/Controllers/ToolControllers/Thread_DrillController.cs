﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrmDb;
using TrmDb.ToolModels;
using TRMWebAPI.ActionFilters;

namespace TRMWebAPI.Controllers.ToolControllers
{
    [BasicAuthentication]
    public class Thread_DrillController : ApiController
    {
        // GET api/Thread_Drill
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                return Ok(db.Thread_Drills.ToList());
            }
        }

        // GET api/Thread_Drill/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.Thread_Drills.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/Thread_Drill
        public IHttpActionResult Post([FromBody]Thread_Drill Thread_Drill)
        {
            using (var db = new TrmContext())
            {
                if (Thread_Drill == null) return BadRequest();
                db.Thread_Drills.Add(Thread_Drill);
                db.SaveChanges();
                return Ok(Thread_Drill.Id);
            }

        }

        // PUT api/Thread_Drill/5
        public IHttpActionResult Put(int id, [FromBody]Thread_Drill Thread_Drill)
        {
            using (var db = new TrmContext())
            {
                if (Thread_Drill == null) return BadRequest();
                var c = db.Thread_Drills.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                Thread_Drill.Id = id;
                c = Thread_Drill;
                db.SaveChanges();
                return Ok();
            }
        }

        // DELETE api/Thread_Drill/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.Thread_Drills.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                db.Thread_Drills.Remove(c);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
