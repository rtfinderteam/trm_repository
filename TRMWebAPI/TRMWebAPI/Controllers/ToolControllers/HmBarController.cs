﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrmDb;
using TrmDb.ToolModels;
using TRMWebAPI.ActionFilters;

namespace TRMWebAPI.Controllers.ToolControllers
{
    [BasicAuthentication]
    public class HmBarController : ApiController
    {
        // GET api/HmBar
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                return Ok(db.HmBars.ToList());
            }
        }

        // GET api/HmBar/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.HmBars.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/HmBar
        public IHttpActionResult Post([FromBody]HmBar HmBar)
        {
            using (var db = new TrmContext())
            {
                if (HmBar == null) return BadRequest();
                db.HmBars.Add(HmBar);
                db.SaveChanges();
                return Ok(HmBar.Id);
            }

        }

        // PUT api/HmBar/5
        public IHttpActionResult Put(int id, [FromBody]HmBar HmBar)
        {
            using (var db = new TrmContext())
            {
                if (HmBar == null) return BadRequest();
                var c = db.HmBars.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                HmBar.Id = id;
                c = HmBar;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/HmBar/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.HmBars.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                db.HmBars.Remove(c);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
