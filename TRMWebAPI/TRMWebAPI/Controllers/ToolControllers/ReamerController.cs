﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrmDb;
using TrmDb.ToolModels;
using TRMWebAPI.ActionFilters;

namespace TRMWebAPI.Controllers.ToolControllers
{
    [BasicAuthentication]
    public class ReamerController : ApiController
    {
        // GET api/Reamer
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                return Ok(db.Reamers.ToList());
            }
        }

        // GET api/Reamer/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.Reamers.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/Reamer
        public IHttpActionResult Post([FromBody]Reamer Reamer)
        {
            using (var db = new TrmContext())
            {
                if (Reamer == null) return BadRequest();
                db.Reamers.Add(Reamer);
                db.SaveChanges();
                return Ok(Reamer.Id);
            }

        }

        // PUT api/Reamer/5
        public IHttpActionResult Put(int id, [FromBody]Reamer Reamer)
        {
            using (var db = new TrmContext())
            {
                if (Reamer == null) return BadRequest();
                var c = db.Reamers.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                Reamer.Id = id;
                c = Reamer;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/Reamer/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.Reamers.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                db.Reamers.Remove(c);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
