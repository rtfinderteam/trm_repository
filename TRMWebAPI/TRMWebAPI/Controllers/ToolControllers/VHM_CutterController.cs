﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrmDb;
using TrmDb.ToolModels;
using TRMWebAPI.ActionFilters;

namespace TRMWebAPI.Controllers.ToolControllers
{
    [BasicAuthentication]
    public class VHM_CutterController : ApiController
    {
        // GET api/VHM_Cutter
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                return Ok(db.VHM_Cutters.ToList());
            }
        }

        // GET api/VHM_Cutter/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.VHM_Cutters.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/VHM_Cutter
        public IHttpActionResult Post([FromBody]VHM_Cutter VHM_Cutter)
        {
            using (var db = new TrmContext())
            {
                if (VHM_Cutter == null) return BadRequest();
                db.VHM_Cutters.Add(VHM_Cutter);
                db.SaveChanges();
                return Ok(VHM_Cutter.Id);
            }
        }

        // PUT api/VHM_Cutter/5
        public IHttpActionResult Put(int id, [FromBody]VHM_Cutter VHM_Cutter)
        {
            using (var db = new TrmContext())
            {
                if (VHM_Cutter == null) return BadRequest();
                var c = db.VHM_Cutters.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                VHM_Cutter.Id = id;
                c = VHM_Cutter;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/VHM_Cutter/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.VHM_Cutters.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                db.VHM_Cutters.Remove(c);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
