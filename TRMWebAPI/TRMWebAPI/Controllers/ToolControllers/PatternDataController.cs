﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrmDb;
using TrmDb.ToolModels;
using TRMWebAPI.ActionFilters;

namespace TRMWebAPI.Controllers.ToolControllers
{
    [BasicAuthentication]
    public class PatternDataController : ApiController
    {
        // GET api/PatternData
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                return Ok(db.PatternData.ToList());
            }
        }

        // GET api/PatternData/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.PatternData.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/PatternData
        public IHttpActionResult Post([FromBody]PatternData PatternData)
        {
            using (var db = new TrmContext())
            {
                if (PatternData == null) return BadRequest();
                db.PatternData.Add(PatternData);
                db.SaveChanges();
                return Ok(PatternData.Id);
            }

        }

        // PUT api/PatternData/5
        public IHttpActionResult Put(int id, [FromBody]PatternData PatternData)
        {
            using (var db = new TrmContext())
            {
                if (PatternData == null) return BadRequest();
                var c = db.PatternData.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                PatternData.Id = id;
                c = PatternData;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/PatternData/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.PatternData.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                db.PatternData.Remove(c);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
