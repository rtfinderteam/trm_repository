﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrmDb;
using TrmDb.ToolModels;
using TRMWebAPI.ActionFilters;

namespace TRMWebAPI.Controllers.ToolControllers
{
    [BasicAuthentication]
    public class ColletController : ApiController
    {
        // GET api/Collet
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                return Ok(db.Collets.ToList());
            }
        }

        // GET api/Collet/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.Collets.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/Collet
        public IHttpActionResult Post([FromBody]Collet Collet)
        {
            using (var db = new TrmContext())
            {
                if (Collet == null) return BadRequest();
                db.Collets.Add(Collet);
                db.SaveChanges();
                return Ok(Collet.Id);
            }

        }

        // PUT api/Collet/5
        public IHttpActionResult Put(int id, [FromBody]Collet Collet)
        {
            using (var db = new TrmContext())
            {
                if (Collet == null) return BadRequest();
                var c = db.Collets.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                Collet.Id = id;
                c = Collet;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/Collet/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                using (var db = new TrmContext())
                {
                    var c = db.Collets.Where(x => x.Id == id).First();
                    if (c == null) return BadRequest("No collets with this id were found.");

                    db.Collets.Remove(c);
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception e)
            {
                return NotFound();
            }


        }
    }
}
