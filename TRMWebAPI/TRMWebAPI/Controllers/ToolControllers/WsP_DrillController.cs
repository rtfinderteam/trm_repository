﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrmDb;
using TrmDb.ToolModels;
using TRMWebAPI.ActionFilters;

namespace TRMWebAPI.Controllers.ToolControllers
{
    [BasicAuthentication]
    public class WsP_DrillController : ApiController
    {
        // GET api/WsP_Drill
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                return Ok(db.WsP_Drills.ToList());
            }
        }

        // GET api/WsP_Drill/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.WsP_Drills.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/WsP_Drill
        public IHttpActionResult Post([FromBody]WsP_Drill WsP_Drill)
        {
            using (var db = new TrmContext())
            {
                if (WsP_Drill == null) return BadRequest();
                db.WsP_Drills.Add(WsP_Drill);
                db.SaveChanges();
                return Ok(WsP_Drill.Id);
            }

        }

        // PUT api/WsP_Drill/5
        public IHttpActionResult Put(int id, [FromBody]WsP_Drill WsP_Drill)
        {
            using (var db = new TrmContext())
            {
                if (WsP_Drill == null) return BadRequest();
                var c = db.WsP_Drills.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                WsP_Drill.Id = id;
                c = WsP_Drill;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/WsP_Drill/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.WsP_Drills.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                db.WsP_Drills.Remove(c);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
