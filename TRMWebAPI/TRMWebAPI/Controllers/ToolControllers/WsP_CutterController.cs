﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrmDb;
using TrmDb.ToolModels;
using TRMWebAPI.ActionFilters;

namespace TRMWebAPI.Controllers.ToolControllers
{
    [BasicAuthentication]
    public class WsP_CutterController : ApiController
    {
        // GET api/WsP_Cutter
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                return Ok(db.WsP_Cutters.ToList());
            }
        }

        // GET api/WsP_Cutter/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.WsP_Cutters.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/WsP_Cutter
        public IHttpActionResult Post([FromBody]WsP_Cutter WsP_Cutter)
        {
            using (var db = new TrmContext())
            {
                if (WsP_Cutter == null) return BadRequest();
                db.WsP_Cutters.Add(WsP_Cutter);
                db.SaveChanges();
                return Ok(WsP_Cutter.Id);
            }

        }

        // PUT api/WsP_Cutter/5
        public IHttpActionResult Put(int id, [FromBody]WsP_Cutter WsP_Cutter)
        {
            using (var db = new TrmContext())
            {
                if (WsP_Cutter == null) return BadRequest();
                var c = db.WsP_Cutters.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                WsP_Cutter.Id = id;
                c = WsP_Cutter;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/WsP_Cutter/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.WsP_Cutters.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                db.WsP_Cutters.Remove(c);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
