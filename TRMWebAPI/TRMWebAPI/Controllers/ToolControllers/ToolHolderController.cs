﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrmDb;
using TrmDb.ToolModels;
using TRMWebAPI.ActionFilters;

namespace TRMWebAPI.Controllers.ToolControllers
{
    [BasicAuthentication]
    public class ToolHolderController : ApiController
    {
        // GET api/ToolHolder
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                return Ok(db.ToolHolders.ToList());
            }
        }

        // GET api/ToolHolder/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.ToolHolders.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/ToolHolder
        public IHttpActionResult Post([FromBody]ToolHolder ToolHolder)
        {
            using (var db = new TrmContext())
            {
                if (ToolHolder == null) return BadRequest();
                db.ToolHolders.Add(ToolHolder);
                db.SaveChanges();
                return Ok(ToolHolder.Id);
            }

        }

        // PUT api/ToolHolder/5
        public IHttpActionResult Put(int id, [FromBody]ToolHolder ToolHolder)
        {
            using (var db = new TrmContext())
            {
                if (ToolHolder == null) return BadRequest();
                var c = db.ToolHolders.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                ToolHolder.Id = id;
                c = ToolHolder;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/ToolHolder/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.ToolHolders.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                db.ToolHolders.Remove(c);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
