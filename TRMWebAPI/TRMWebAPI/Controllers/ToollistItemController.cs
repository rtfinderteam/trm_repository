﻿using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using TrmDb;
using TrmDb.Models;
using TRMWebAPI.ActionFilters;
using TRMWebAPI.HelperClasses;
using TRMWebAPI.ModelsWithPictures;

namespace TRMWebAPI.Controllers
{
    [BasicAuthentication]
    public class ToollistItemController : ApiController
    {
        // GET api/ToollistItem
        public IHttpActionResult Get()
        {
            using (var db = new TrmContext())
            {
                var e = db.ToollistItems.ToList();
                List<ToollistItemWithPic> r = new List<ToollistItemWithPic>();
                foreach (var i in e)
                {
                    r.Add(new ToollistItemWithPic(i));
                }
                return Ok(r);
            }
        }

        // GET api/ToollistItem/5
        public IHttpActionResult Get(int id)
        {
            using (var db = new TrmContext())
            {
                var c = db.ToollistItems.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();
                ToollistItemWithPic e = new ToollistItemWithPic(c);
                return Ok(e);
            }
        }

        // POST api/ToollistItem
        public IHttpActionResult Post([FromBody]ToollistItemWithPic toollistitem)
        {
            using (var db = new TrmContext())
            {
                if (toollistitem == null) return BadRequest();
                ToollistItem e = toollistitem.ParseToToollistItem();
                db.ToollistItems.Add(e);
                db.SaveChanges();
                return Ok(e.Id);
            }

        }

        // PUT api/ToollistItem/5
        public IHttpActionResult Put(int id, [FromBody]ToollistItemWithPic toollistitem)
        {
            using (var db = new TrmContext())
            {
                if (toollistitem == null) return BadRequest();
                var e = db.ToollistItems.Where(x => x.Id == id).First();
                if (e == null) return BadRequest();

                //might not be perfect way to do it since if the picture does not change it will be saved a second time
                //but i dont care right now ¯\_(ツ)_/¯
                ToollistItem y = toollistitem.ParseToToollistItem();
                y.Id = id;
                e = y;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/ToollistItem/5
        public IHttpActionResult Delete(int id)
        {
            using (var db = new TrmContext())
            {
                var e = db.Reports.Where(x => x.Id == id).First();
                if (e == null) return BadRequest();

                db.Reports.Remove(e);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
