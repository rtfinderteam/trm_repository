﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using TrmDb;
using TrmDb.Models;
using TRMWebAPI.ActionFilters;
using TRMWebAPI.HelperClasses;
using TRMWebAPI.ModelsWithPictures;

namespace TRMWebAPI.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/search")]
    public class SearchController : ApiController
    {
        [HttpPost]
        [Route("getReport")]
        public IHttpActionResult GetReport([FromBody]Search searchParams)
        {
            try
            {
                using (var db = new TrmContext())
                {
                    List<Report> result = db.Reports.ToList();
                    //einzelene teile der searchParams durchgehen bzw. checken ob sie null od. "" sind
                    //customer
                    if (searchParams.S_Customer != null)
                    {
                        if (searchParams.S_Customer.name != "" && searchParams.S_Customer.name != null)
                        {
                            result = result.Where(x => x.Customer.Name.ContainsNonCase(searchParams.S_Customer.name, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                        if (searchParams.S_Customer.branch != "" && searchParams.S_Customer.branch != null)
                        {
                            result = result.Where(x => x.Customer.Branch.ContainsNonCase(searchParams.S_Customer.branch, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                    }
                    //engine
                    if (searchParams.S_Engine != null)
                    {
                        if (searchParams.S_Engine.manufacture != "" && searchParams.S_Engine.manufacture != null)
                        {
                            result = result.Where(x => x.Engine.Manufacturer.ContainsNonCase(searchParams.S_Engine.manufacture, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                        if (searchParams.S_Engine.spindel != "" && searchParams.S_Engine.spindel != null)
                        {
                            result = result.Where(x => x.Engine.Spindle.ContainsNonCase(searchParams.S_Engine.spindel, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                        if (searchParams.S_Engine.typ != "" && searchParams.S_Engine.typ != null)
                        {
                            result = result.Where(x => x.Engine.Type.ContainsNonCase(searchParams.S_Engine.typ, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                    }
                    //Material
                    if (searchParams.S_Material != null)
                    {
                        if (searchParams.S_Material.designation != "" && searchParams.S_Material.designation != null)
                        {
                            result = result.Where(x => x.Material.Designation.ContainsNonCase(searchParams.S_Material.designation, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                        if (searchParams.S_Material.dincode != "" && searchParams.S_Material.dincode != null)
                        {
                            result = result.Where(x => x.Material.Dincode.ToString().ContainsNonCase(searchParams.S_Material.dincode, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                        if (searchParams.S_Material.encode != "" && searchParams.S_Material.encode != null)
                        {
                            result = result.Where(x => x.Material.Encode.ToString().ContainsNonCase(searchParams.S_Material.encode, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                        if (searchParams.S_Material.hardness != "" && searchParams.S_Material.hardness != null)
                        {
                            result = result.Where(x => x.Material.HardnessHB.ContainsNonCase(searchParams.S_Material.hardness, StringComparison.OrdinalIgnoreCase)
                            || x.Material.HardnessHRC.ContainsNonCase(searchParams.S_Material.hardness, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                        if (searchParams.S_Material.materialgroup != "" && searchParams.S_Material.materialgroup != null)
                        {
                            result = result.Where(x => x.Material.Materialgroup.ContainsNonCase(searchParams.S_Material.materialgroup, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                    }
                    //report
                    if (searchParams.S_Report != null)
                    {
                        if (searchParams.S_Report.employeeshort != "" && searchParams.S_Report.employeeshort != null)
                        {
                            result = result.Where(x => x.Employeeshort.ContainsNonCase(searchParams.S_Report.employeeshort, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                        if (searchParams.S_Report.strength != "" && searchParams.S_Report.strength != null)
                        {
                            result = result.Where(x => x.Strength.ContainsNonCase(searchParams.S_Report.strength, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                    }
                    //cast to pic-Version nicht vergessen!
                    List<ReportWithPic> r = new List<ReportWithPic>();
                    foreach (var i in result)
                    {
                        r.Add(new ReportWithPic(i));
                    }
                    return Ok(r);
                }
            }
            catch (Exception e)
            {
                var x = new HttpRequestMessage();
                x.Content = new StringContent(e.Message);
                return new NotFoundResult(x);
            }

        }
    }
}