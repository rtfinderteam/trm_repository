﻿using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using TRMWebAPI.ActionFilters;
using TRMWebAPI.HelperClasses;
using TrmDb;
using TrmDb.Models;

namespace TRMWebAPI.Controllers
{
    [BasicAuthentication]
    public class CustomerController : ApiController
    {
        // GET api/Customer
        public IHttpActionResult Get()
        {
            //var x = TrmConnection.DoStatement(new Customer().BuildStatement(Statement.SELECT, new Customer()));
            using (var db = new TrmContext())
            {
                //db.Customers.Add(new TrmDb.Models.Customer { Name = "Max Mustermann", Branch = "left", Contactpartner = "none" });
                //db.SaveChanges();
                return Ok(db.Customers.ToList());
            }
            // return Ok(IterateDatatable.Iterate(x, new Customer()));
        }

        // GET api/Customer/5
        public IHttpActionResult Get(int id)
        {
            //string checkedId = CheckIdHelper.GetOkId(id + "");
            //if (checkedId != null)
            //{
            //    var x = TrmConnection.DoStatement(new Customer().BuildStatement(Statement.SELECT, new Customer(), Int32.Parse(checkedId)));
            //    return Ok(IterateDatatable.Iterate(x, new Customer()));
            //}
            //return BadRequest();

            using (var db = new TrmContext())
            {
                //get the Customer with the proper id
                var c = db.Customers.Where(x => x.Id == id).First();
                //check if Customer found, if not return badRequest otherwise return found Customer
                if (c == null) return BadRequest();
                return Ok(c);
            }
        }

        // POST api/Customer
        public IHttpActionResult Post([FromBody]Customer Customer)
        {
            //if (Customer != null)
            //{
            //    DataTable dt = TrmConnection.DoStatement(Customer.BuildStatement<Customer>(Statement.INSERT, Customer));
            //    return Ok(Int32.Parse(dt.Rows[0][0].ToString()));
            //}
            //return BadRequest();

            using (var db = new TrmContext())
            {
                if (Customer == null) return BadRequest();
                db.Customers.Add(Customer);
                db.SaveChanges();
                return Ok(Customer.Id);
            }

        }

        // PUT api/Customer/5
        public IHttpActionResult Put(int id, [FromBody]Customer Customer)
        {
            //string checkedId = CheckIdHelper.GetOkId(id + "");
            //if (checkedId != null && Customer != null && Customer.id + "" == checkedId)
            //{
            //    TrmConnection.DoStatementWithNoReturn(Customer.BuildStatement(Statement.UPDATE, Customer, id));
            //    return Ok();
            //}
            //return BadRequest();

            using (var db = new TrmContext())
            {
                if (Customer == null) return BadRequest();
                var c = db.Customers.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                Customer.Id = id;
                c = Customer;
                db.SaveChanges();
                return Ok();
            }

        }

        // DELETE api/Customer/5
        public IHttpActionResult Delete(int id)
        {
            //string checkedId = CheckIdHelper.GetOkId(id + "");
            //if (checkedId != null)
            //{
            //    TrmConnection.DoStatementWithNoReturn(new Customer().BuildStatement(Statement.DELETE, new Customer(), Int32.Parse(checkedId)));
            //    return Ok();
            //}
            //return BadRequest();
            using (var db = new TrmContext())
            {
                var c = db.Customers.Where(x => x.Id == id).First();
                if (c == null) return BadRequest();

                db.Customers.Remove(c);
                db.SaveChanges();
                return Ok();
            }
        }
    }
}
