﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TRMWebAPI.HelperClasses;

using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;

using org.xml.sax;
using Fonet;
using static com.sun.org.apache.xerces.@internal.impl.XMLDocumentFragmentScannerImpl;
using System.Xml.Linq;
using System.IO;
using iTextSharp.text.pdf;
using TRMWebAPI.ActionFilters;
using iText;
using iText.Html2pdf;
using java.io;
using System.Reflection;
using System.Text.RegularExpressions;
using TrmDb;
using TrmDb.Models;
using System.Net.Http;
using System.Web.Http.Results;

namespace TRMWebAPI.Controllers
{
    [RoutePrefix("api/pdf")]
    //[BasicAuthentication]
    public class PdfController : ApiController
    {
        #region old
        //[HttpGet]
        //[Route("getPdf/{reportId}")]
        //public IHttpActionResult GetPdf(int reportId)
        //{
        //    Report r = IterateDatatable.Iterate(TrmConnection.DoStatement(new Report().BuildStatement(Statement.SELECT, new Report(), reportId)), new Report()).First();
        //    Material m = IterateDatatable.Iterate(TrmConnection.DoStatement(new Material().BuildStatement(Statement.SELECT, new Material(), r.material)), new Material()).First();
        //    Engine e = IterateDatatable.Iterate(TrmConnection.DoStatement(new Engine().BuildStatement(Statement.SELECT, new Engine(), r.engine)), new Engine()).First();
        //    Customer c = IterateDatatable.Iterate(TrmConnection.DoStatement(new Customer().BuildStatement(Statement.SELECT, new Customer(), r.customer)), new Customer()).First();
        //    List<Picture> pictures = IterateDatatable.Iterate(TrmConnection.DoStatement($"SELECT * FROM picture WHERE report = {r.id}"), new Picture());
        //    List<Toollist> tl = IterateDatatable.Iterate(TrmConnection.DoStatement($"SELECT * FROM toollist WHERE report = {r.id}"), new Toollist());
        //    List<ToolData> td = new List<ToolData>();
        //    foreach (var item in tl)
        //    {
        //        //1) ToolValuePage (ez)
        //        Toolvaluepage tvpg = IterateDatatable.Iterate(TrmConnection.DoStatement($"SELECT * FROM toolvaluepage WHERE tool_list = {item.id}"), new Toolvaluepage()).First();
        //        //2) ToolValuePictures (mz)
        //        List<Toolvaluepicture> tvpc = IterateDatatable.Iterate(TrmConnection.DoStatement($"SELECT * FROM toolvaluepicture WHERE tool_value_page = {tvpg.id}"), new Toolvaluepicture());
        //        //3) Toolpicture (ez)
        //        Toolpicture tp = IterateDatatable.Iterate(TrmConnection.DoStatement($"SELECT * FROM toolpicture WHERE tool_list = {item.id}"), new Toolpicture()).First();
        //        //4) ManufacturePicture (ez)
        //        Manufacturepicture mp = IterateDatatable.Iterate(TrmConnection.DoStatement(new Manufacturepicture().BuildStatement(Statement.SELECT, new Manufacturepicture(), item.manufacture_picture)), new Manufacturepicture()).First();
        //        //dann neues Tooldata
        //        td.Add(new ToolData
        //        {
        //            Toollist = item,
        //            Toolvaluepage = tvpg,
        //            Toolpicture = tp,
        //            Toolvaluepictures = tvpc,
        //            Manufacturepicture = mp
        //        });
        //    }
        //    byte[] bytePdf = MakePdf(r, m, e, c, pictures, td);

        //    return Ok(bytePdf);
        //}

        //private byte[] MakePdf(Report r, Material m, Engine e, Customer c, List<Picture> pictures, List<ToolData> td)
        //{
        //    string pdfPath = HelperClasses.PdfName.GetUniquePdfName();

        //    FileStream fs = new FileStream(pdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
        //    Document doc = new Document();

        //    PdfWriter writer = PdfWriter.GetInstance(doc, fs);

        //    doc.Open();
        //    Font titleFont = FontFactory.GetFont("Arial", 40, Font.BOLD);
        //    Font headingFont = FontFactory.GetFont("Arial", 32);

        //    Paragraph p = new Paragraph("Report Datasheet", titleFont);
        //    p.SpacingAfter = 30;
        //    doc.Add(p);

        //    p = new Paragraph("Report", headingFont);
        //    p.SpacingAfter = 30;
        //    doc.Add(p);
        //    PdfPTable t = new PdfPTable(2);
        //    foreach (var item in r.GetType().GetProperties())
        //    {
        //        PdfPCell cell = new PdfPCell();
        //        cell.AddElement(new Chunk(item.Name));
        //        t.AddCell(cell);
        //        cell = new PdfPCell();
        //        cell.AddElement(new Chunk((String)item.GetValue(r).ToString()));
        //        t.AddCell(cell);
        //    }
        //    doc.Add(t);
        //    doc.NewPage();

        //    p = new Paragraph("Material", headingFont);
        //    p.SpacingAfter = 30;
        //    doc.Add(p);
        //    t = new PdfPTable(2);
        //    foreach (var item in m.GetType().GetProperties())
        //    {
        //        PdfPCell cell = new PdfPCell();
        //        cell.AddElement(new Chunk(item.Name));
        //        t.AddCell(cell);
        //        cell = new PdfPCell();
        //        cell.AddElement(new Chunk((String)item.GetValue(m).ToString()));
        //        t.AddCell(cell);
        //    }
        //    doc.Add(t);
        //    doc.NewPage();

        //    p = new Paragraph("Engine", headingFont);
        //    p.SpacingAfter = 30;
        //    doc.Add(p);
        //    t = new PdfPTable(2);
        //    foreach (var item in e.GetType().GetProperties())
        //    {
        //        PdfPCell cell = new PdfPCell();
        //        if (item.Name == "path") continue;
        //        if (item.Name == "picturename")
        //        {
        //            cell = new PdfPCell();
        //            cell.AddElement(new Chunk((String)item.GetValue(e).ToString()));
        //            t.AddCell(cell);

        //            cell = new PdfPCell();
        //            iTextSharp.text.Image pic = Image.GetInstance(FtpHelper.GetImage(item.GetValue(e).ToString()));
        //            cell.AddElement(pic);
        //            t.AddCell(cell);
        //        }
        //        else
        //        {
        //            cell.AddElement(new Chunk(item.Name));
        //            t.AddCell(cell);
        //            cell = new PdfPCell();
        //            cell.AddElement(new Chunk((String)item.GetValue(e).ToString()));
        //            t.AddCell(cell);
        //        }
        //    }
        //    doc.Add(t);
        //    doc.NewPage();

        //    p = new Paragraph("Customer", headingFont);
        //    p.SpacingAfter = 30;
        //    doc.Add(p);
        //    t = new PdfPTable(2);
        //    foreach (var item in c.GetType().GetProperties())
        //    {
        //        PdfPCell cell = new PdfPCell();
        //        cell.AddElement(new Chunk(item.Name));
        //        t.AddCell(cell);
        //        cell = new PdfPCell();
        //        cell.AddElement(new Chunk((String)item.GetValue(c).ToString()));
        //        t.AddCell(cell);
        //    }
        //    doc.Add(t);
        //    doc.NewPage();

        //    p = new Paragraph("Pictures", headingFont);
        //    p.SpacingAfter = 30;
        //    doc.Add(p);
        //    t = new PdfPTable(2);
        //    foreach (var x in pictures)
        //    {
        //        PdfPCell cell = new PdfPCell();
        //        cell.AddElement(new Chunk(x.name));
        //        t.AddCell(cell);


        //        cell = new PdfPCell();
        //        Image pic = Image.GetInstance(FtpHelper.GetImage(x.name));

        //        cell.AddElement(pic);
        //        t.AddCell(cell);
        //    }
        //    doc.Add(t);
        //    doc.NewPage();

        //    p = new Paragraph("ToolData", headingFont);
        //    p.SpacingAfter = 30;
        //    doc.Add(p);
        //    Font lowerFont = FontFactory.GetFont("Arial", 20);

        //    foreach (var d in td)
        //    {
        //        //Toollist
        //        //(manufacuter-pic and tool-pic)
        //        p = new Paragraph("Toollist", lowerFont);
        //        p.SpacingAfter = 20;
        //        doc.Add(p);
        //        t = new PdfPTable(2);
        //        foreach (var x in d.Toollist.GetType().GetProperties())
        //        {
        //            PdfPCell cell = new PdfPCell();

        //            if (x.Name == "manufacture_picture")
        //            {
        //                cell.AddElement(new Chunk("Manufacture-Picture: " + d.Manufacturepicture.name));
        //                t.AddCell(cell);
        //                cell = new PdfPCell();
        //                Image img = Image.GetInstance(FtpHelper.GetImage(d.Manufacturepicture.name));
        //                cell.AddElement(img);
        //                t.AddCell(cell);
        //                continue;
        //            }

        //            cell.AddElement(new Chunk(x.Name));
        //            t.AddCell(cell);
        //            cell = new PdfPCell();
        //            cell.AddElement(new Chunk((String)x.GetValue(d.Toollist).ToString() + ""));
        //            t.AddCell(cell);
        //        }
        //        //toolpic
        //        PdfPCell toolPicName = new PdfPCell();
        //        toolPicName.AddElement(new Chunk("Tool-Picture: " + d.Toolpicture.name));
        //        t.AddCell(toolPicName);
        //        toolPicName = new PdfPCell();
        //        Image pic = Image.GetInstance(FtpHelper.GetImage(d.Toolpicture.name));
        //        toolPicName.AddElement(pic);
        //        t.AddCell(toolPicName);
        //        doc.Add(t);

        //        //Toolvaluepage
        //        p = new Paragraph("ToolValuePage", lowerFont);
        //        p.SpacingAfter = 20;
        //        doc.Add(p);
        //        t = new PdfPTable(2);
        //        foreach (var item in d.Toolvaluepage.GetType().GetProperties())
        //        {
        //            PdfPCell cell = new PdfPCell();
        //            cell.AddElement(new Chunk(item.Name));
        //            t.AddCell(cell);
        //            cell = new PdfPCell();
        //            cell.AddElement(new Chunk((String)item.GetValue(d.Toolvaluepage).ToString()));
        //            t.AddCell(cell);
        //        }
        //        doc.Add(t);
        //        //toolvaluepictures
        //        p = new Paragraph("ToolValuePictures", lowerFont);
        //        p.SpacingAfter = 20;
        //        doc.Add(p);
        //        t = new PdfPTable(2);
        //        foreach (var item in d.Toolvaluepictures)
        //        {
        //            PdfPCell cell = new PdfPCell();
        //            cell.AddElement(new Chunk(item.name));
        //            t.AddCell(cell);

        //            cell = new PdfPCell();
        //            Image y = Image.GetInstance(FtpHelper.GetImage(item.name));

        //            cell.AddElement(y);
        //            t.AddCell(cell);
        //        }

        //    }
        //    doc.Close();

        //    return System.IO.File.ReadAllBytes(pdfPath);

        //    //still need to return stuff

        //    //xml and xsl to fo doesnt work very well-->just gonna do it with itextsharp by myself
        //    // Load the FO style sheet.
        //    //XslCompiledTransform xslt = new XslCompiledTransform();

        //    //xslt.Load(System.AppDomain.CurrentDomain.BaseDirectory+@"\PdfXML\BookFo.xsl");
        //    //xslt.Transform(System.AppDomain.CurrentDomain.BaseDirectory + @"\PdfXML\books.xml", System.AppDomain.CurrentDomain.BaseDirectory + @"\PdfXML\books.fo");
        //    //GeneratePDF();
        //    // Execute the transform and output the results to a file.

        //}
        #endregion

        [HttpGet]
        [Route("getPdf/{reportId}")]
        public IHttpActionResult GetPdf(int reportId)
        {
            using (var db = new TrmContext())
            {
                //inserting demo data to db for testing
                //Customer c = new Customer { Name = "Demo", Branch = "Demo", Contactpartner = "DemoMan" };
                //Material m = new Material { Encode = "EncodeCODE", Designation = "Yeah." };
                //Engine e = new Engine { Activationyear = 1999, Manufacturer = "That guy.", EnginePicturename = "grumpy_cat_0.jpg", Type = "good one" };
                //Report rep = new Report { Date = new DateTime(), Customer = c, Engine = e, Material = m, Projectname = "TestPdf", ReportPicturename1 = "grumpy_cat_0.jpg" };
                //db.Customers.Add(c);
                //db.Materials.Add(m);
                //db.Engines.Add(e);
                //db.Reports.Add(rep);

                //ToollistItem tli = new ToollistItem { Diameter = 5, Report = rep, ToolPicturename = "grumpy_cat_0.jpg", Cuttingedges = 5, Flutes = 6 };
                //Toolvaluepage tvp = new Toolvaluepage { ToollistItem = tli, ActualAxial = 5, ExpectedAxial = 5, ToolvaluepagePicturename1 = "grumpy_cat_0.jpg", ToolvaluepagePicturename2 = "grumpy_cat_0.jpg" };
                //db.ToollistItems.Add(tli);
                //db.Toolvaluepages.Add(tvp);

                //db.SaveChanges();
                try
                {
                    //find the report
                    var r = db.Reports.Where(x => x.Id == reportId).First();
                    List<Toolvaluepage> tools = db.Toolvaluepages.Where(x => x.ToollistItem.Report.Id == reportId).ToList();
                    //Return badRequest if not found
                    if (r == null) BadRequest();
                    //read the format and make it to a string
                    using (StreamReader sr = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "/PDFs/PdfFormat.html"))
                    {
                        String pdfString = sr.ReadToEnd();
                        //fill in all the proper values
                        #region easyFillin
                        //customer stuff
                        pdfString = pdfString.Replace("#CustomerName", r.Customer.Name);
                        pdfString = pdfString.Replace("#CustomerContactpartner", r.Customer.Contactpartner);
                        pdfString = pdfString.Replace("#CustomerBranch", r.Customer.Branch);
                        pdfString = pdfString.Replace("#ReportProjectname", r.Projectname);

                        //engine stuff
                        pdfString = pdfString.Replace("#EngineManufacturer", r.Engine.Manufacturer);
                        pdfString = pdfString.Replace("#EngineType", r.Engine.Type);
                        pdfString = pdfString.Replace("#EngineSpindle", r.Engine.Spindle);
                        pdfString = pdfString.Replace("#EngineCoolantoutside", r.Engine.Coolantoutside.ToString());
                        pdfString = pdfString.Replace("#EngineCoolantinside", r.Engine.Coolantinside.ToString());
                        pdfString = pdfString.Replace("#EnginePower", r.Engine.Power.ToString());
                        pdfString = pdfString.Replace("#EngineRotationalspeed", r.Engine.Rotationalspeed.ToString());
                        pdfString = pdfString.Replace("#EngineFeed", r.Engine.Feed.ToString());
                        pdfString = pdfString.Replace("#EngineActivationyear", r.Engine.Activationyear.ToString());
                        pdfString = pdfString.Replace("#EngineHourlyrate", r.Engine.Hourlyrate.ToString());
                        pdfString = pdfString.Replace("#ReportEngineInfo", r.EngineInfo);
                        if (r.Engine.EnginePicturename != null)
                        {
                            pdfString = pdfString.ReplaceFirst("#EnginePicture", Globals.PicturePath + r.Engine.EnginePicturename);
                        }
                        else
                        {
                            pdfString = pdfString.ReplaceFirst("#EnginePicture", "");

                        }
                        //Material stuff
                        pdfString = pdfString.Replace("#MaterialDesignation", r.Material.Designation);
                        //insert preferably DinCode, otherwise Encode
                        pdfString = pdfString.Replace("#MaterialDinEnCode", r.Material.Dincode != "" ? r.Material.Dincode : r.Material.Encode);
                        pdfString = pdfString.Replace("#ReportStrength", r.Strength);
                        pdfString = pdfString.Replace("#MaterialHardnessHRC", r.Material.HardnessHRC);
                        pdfString = pdfString.Replace("#MaterialHardnessHB", r.Material.HardnessHB);
                        pdfString = pdfString.Replace("#ReportBuildingcomponent", r.Buildingcomponent);
                        pdfString = pdfString.Replace("#ReportWorkpiececlamping", r.Workpiececlamping);
                        //i dont like these percents D:
                        pdfString = pdfString.Replace("#MaterialCRpercent", r.Material.CRpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialSIpercent", r.Material.SIpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialNIpercent", r.Material.NIpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialCUpercent", r.Material.CUpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialNBpercent", r.Material.NBpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialCEpercent", r.Material.CEpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialZNpercent", r.Material.ZNpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialWpercent", r.Material.Wpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialMNpercent", r.Material.MNpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialVpercent", r.Material.Vpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialTIpercent", r.Material.TIpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialCOpercent", r.Material.COpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialALpercent", r.Material.ALpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialBpercent", r.Material.Bpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialSpercent", r.Material.Spercent.ToString());
                        pdfString = pdfString.Replace("#MaterialNpercent", r.Material.Npercent.ToString());
                        pdfString = pdfString.Replace("#MaterialMOpercent", r.Material.MOpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialPBpercent", r.Material.PBpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialFEpercent", r.Material.FEpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialCpercent", r.Material.Cpercent.ToString());
                        pdfString = pdfString.Replace("#MaterialMGpercent", r.Material.MGpercent.ToString());
                        pdfString = pdfString.Replace("#ReportMaterialInfo", r.MaterialInfo);

                        #endregion
                        //Werkstücke stuff
                        //check first if picturename is not null
                        if (r.ReportPicturename1 != null)
                        {
                            pdfString = pdfString.ReplaceFirst("#ReportPicture1", Globals.PicturePath + r.ReportPicturename1);
                        }
                        else
                        {
                            pdfString = pdfString.ReplaceFirst("#ReportPicture1", "");
                        }
                        if (r.ReportPicturename2 != null)
                        {
                            pdfString = pdfString.ReplaceFirst("#ReportPicture2", Globals.PicturePath + r.ReportPicturename2);
                        }
                        else
                        {
                            pdfString = pdfString.ReplaceFirst("#ReportPicture2", "");
                        }
                        if (r.ReportPicturename3 != null)
                        {
                            pdfString = pdfString.ReplaceFirst("#ReportPicture3", Globals.PicturePath + r.ReportPicturename3);
                        }
                        else
                        {
                            pdfString = pdfString.ReplaceFirst("#ReportPicture3", "");
                        }

                        pdfString = pdfString.Replace("#ReportClampingInfo", r.ClampingInfo);
                        string job;
                        for (int i = 0; i < 7; i++)
                        {
                            //stop if no tools left
                            if (tools.Count() <= i) { break; }
                            //wenn kein tool gefunden-->skip
                            if (tools[i] == null) continue;

                            //count from 0 to 6
                            //each lap get next tool from list and fill in the toollist
                            //then attach a jobpage for the tool
                            //(still need to do pics after that)
                            using (StreamReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "/PDFs/JobPart.html", System.Text.Encoding.Default, true))
                            {
                                job = reader.ReadToEnd();
                            }
                            //Werkzeugliste insert


                            #region WerkzeuglisteInsert
                            pdfString = pdfString.Replace($"#Toollistitem{i + 1}Manufacturer", tools[i].ToollistItem.Manufacturer);
                            if (tools[i].ToollistItem.ToolPicturename != null)
                            {
                                pdfString = pdfString.ReplaceFirst($"#Toollistitem{i + 1}ToolPicture", Globals.PicturePath + tools[i].ToollistItem.ToolPicturename);
                            }
                            else
                            {
                                pdfString = pdfString.ReplaceFirst($"#Toollistitem{i + 1}ToolPicture", "");
                            }
                            pdfString = pdfString.Replace($"#Toollistitem{i + 1}Toolholder", tools[i].ToollistItem.Toolholder);
                            pdfString = pdfString.Replace($"#Toollistitem{i + 1}ToolId", tools[i].ToollistItem.ToolId);
                            pdfString = pdfString.Replace($"#Toollistitem{i + 1}Insert", tools[i].ToollistItem.Insert);
                            pdfString = pdfString.Replace($"#Toollistitem{i + 1}Coating", tools[i].ToollistItem.Coating);
                            pdfString = pdfString.Replace($"#Toollistitem{i + 1}Flutes", tools[i].ToollistItem.Flutes.ToString());
                            pdfString = pdfString.Replace($"#Toollistitem{i + 1}Cuttingedges", tools[i].ToollistItem.Cuttingedges.ToString());
                            pdfString = pdfString.Replace($"#Toollistitem{i + 1}Diameter", tools[i].ToollistItem.Diameter.ToString());
                            pdfString = pdfString.Replace($"#Toollistitem{i + 1}Necklength", tools[i].ToollistItem.Necklength.ToString());
                            pdfString = pdfString.Replace($"#Toollistitem{i + 1}Overhang", tools[i].ToollistItem.Overhang.ToString());
                            pdfString = pdfString.Replace($"#Toollistitem{i + 1}Processing", tools[i].ToollistItem.Processing);
                            #endregion
                            //job insert
                            job = job.Replace("#ToollistitemToolholder", tools[i].ToollistItem.Toolholder);
                            job = job.Replace("#ToollistitemToolid", tools[i].ToollistItem.ToolId);
                            job = job.Replace("#ToollistitemInsert", tools[i].ToollistItem.Insert);
                            job = job.Replace("#ToollistitemCoating", tools[i].ToollistItem.Coating);
                            job = job.Replace("#ToollistitemFlutes", tools[i].ToollistItem.Flutes.ToString());
                            job = job.Replace("#ToollistitemCuttingedges", tools[i].ToollistItem.Cuttingedges.ToString());
                            job = job.Replace("#ToollistitemDiameter", tools[i].ToollistItem.Diameter.ToString());
                            job = job.Replace("#ToollistitemNecklength", tools[i].ToollistItem.Necklength.ToString());
                            job = job.Replace("#ToollistitemOverhang", tools[i].ToollistItem.Overhang.ToString());
                            job = job.Replace("#ToollistitemProcessing", tools[i].ToollistItem.Processing);
                            if (tools[i].ToollistItem.ToolPicturename != null)
                            {
                                job = job.ReplaceFirst("#ToollistitemToolPicture", Globals.PicturePath + tools[i].ToollistItem.ToolPicturename);
                            }
                            else
                            {
                                job = job.ReplaceFirst("#ToollistitemToolPicture", "");
                            }
                            job = job.Replace("#ToolNr", i + 1.ToString());

                            List<string> help = new List<string>();
                            help.Add(tools[i].ToolvaluepagePicturename1);
                            help.Add(tools[i].ToolvaluepagePicturename2);
                            help.Add(tools[i].ToolvaluepagePicturename3);
                            help.Add(tools[i].ToolvaluepagePicturename4);
                            help.Add(tools[i].ToolvaluepagePicturename5);
                            var x = PictureHelper.MergePictures(help);
                            job = job.ReplaceFirst("#ToolvaluepagePictures", x);

                            job = job.Replace("#ToolvaluepageActualCuttingspeed", tools[i].ActualCuttingspeed.ToString());
                            job = job.Replace("#ToolvaluepageActualSpindlespeed", tools[i].ActualSpindlespeed.ToString());
                            job = job.Replace("#ToolvaluepageActualFeedPerMinute", tools[i].ActualFeedPerMinute.ToString());
                            job = job.Replace("#ToolvaluepageActualFeedPerTooth", tools[i].ActualFeedPerTooth.ToString());
                            job = job.Replace("#ToolvaluepageActualAxial", tools[i].ActualAxial.ToString());
                            job = job.Replace("#ToolvaluepageActualRadial", tools[i].ActualRadial.ToString());
                            job = job.Replace("#ToolvaluepageProcessing", tools[i].Processing);
                            job = job.Replace("#ToolvaluepageSpindleLoadingCapacity", tools[i].SpindleLoadingCapacity);
                            job = job.Replace("#ToolvaluepageCooling", tools[i].Cooling);
                            job = job.Replace("#ToolvaluepageOperationtime", tools[i].Operationtime.ToString());
                            job = job.Replace("#ToolvaluepageOfwear", tools[i].Ofwear);
                            job = job.Replace("#ToolvaluepageStrategy", tools[i].Strategy);
                            //Spanvolumen fräsen-->(Axial*Radial*FeedPerMinute)/1000
                            //Spanvolumen bohren-->(feedPerMinute*d*(pi*pi))/4000
                            double? val = (tools[i].ActualAxial * tools[i].ActualRadial * tools[i].ActualFeedPerMinute) / 1000;
                            if (val == null)
                            {
                                //if the report is not complete and one of the needed properties to calc the val just dont show anything
                                job = job.Replace("#CalculatedWithFormula", "");
                            }
                            else
                            {
                                job = job.Replace("#CalculatedWithFormula", val.ToString());
                            }
                            job = job.Replace("#ToolvaluepageRampingAngle", tools[i].RampingAngle.ToString());
                            job = job.Replace("#ToolvaluepageToolLife", tools[i].ToolLife);
                            job = job.Replace("#ToolvaluepageWearintensity", tools[i].Wearintensity);

                            job = job.Replace("#ToolvaluepageExpectedCuttingspeed", tools[i].ExpectedCuttingspeed.ToString());
                            job = job.Replace("#ToolvaluepageExpectedSpindlespeed", tools[i].ExpectedSpindlespeed.ToString());
                            job = job.Replace("#ToolvaluepageExpectedFeedPerMinute", tools[i].ExpectedFeedPerMinute.ToString());
                            job = job.Replace("#ToolvaluepageExpectedFeedPerTooth", tools[i].ExpectedFeedPerTooth.ToString());
                            job = job.Replace("#ToolvaluepageExpectedAxial", tools[i].ExpectedAxial.ToString());
                            job = job.Replace("#ToolvaluepageExpectedRadial", tools[i].ExpectedRadial.ToString());

                            job = job.Replace("#ToolvaluepageToolInfo", tools[i].ToolInfo);



                            //insert job part
                            pdfString = pdfString.Replace("#insertJobsBeforeHere", job + "#insertJobsBeforeHere");
                        }
                        //cleanup-->remove any leftover #Toollistitem.* of the file with regex (watchout of the pics in the Werkzeugliste!)
                        pdfString = Regex.Replace(pdfString, @"\s#Toollistitem.*", "");
                        pdfString = Regex.Replace(pdfString, "<img src=\"#Toollistitem.*\"\\s\\/>", "");
                        //remove the marker where to insert the job sites
                        pdfString = pdfString.Replace("#insertJobsBeforeHere", "");
                        //save the temp-pdf, after that return it
                        string path = System.IO.Path.GetTempPath() + DateTime.Now.Ticks + ".pdf";
                        ConverterProperties cp;
                        cp = new ConverterProperties();
                        cp.SetCharset("UTF-8");
                        cp.SetMediaDeviceDescription(
                            new iText.Html2pdf.Css.Media.MediaDeviceDescription(iText.Html2pdf.Css.Media.MediaType.PRINT)
                            );
                        using (FileStream fs = new FileStream(path, FileMode.Create))
                        {
                            HtmlConverter.ConvertToPdf(pdfString, fs, cp);
                        }
                        //return as byteArray
                        return Ok(System.IO.File.ReadAllBytes(path));
                    }
                }
                catch (Exception e)
                {
                    var x = new HttpRequestMessage();
                    x.Content = new StringContent(e.Message);
                    return new NotFoundResult(x);
                }

            }
        }
        #region old
        //[HttpGet]
        //[Route("TryDemoPdf/")]
        //public IHttpActionResult TryDemoPdf()
        //{
        //    GeneratePDF();
        //    return Ok();
        //}
        //private void GeneratePDF(/*string foFile, string pdfFile*/)
        //{
        //    //String s = "< html >< head >< style>h1 { color: red}p { font - family: \"Comic Sans MS\"}</ style ></ head >< body >< h1 > My First Heading</ h1 >  < p > My first paragraph.</ p > </ body ></ html > ";
        //    //string doesnt work with html-tag

        //    //String s = "" +
        //    //    "<style>" +
        //    //    "h1{color:red} " +
        //    //    "th{background-color:blue;color:yellow; border: 1px solid black;}" +
        //    //    "body{" +
        //    //   //$"background-image: url({AppDomain.CurrentDomain.BaseDirectory + " / Pics / watermark.jpg"});" +
        //    //   //"background-repeat: no-repeat;" +
        //    //   //"background-attachment: fixed;" +
        //    //   //"background-size: cover;" +
        //    //   // "}" +
        //    //    "</style>" +
        //    //    "<div id=\"waterMark\" style=\"position:absolute\">" +
        //    //    //$"<img style=\"opacity:0.5\" src=\"{AppDomain.CurrentDomain.BaseDirectory + "/Pics/watermark.jpg"}\">" +
        //    //    "</div>" +
        //    //    "<h1>Pdf-Test</h1>" +
        //    //    "<p>Hello World</p>" +
        //    //    $"<img src=\"{AppDomain.CurrentDomain.BaseDirectory + "/Pics/trm_logo.jpg"}\">" +
        //    //    "<table>" +
        //    //    "<tr>" +
        //    //    "<th>Demo</th><th>Empty</th>" +
        //    //    "</tr>" +
        //    //    "<tr>" +
        //    //    "<th>Test</th><th>Empty</th>" +
        //    //    "</tr>" +
        //    //    "</table>";
        //    ConverterProperties cp = new ConverterProperties();

        //    using (FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "/PDFs/demoPdf2.pdf", FileMode.Create))
        //    //using (FileStream fr = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "/PDFs/PdfFormat.html", FileMode.Open))
        //    using (StreamReader sr = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "/PDFs/PdfFormat.html"))
        //    {
        //        String format = sr.ReadToEnd();

        //        //found the fucking problem!-->width:100%; die % sind nicht erlaubt!

        //        HtmlConverter.ConvertToPdf(format, fs);
        //    }

        //    //FileInputStream streamFO = new FileInputStream(foFile);
        //    //InputSource src = new InputSource(streamFO);
        //    //FileStream streamOut = new FileStream(System.AppDomain.CurrentDomain.BaseDirectory + @"\PdfXML\testtabelle.pdf", FileMode.Create);
        //    //XmlDocument xml = new XmlDocument();
        //    //xml.Load(System.AppDomain.CurrentDomain.BaseDirectory + @"\PdfXML\testtabelle.fo");
        //    //FonetDriver driver = FonetDriver.Make();
        //    //driver.Render(xml, streamOut);
        //}
        #endregion
    }
}