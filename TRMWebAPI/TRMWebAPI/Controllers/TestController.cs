﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrmDb;

namespace TRMWebAPI.Controllers
{
    public class TestController : ApiController
    {
        //api/test
        public string Get()
        {
            try { 
            using (var db = new TrmContext())
            {
                    var x = db.Engines.Count();
                    return $"Engines count in the database: {x}";
            }
            }
            catch(Exception e)
            {
                return $"An error occured: {e.Message}\nStackTrace: {e.StackTrace}";
            }
        }
    }
}
