﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.Validations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ValidateCode : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string Dincode = (string)validationContext.ObjectType.GetProperty("Dincode").GetValue(validationContext.ObjectInstance, null);
            string Encode = (string)validationContext.ObjectType.GetProperty("Encode").GetValue(validationContext.ObjectInstance, null);

            //check if at least one has a value
            if (string.IsNullOrEmpty(Dincode) && string.IsNullOrEmpty(Encode))
            {
                return new ValidationResult("Either DIN-Code or Encode is required.");
            }
            return ValidationResult.Success;
        }
    }
}
