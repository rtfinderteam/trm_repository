﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.HelperClasses
{
    public class PdfName
    {


        public static string GetUniquePdfName()
        {
            return System.IO.Path.GetTempPath() + DateTime.Now.Ticks + ".pdf";
        }

    }
}
