﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.Classes
{
    public class JsonHelper
    {

        public static string BeautifyJson(string json)
        {
            json = json.Replace("\\", "");
            json = json.Replace("\"", "'");
            json = json.Remove(0, 1);
            json = json.Remove(json.Length - 1, 1);
            return json;

        }
    }
}
