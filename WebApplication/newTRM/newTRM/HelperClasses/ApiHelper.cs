﻿using Newtonsoft.Json;
using newTRM.HelperClasses;
using newTRM.Models;
using newTRM.NewModels.ApiModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace newTRM.Classes
{
    public class ApiHelper
    {
        
        private static string basicAddress = @"http://10.2.0.4:801//api";
        //private static string basicAddress = @"http://localhost:53673//api";

        public static async void NewCompleteReport(CompleteApiReport r)
        {
            var httpClient = SingleHttpClient.getClient;

            HttpResponseMessage response = new HttpResponseMessage();
            PerformAuth(httpClient);

            response = await httpClient.PostAsync($@"{basicAddress}/CompleteReport", new StringContent(JsonConvert.SerializeObject(r), Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();
        }
        public static async Task<List<ApiReport>> DoSearch(Search s)
        {
            var httpClient = SingleHttpClient.getClient;

            HttpResponseMessage response = new HttpResponseMessage();
            PerformAuth(httpClient);
            string content;

            response = await httpClient.PostAsync($@"{basicAddress}/search/GetReport", new StringContent(JsonConvert.SerializeObject(s), Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();
            content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ApiReport>>(content);
        }


        public static async Task<string> GetPdf(int reportId)
        {
            var httpClient = SingleHttpClient.getClient;

            HttpResponseMessage response = new HttpResponseMessage();
            PerformAuth(httpClient);
            byte[] content;

            response = await httpClient.GetAsync($@"{basicAddress}/pdf/getPdf/" + reportId);
            response.EnsureSuccessStatusCode();
            content = JsonConvert.DeserializeObject<byte[]>(await response.Content.ReadAsStringAsync());

            string pdf = PdfName.GetUniquePdfName();
            System.IO.File.WriteAllBytes(pdf, content);
            return pdf;
        }

        /// <summary>
        /// Helper for the API. **Is out of date therefore does not work for every model.**
        /// </summary>
        /// <param name="type"></param>
        /// <param name="thing">only needed for post and Put otherwise just the thing you want from the API, for isntance a Customer obj if you want a Customer(-->new Custoemr())</param>
        /// <param name="id">is optional except for PUT and DELETE</param>
        /// <returns>A result list of the the type of the thing</returns>
        public static async Task<List<T>> MakeRequest<T>(ApiTypes type, T thing, int id = -1)
        {
            //user stuff needs to be implemented once i have time for it
            var httpClient = SingleHttpClient.getClient;

            HttpResponseMessage response = new HttpResponseMessage();
            List<T> content = new List<T>();
            PerformAuth(httpClient);
            //if an model with a picture is used the name is wrong-->therefore just remove the Api from the name
            var n = typeof(T).Name;
            n = n.Replace("Api", "");

            switch (type)
            {
                case ApiTypes.GET:
                    if (id == -1)
                    {
                        response = await httpClient.GetAsync($@"{basicAddress}/{n}");
                        response.EnsureSuccessStatusCode();
                        content = JsonConvert.DeserializeObject<List<T>>(await response.Content.ReadAsStringAsync());
                        return content;
                    }
                    else
                    {
                        response = await httpClient.GetAsync($@"{basicAddress}/{n}/{id}");
                        response.EnsureSuccessStatusCode();
                        content = JsonConvert.DeserializeObject<List<T>>(await response.Content.ReadAsStringAsync());
                        return content;
                    }
                case ApiTypes.POST:
                    if (thing == null) return null;
                    response = await httpClient.PostAsync($@"{basicAddress}/{n}", new StringContent(JsonConvert.SerializeObject(thing), Encoding.UTF8, "application/json"));
                    response.EnsureSuccessStatusCode();
                    //api returns the id of the inserted object
                    int x = JsonConvert.DeserializeObject<int>(await response.Content.ReadAsStringAsync());
                    //find the id property of the generic thing and set it to the result of the api
                    thing.GetType().GetProperties().Where(y => y.Name == "id").First().SetValue(thing, x);
                    //add the inserted thing into the results list and return it
                    content.Add(thing);
                    return content;
                case ApiTypes.PUT:
                    if (thing == null || id == -1) return null;
                    response = await httpClient.PutAsync($@"{basicAddress}/{n}/{id}", new StringContent(JsonConvert.SerializeObject(thing), Encoding.UTF8, "application/json"));
                    response.EnsureSuccessStatusCode();
                    content = JsonConvert.DeserializeObject<List<T>>(await response.Content.ReadAsStringAsync());
                    return content;
                case ApiTypes.DELETE:
                    if (id == -1) return null;
                    response = await httpClient.DeleteAsync($@"{basicAddress}/{n}/{id}");
                    response.EnsureSuccessStatusCode();
                    content = JsonConvert.DeserializeObject<List<T>>(await response.Content.ReadAsStringAsync());
                    return content;
                default:
                    break;
            }
            return null;
        }
        private static void PerformAuth(HttpClient hc)
        {
            var byteArray = Encoding.ASCII.GetBytes("admin:nimda");
            hc.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
        }

    }
    //    public static async Task<string> MakeRequest<T>(ApiTypes type, T thing, int id = -1)
    //    {
    //        //user stuff needs to be implemented once i have time for it
    //        var httpClient = SingleHttpClient.getClient;

    //        HttpResponseMessage response = new HttpResponseMessage();
    //        string content;
    //        PerformAuth(httpClient);
    //        switch (type)
    //        {
    //            case ApiTypes.GET:
    //                if (id == -1)
    //                {
    //                    response = await httpClient.GetAsync($@"{basicAddress}/{typeof(T).Name}");
    //                    response.EnsureSuccessStatusCode();
    //                    content = await response.Content.ReadAsStringAsync();
    //                    return content;
    //                }
    //                else
    //                {
    //                    response = await httpClient.GetAsync($@"{basicAddress}/{typeof(T).Name}/{id}");
    //                    response.EnsureSuccessStatusCode();
    //                    content = await response.Content.ReadAsStringAsync();
    //                    return content;
    //                }
    //            case ApiTypes.POST:
    //                if (thing == null) return null;
    //                response = await httpClient.PostAsync($@"{basicAddress}/{typeof(T).Name}", new StringContent(JsonConvert.SerializeObject(thing), Encoding.UTF8, "application/json"));
    //                response.EnsureSuccessStatusCode();
    //                content = await response.Content.ReadAsStringAsync();
    //                return content;
    //            case ApiTypes.PUT:
    //                if (thing == null || id == -1) return null;
    //                response = await httpClient.PutAsync($@"{basicAddress}/{typeof(T).Name}/{id}", new StringContent(JsonConvert.SerializeObject(thing), Encoding.UTF8, "application/json"));
    //                response.EnsureSuccessStatusCode();
    //                content = await response.Content.ReadAsStringAsync();
    //                return content;
    //            case ApiTypes.DELETE:
    //                if (id == -1) return null;
    //                response = await httpClient.DeleteAsync($@"{basicAddress}/{typeof(T).Name}/{id}");
    //                response.EnsureSuccessStatusCode();
    //                content = await response.Content.ReadAsStringAsync();
    //                return content;
    //            default:
    //                break;
    //        }
    //        return null;
    //    }
    //    private static void PerformAuth(HttpClient hc)
    //    {
    //        var byteArray = Encoding.ASCII.GetBytes("admin:nimda");
    //        hc.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
    //    }

    //}
    public enum ApiTypes
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
