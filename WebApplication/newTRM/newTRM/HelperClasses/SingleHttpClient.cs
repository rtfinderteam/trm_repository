﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace newTRM.Classes
{
    public class SingleHttpClient
    {
        private static HttpClient client = null;
        private SingleHttpClient()
        {
        }
        public static HttpClient getClient
        {
            get
            {
                if (client == null)
                {
                    client = new HttpClient();
                   
                }
                return client;
            }
        }
    }
}
