﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using newTRM.Data;
using Microsoft.AspNetCore.Identity;
using newTRM.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace newTRM.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleController : Controller
    {
        UserManager<ApplicationUser> userManager;
        IConfiguration config;
        //in html-->IsInRole() to check if user is allowed to see

        public RoleController(UserManager<ApplicationUser> _userManager, IConfiguration _config)
        {

            userManager = _userManager;
            config = _config;
        }

        public IActionResult RoleManagement()
        {
            return View(GetUsers());
        }
        [HttpPost]
        public async Task<IActionResult> AddUser(string Email, string Password)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = Email, Email = Email };
                var result = await userManager.CreateAsync(user, Password);
                if (result.Succeeded)
                {
                    return View("RoleManagement", GetUsers());
                }
            }
            return View("RoleManagement", GetUsers());
        }

        private List<UsersModel> GetUsers()
        {
            var users = userManager.Users.Select(x => new UsersModel
            {
                UserId = x.Id,
                Username = x.UserName,
                Email = x.Email
            }).ToList();
            users.ForEach(x =>
            {
                x.IsAdmin = userManager.IsInRoleAsync(userManager.FindByNameAsync(x.Username).Result, "Admin").Result;
            });
            users.ForEach(x =>
            {
                x.IsEmployee = userManager.IsInRoleAsync(userManager.FindByNameAsync(x.Username).Result, "Employee").Result;
            });
            return users;
        }

        public async Task<IActionResult> RevokeRole(string id, string role)
        {
            var superadminId = config.GetSection("UserSettings")["UserId"];
            var user = userManager.FindByIdAsync(id).Result;
            if (!(user.Id.Equals(superadminId) && role.Equals("Admin")))
            {
                await userManager.RemoveFromRoleAsync(user, role);
            }
            return View("RoleManagement", GetUsers());
        }
        public async Task<IActionResult> GiveRole(string id, string role)
        {
            await userManager.AddToRoleAsync(userManager.FindByIdAsync(id).Result, role);
            return View("RoleManagement", GetUsers());
        }
        public async Task<IActionResult> DeleteUser(string id)
        {
            var user = userManager.FindByIdAsync(id).Result;
            var superadminId = config.GetSection("UserSettings")["UserId"];
            if (!user.Id.Equals(superadminId))
            {
                await userManager.DeleteAsync(user);
            }
            return View("RoleManagement", GetUsers());
        }
    }
}