﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using newTRM.Classes;
using newTRM.NewModels;
using newTRM.NewModels.ApiModels;

namespace newTRM.Controllers
{
    [Authorize(Roles = "Employee,Admin")]
    public class ReportController : Controller
    {
        [HttpPost]
        public IActionResult NoteReport(Note n)
        {
            var m = new NoteReportModel();
            m.note = n;
            return View(m);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<IActionResult> CreateNoteReport(CompleteModel model, int noteId)
        public async Task<IActionResult> CreateNoteReport(NoteReportModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //make the proper model-->pictures to Byte[]
                    CompleteApiReport r = new CompleteApiReport(model.completeModel);
                    //send POST to API
                    ApiHelper.NewCompleteReport(r);
                    //delete note
                    await ApiHelper.MakeRequest(ApiTypes.DELETE, new Note(), model.note.Id);
                    //return to index
                    return RedirectToAction("Index", "TRM");
                }
                catch (Exception e)
                {
                    //var m = new NoteReportModel();
                    //m.completeModel = model;
                    //m.note = new Note { Id = noteId };
                    return View("NoteReport", model);
                }
            }
            //var nrm = new NoteReportModel();
            //nrm.completeModel = model;
            //nrm.note = new Note { Id = noteId };
            return View(model);
        }


        public IActionResult Create()
        {
            return View(new CompleteModel());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CompleteModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //make the proper model-->pictures to Byte[]
                    CompleteApiReport r = new CompleteApiReport(model);
                    //send POST to API
                    ApiHelper.NewCompleteReport(r);
                    //return to index
                    return RedirectToAction("Index", "TRM");
                }
                catch (Exception e)
                {
                    return View(model);
                }
            }
            return View(model);
        }

        public async Task<JsonResult> AutocompleteCustomers()
        {
            List<Customer> customers = await ApiHelper.MakeRequest(ApiTypes.GET, new Customer());
            var c = customers.Select(x => new { id = x.Id, name = x.Name, branch = x.Branch })
                .GroupBy(x => new { x.name, x.branch })
                .Distinct()
                .Select(y => new
                {
                    id = y.FirstOrDefault().id,
                    name = y.First().name,
                    branch = y.First().branch
                })
                .ToList();
            return Json(c);
        }
        public async Task<JsonResult> AutocompleteEngines()
        {
            List<ApiEngine> engines = await ApiHelper.MakeRequest(ApiTypes.GET, new ApiEngine());
            var e = engines.Select(x => new
            {
                id = x.Id,
                manufacturer = x.Manufacturer,
                type = x.Type,
                spindle = x.Spindle,
                coolantoutside = x.Coolantoutside,
                coolantinside = x.Coolantinside,
                power = x.Power,
                rotationalspeed = x.Rotationalspeed,
                feed = x.Feed,
                activationyear = x.Activationyear,
                programming = x.Programming,
                controlling = x.Controlling,
                hourlyrate = x.Hourlyrate
            })
                .GroupBy(x => new
                {
                    x.manufacturer,
                    x.type,
                    x.spindle,
                    x.coolantoutside,
                    x.coolantinside,
                    x.power,
                    x.rotationalspeed,
                    x.feed,
                    x.activationyear,
                    x.programming,
                    x.controlling,
                    x.hourlyrate
                })
                .Distinct()
                .Select(y => new
                {
                    id = y.FirstOrDefault().id,
                    manufacturer = y.First().manufacturer,
                    type = y.First().type,
                    spindle = y.First().spindle,
                    coolantoutside = y.First().coolantoutside,
                    coolantinside = y.First().coolantinside,
                    power = y.First().power,
                    rotationalspeed = y.First().rotationalspeed,
                    feed = y.First().feed,
                    activationyear = y.First().activationyear,
                    programming = y.First().programming,
                    controlling = y.First().controlling,
                    hourlyrate = y.First().hourlyrate
                })
                .ToList();
            return Json(e);
        }
        public async Task<JsonResult> AutocompleteMaterials()
        {
            List<Material> materials = await ApiHelper.MakeRequest(ApiTypes.GET, new Material());
            var m = materials.Select(x => new
            {
                id = x.Id,
                designation = x.Designation,
                dincode = x.Dincode,
                encode = x.Encode,
                hardnesshb = x.HardnessHB,
                hardnesshrc = x.HardnessHRC,
                materialgroup = x.Materialgroup,
                crprc = x.CRpercent,
                wprc = x.Wpercent,
                sprc = x.Spercent,
                siprc = x.SIpercent,
                mnprc = x.MNpercent,
                nprc = x.NBpercent,
                niprc = x.NIpercent,
                vprc = x.Vpercent,
                moprc = x.MOpercent,
                cuprc = x.CUpercent,
                tiprc = x.TIpercent,
                pbprc = x.PBpercent,
                nbprc = x.NBpercent,
                coprc = x.COpercent,
                feprc = x.FEpercent,
                ceprc = x.CEpercent,
                alprc = x.ALpercent,
                cprc = x.Cpercent,
                znprc = x.ZNpercent,
                bprc = x.Bpercent,
                mgprc = x.MGpercent
            })
                .GroupBy(x => new
                {
                    x.designation,
                    x.dincode,
                    x.encode,
                    x.hardnesshb,
                    x.hardnesshrc,
                    x.materialgroup,
                    x.crprc,
                    x.wprc,
                    x.sprc,
                    x.siprc,
                    x.mnprc,
                    x.nprc,
                    x.niprc,
                    x.vprc,
                    x.moprc,
                    x.cuprc,
                    x.tiprc,
                    x.pbprc,
                    x.nbprc,
                    x.coprc,
                    x.feprc,
                    x.ceprc,
                    x.alprc,
                    x.cprc,
                    x.znprc,
                    x.bprc,
                    x.mgprc
                })
                .Distinct()
                .Select(y => new
                {
                    id = y.FirstOrDefault().id,
                    designation = y.First().designation,
                    dincode = y.First().dincode,
                    encode = y.First().encode,
                    hardnesshb = y.First().hardnesshb,
                    hardnesshrc = y.First().hardnesshrc,
                    materialgroup = y.First().materialgroup,
                    crprc = y.First().crprc,
                    wprc = y.First().wprc,
                    sprc = y.First().sprc,
                    siprc = y.First().siprc,
                    mnprc = y.First().mnprc,
                    nprc = y.First().nprc,
                    niprc = y.First().niprc,
                    vprc = y.First().vprc,
                    moprc = y.First().moprc,
                    cuprc = y.First().cuprc,
                    tiprc = y.First().tiprc,
                    pbprc = y.First().pbprc,
                    nbprc = y.First().nbprc,
                    coprc = y.First().coprc,
                    feprc = y.First().feprc,
                    ceprc = y.First().ceprc,
                    alprc = y.First().alprc,
                    cprc = y.First().cprc,
                    znprc = y.First().znprc,
                    bprc = y.First().bprc,
                    mgprc = y.First().mgprc
                })
                .ToList();
            return Json(m);
        }
    }
}