using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Data;
using Npgsql;
using newTRM.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using newTRM.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using newTRM.Classes;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using newTRM.NewModels.ApiModels;
using newTRM.NewModels;

namespace newTRM.Controllers
{
    public class TRMController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        public TRMController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;

        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> NotePartial()
        {
            List<Note> notes = await ApiHelper.MakeRequest(ApiTypes.GET, new Note());
            return PartialView("NotePartial", notes);
        }
        public async Task<int> DeleteNote(int noteId)
        {
            ApiHelper.MakeRequest(ApiTypes.DELETE, new Note(), noteId);
            return 1;
        }

        [Authorize]
        public IActionResult NewProjectFolder()
        {
            return View();
        }


        [Authorize(Roles = "Employee,Admin")]
        public IActionResult Search()
        {
            return View(new Search());
        }
        [HttpPost]
        [Authorize(Roles = "Employee,Admin")]
        public IActionResult Search(Search s)
        {
            try
            {
                List<ApiReport> reports = ApiHelper.DoSearch(s).Result;
                return View("SearchResults", reports);
            }
            catch (Exception e)
            {
                return View(s);
            }

        }
        [Authorize(Roles = "Employee,Admin")]
        public IActionResult ShowReport(int id)
        {
            try
            {
                var fileStream = new FileStream(ApiHelper.GetPdf(id).Result, FileMode.Open, FileAccess.Read);
                var fsResult = new FileStreamResult(fileStream, "application/pdf");

                return fsResult;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        [Authorize(Roles = "Employee,Admin")]
        public IActionResult EditReport(int id)
        {
            //not implemented

            return View("Index");
        }
        [Authorize(Roles = "Employee,Admin")]
        public async Task<IActionResult> DeleteReport(int id)
        {
            //even tho it uses the old Report-Model it actually should still work
            await ApiHelper.MakeRequest(ApiTypes.DELETE, new Report(), id);
            return View("Search", new Search());
        }
    }

}