﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.Models
{
    public class AddUserModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }
        [RegularExpression(@"(.{0,7}|[^0-9]*|[^A-Z]*|[^a-z]*|[a-zA-Z0-9]*)", ErrorMessage = "The {0} must have at least one upper-case and one lower-case letter as well as a sign and a number.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
