﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.Models
{
    public class Search
    {
        //TOOL erst wenn Schnittstelle fertig
        //TOOL erst wenn Schnittstelle fertig
        //public string supplier { get; set; } 
        //public string toolgroup { get; set; }
        //public int? diameter { get; set; }
        //public string articlenumber { get; set; }
        //public string wspquality { get; set; }

        public S_Material S_Material { get; set; }
        public S_Engine S_Engine { get; set; }
        public S_Customer S_Customer { get; set; }
        public S_Report S_Report { get; set; }
    }

    public class S_Material
    {
        [Display(Name = "Materialgroup")]
        public string materialgroup { get; set; } //materialgroup im Material
        [Display(Name = "Designation")]
        public string designation { get; set; } //designation im Material
        [Display(Name = "DIN-Code")]
        public string dincode { get; set; }
        [Display(Name = "EN-Code")]
        public string encode { get; set; }
        [Display(Name = "Hardness")]
        public string hardness { get; set; } //hardness im Material
    }

    public class S_Engine
    {
        [Display(Name = "Manufacturer")]
        public string manufacture { get; set; }
        [Display(Name = "Type")]
        public string typ { get; set; } //typ
        [Display(Name = "Spindle")]
        public string spindel { get; set; }
    }

    public class S_Customer
    {
        [Display(Name = "Name")]
        public string name { get; set; } //name
        [Display(Name = "Branch")]
        public string branch { get; set; }
    }

    public class S_Report
    {
        [Display(Name = "Strength")]
        public string strength { get; set; }
        [Display(Name = "Creator")]
        public string employeeshort { get; set; }
    }
}
