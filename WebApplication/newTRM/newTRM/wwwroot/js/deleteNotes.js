﻿
function deleteNote(id) {
    var elem = event.target.parentElement.parentElement.parentElement.parentElement.parentElement;
    console.log(elem);
    $.ajax({
        datatype: "text/plain",
        type: "POST",
        url: 'TRM/DeleteNote',
        data: {
            noteId: id
        },
        cache: false,
        success: function (data) {
            //somehow my jquery does not work in a partial view so i have to rely on vanilla js
            elem.parentElement.removeChild(elem);
        }
    });
}