﻿var customers;
var engines;
var materials;
$(document).ready(function () {
    console.log("doc ready");
    $.ajax({
        url: 'AutocompleteCustomers',
        dataType: "json",
        success: function (data) {
            customers = data;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Unable to fetch data!");
        }
    });
    $.ajax({
        url: 'AutocompleteEngines',
        dataType: "json",
        success: function (data) {
            engines = data;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Unable to fetch data!");
        }
    });
    $.ajax({
        url: 'AutocompleteMaterials',
        dataType: "json",
        success: function (data) {
            materials = data;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Unable to fetch data!");
        }
    });

});

$('#materialDincode').autocomplete({
    source: function (request, response) {
        response(
            $.ui.autocomplete.filter(
                $.map(materials, function (item) {
                    return {
                        label: item.dincode,
                        id: item.id
                    };
                }), request.term));
    },
    select: function (event, ui) {
        var data = materials.find(x => x.id === ui.item.id);
        $('#materialDesignation').val(data.designation);
        $('#materialEncode').val(data.encode);
        $('#materialHardnesshb').val(data.hardnesshb);
        $('#materialHardnesshrc').val(data.hardnesshrc);
        $('#materialMaterialgroup').val(data.materialgroup);
        $('#materialCrprc').val(data.crprc);
        $('#materialWprc').val(data.wprc);
        $('#materialSprc').val(data.sprc);
        $('#materialSiprc').val(data.siprc);
        $('#materialMnprc').val(data.mnprc);
        $('#materialNprc').val(data.nprc);
        $('#materialNiprc').val(data.niprc);
        $('#materialVprc').val(data.vprc);
        $('#materialMoprc').val(data.moprc);
        $('#materialCuprc').val(data.cuprc);
        $('#materialTiprc').val(data.tiprc);
        $('#materialPbprc').val(data.pbprc);
        $('#materialNbprc').val(data.nbprc);
        $('#materialCoprc').val(data.coprc);
        $('#materialFeprc').val(data.feprc);
        $('#materialCeprc').val(data.ceprc);
        $('#materialAlprc').val(data.alprc);
        $('#materialCprc').val(data.cprc);
        $('#materialZnprc').val(data.znprc);
        $('#materialBprc').val(data.bprc);
        $('#materialMgprc').val(data.mgprc);
    }
});
$('#materialEncode').autocomplete({
    source: function (request, response) {
        response(
            $.ui.autocomplete.filter($.map(materials, function (item) {
                return {
                    label: item.encode,
                    id: item.id
                };
            }), request.term));
    },
    select: function (event, ui) {
        var data = materials.find(x => x.id === ui.item.id);
        $('#materialDesignation').val(data.designation);
        $('#materialDincode').val(data.dincode);
        $('#materialHardnesshb').val(data.hardnesshb);
        $('#materialHardnesshrc').val(data.hardnesshrc);
        $('#materialMaterialgroup').val(data.materialgroup);
        $('#materialCrprc').val(data.crprc);
        $('#materialWprc').val(data.wprc);
        $('#materialSprc').val(data.sprc);
        $('#materialSiprc').val(data.siprc);
        $('#materialMnprc').val(data.mnprc);
        $('#materialNprc').val(data.nprc);
        $('#materialNiprc').val(data.niprc);
        $('#materialVprc').val(data.vprc);
        $('#materialMoprc').val(data.moprc);
        $('#materialCuprc').val(data.cuprc);
        $('#materialTiprc').val(data.tiprc);
        $('#materialPbprc').val(data.pbprc);
        $('#materialNbprc').val(data.nbprc);
        $('#materialCoprc').val(data.coprc);
        $('#materialFeprc').val(data.feprc);
        $('#materialCeprc').val(data.ceprc);
        $('#materialAlprc').val(data.alprc);
        $('#materialCprc').val(data.cprc);
        $('#materialZnprc').val(data.znprc);
        $('#materialBprc').val(data.bprc);
        $('#materialMgprc').val(data.mgprc);
    }
});
$('#customerName').autocomplete({
    source: function (request, response) {
        response(
            $.ui.autocomplete.filter(
                $.map(customers, function (item) {
                    return {
                        label: item.name,
                        id: item.id
                    };
                }), request.term));
    },
    select: function (event, ui) {
        var data = customers.find(x => x.id === ui.item.id);
        $('#customerBranch').val(data.branch);
    }
});
$('#engineManufacturer').autocomplete({
    source: function (request, response) {
        response(
            $.ui.autocomplete.filter($.map(engines, function (item) {
                return {
                    label: item.manufacturer,
                    id: item.id
                };
            }), request.term));
    },
    select: function (event, ui) {
        var data = engines.find(x => x.id === ui.item.id);
        $('#engineType').val(data.type);
        $('#engineSpindle').val(data.spindle);
        $('#engineCoolantoutside').val(data.coolantoutside);
        $('#engineCoolantinside').val(data.coolantinside);
        $('#enginePower').val(data.power);
        $('#engineRotationalspeed').val(data.rotationalspeed);
        $('#engineFeed').val(data.feed);
        $('#engineActivationyear').val(data.activationyear);
        $('#engineProgramming').val(data.programming);
        $('#engineControlling').val(data.controlling);
        $('#engineHourlyrate').val(data.hourlyrate);
    }
});
$('#engineType').autocomplete({
    source: function (request, response) {
        response(
            $.ui.autocomplete.filter($.map(engines, function (item) {
                return {
                    label: item.type,
                    id: item.id
                };
            }), request.term));
    },
    select: function (event, ui) {
        var data = engines.find(x => x.id === ui.item.id);
        $('#engineManufacturer').val(data.manufacturer);
        $('#engineSpindle').val(data.spindle);
        $('#engineCoolantoutside').val(data.coolantoutside);
        $('#engineCoolantinside').val(data.coolantinside);
        $('#enginePower').val(data.power);
        $('#engineRotationalspeed').val(data.rotationalspeed);
        $('#engineFeed').val(data.feed);
        $('#engineActivationyear').val(data.activationyear);
        $('#engineProgramming').val(data.programming);
        $('#engineControlling').val(data.controlling);
        $('#engineHourlyrate').val(data.hourlyrate);


    }
});