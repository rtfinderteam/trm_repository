﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.NewModels
{
    public class Note
    {
        public Note()
        {
            Date = new DateTime();
        }
        public int Id { get; set; }
        [Display(Name = "Date")]
        public DateTime? Date { get; set; } = new DateTime();
        [Display(Name = "Creator")]
        public string Employeeshort { get; set; }
        [Display(Name = "Text")]
        public string Text { get; set; }
    }
}
