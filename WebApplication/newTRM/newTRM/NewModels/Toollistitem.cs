﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.NewModels
{
    public class ToollistItem
    {
        public int Id { get; set; }
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg)$", ErrorMessage = "Please select an image-file.")]
        [Display(Name = "Tool picture")]
        public IFormFile ToolPicturename { get; set; }
        //[Required]
        [Display(Name = "Manufacturer")]
        public string Manufacturer { get; set; }
        //[Required]
        [Display(Name = "Competitor")]
        public bool IsCompetitor { get; set; }
        //[Required]
        [Display(Name = "Toolholder")]
        public string Toolholder { get; set; }
        //[Required]
        [Display(Name = "Tool-ID")]
        public string ToolId { get; set; }
        //[Required]
        [Display(Name = "Insert")]
        public string Insert { get; set; }
        //[Required]
        [Display(Name = "Coating")]
        public string Coating { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        //[Required]
        [Display(Name = "Flutes")]
        public int? Flutes { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        //[Required]
        [Display(Name = "Cutting edges")]
        public int? Cuttingedges { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        //[Required]
        [Display(Name = "Diameter")]
        public int? Diameter { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        //[Required]
        [Display(Name = "Necklength")]
        public int? Necklength { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        //[Required]
        [Display(Name = "Overhang")]
        public int? Overhang { get; set; }
        //[Required]
        [Display(Name = "Processing")]
        public string Processing { get; set; }
        public virtual Report Report { get; set; }
    }
}
