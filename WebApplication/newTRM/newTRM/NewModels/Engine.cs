﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.NewModels
{
    public class Engine
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Manufacturer")]
        public string Manufacturer { get; set; }
        [Required]
        [Display(Name = "Type")]
        public string Type { get; set; }
        [Required]
        [Display(Name = "Spindle")]
        public string Spindle { get; set; }
        [Required]
        [Display(Name = "Coolant outside")]
        public double Coolantoutside { get; set; }
        [Required]
        [Display(Name = "Coolant inside")]
        public double Coolantinside { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        [Required]
        [Display(Name = "Power")]
        public int Power { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        [Required]
        [Display(Name = "Rotational speed")]
        public int Rotationalspeed { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        [Required]
        [Display(Name = "Feed")]
        public int Feed { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        [Required]
        [Display(Name = "Activation year")]
        public int Activationyear { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        [Required]
        [Display(Name = "Hourly rate")]
        public int Hourlyrate { get; set; }
        [Required]
        [Display(Name = "Programming")]
        public string Programming { get; set; }
        [Required]
        [Display(Name = "Controlling")]
        public string Controlling { get; set; }
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg)$", ErrorMessage = "Please select an image-file.")]
        [Display(Name = "Engine picture")]
        public IFormFile EnginePicturename { get; set; }
    }
}
