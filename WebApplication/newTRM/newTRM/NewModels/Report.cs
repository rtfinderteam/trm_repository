﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace newTRM.NewModels
{
    public class Report
    {
        public Report()
        {
            this.Date = new DateTime();
        }
        public int Id { get; set; }
        [Required]
        [Display(Name = "Project name")]
        public string Projectname { get; set; }
        [Required]
        [Display(Name = "Creator")]
        public string Employeeshort { get; set; }
        [DataType(DataType.Date)]
        [Required]
        [Display(Name = "Date")]
        public DateTime? Date { get; set; } = new DateTime();
        [Required]
        [Display(Name = "Building component")]
        public string Buildingcomponent { get; set; }
        [Required]
        [Display(Name = "Workpiece clamping")]
        public string Workpiececlamping { get; set; }
        [Required]
        [Display(Name = "Strength")]
        public string Strength { get; set; }
        [Required]
        [Display(Name = "Engine information")]
        public string EngineInfo { get; set; }
        [Required]
        [Display(Name = "Material information")]
        public string MaterialInfo { get; set; }
        [Required]
        [Display(Name = "Clamping information")]
        public string ClampingInfo { get; set; }
        public virtual Engine Engine { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Material Material { get; set; }
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg)$", ErrorMessage = "Please select an image-file.")]
        [Display(Name = "Report picture 1")]
        public IFormFile ReportPicturename1 { get; set; }
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg)$", ErrorMessage = "Please select an image-file.")]
        [Display(Name = "Report picture 2")]
        public IFormFile ReportPicturename2 { get; set; }
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg)$", ErrorMessage = "Please select an image-file.")]
        [Display(Name = "Report picture 3")]
        public IFormFile ReportPicturename3 { get; set; }
    }
}
