﻿using newTRM.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.NewModels
{
    public class Material
    {
        public int Id { get; set; }
        [Display(Name = "DIN-Code")]
        //[ValidateCode]
        public string Dincode { get; set; }
        [Display(Name = "EN-Code")]
        public string Encode { get; set; }
        [Required]
        [Display(Name = "Designation")]
        public string Designation { get; set; }
        [Required]
        [Display(Name = "Hardness (HRC)")]
        public string HardnessHRC { get; set; }
        [Required]
        [Display(Name = "Hardness (HB)")]
        public string HardnessHB { get; set; }
        [Required]
        [Display(Name = "Material group")]
        public string Materialgroup { get; set; }
        [Required]
        [Display(Name = "Cr %")]
        public double CRpercent { get; set; }
        [Required]
        [Display(Name = "Si %")]
        public double SIpercent { get; set; }
        [Required]
        [Display(Name = "Ni %")]
        public double NIpercent { get; set; }
        [Required]
        [Display(Name = "Cu %")]
        public double CUpercent { get; set; }
        [Required]
        [Display(Name = "Nb %")]
        public double NBpercent { get; set; }
        [Required]
        [Display(Name = "Ce %")]
        public double CEpercent { get; set; }
        [Required]
        [Display(Name = "Zn %")]
        public double ZNpercent { get; set; }
        [Required]
        [Display(Name = "W %")]
        public double Wpercent { get; set; }
        [Required]
        [Display(Name = "Mn %")]
        public double MNpercent { get; set; }
        [Required]
        [Display(Name = "V %")]
        public double Vpercent { get; set; }
        [Required]
        [Display(Name = "Ti %")]
        public double TIpercent { get; set; }
        [Required]
        [Display(Name = "Co %")]
        public double COpercent { get; set; }
        [Required]
        [Display(Name = "Al %")]
        public double ALpercent { get; set; }
        [Required]
        [Display(Name = "B %")]
        public double Bpercent { get; set; }
        [Required]
        [Display(Name = "S %")]
        public double Spercent { get; set; }
        [Required]
        [Display(Name = "N %")]
        public double Npercent { get; set; }
        [Required]
        [Display(Name = "Mo %")]
        public double MOpercent { get; set; }
        [Required]
        [Display(Name = "Pb %")]
        public double PBpercent { get; set; }
        [Required]
        [Display(Name = "Fe %")]
        public double FEpercent { get; set; }
        [Required]
        [Display(Name = "C %")]
        public double Cpercent { get; set; }
        [Required]
        [Display(Name = "Mg %")]
        public double MGpercent { get; set; }
    }
}
