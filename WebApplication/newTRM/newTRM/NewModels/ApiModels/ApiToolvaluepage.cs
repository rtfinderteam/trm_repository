﻿using newTRM.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.NewModels.ApiModels
{
    public class ApiToolvaluepage
    {
        public ApiToolvaluepage(Toolvaluepage t)
        {
            this.Id = t.Id;
            this.ActualAxial = t.ActualAxial;
            this.ActualCuttingspeed = t.ActualCuttingspeed;
            this.ActualFeedPerMinute = t.ActualFeedPerMinute;
            this.ActualFeedPerRotation = t.ActualFeedPerRotation;
            this.ActualFeedPerTooth = t.ActualFeedPerTooth;
            this.ActualRadial = t.ActualRadial;
            this.ActualSpindlespeed = t.ActualSpindlespeed;
            this.Application = t.Application;
            this.Applicationtype = t.Applicationtype;
            this.Cooling = t.Cooling;
            this.ExpectedAxial = t.ExpectedAxial;
            this.ExpectedCuttingspeed = t.ExpectedCuttingspeed;
            this.ExpectedFeedPerMinute = t.ExpectedFeedPerMinute;
            this.ExpectedFeedPerRotation = t.ExpectedFeedPerRotation;
            this.ExpectedFeedPerTooth = t.ExpectedFeedPerTooth;
            this.ExpectedRadial = t.ExpectedRadial;
            this.ExpectedSpindlespeed = t.ExpectedSpindlespeed;
            this.Hasavideo = t.Hasavideo;
            this.MaterialRemovalrate = t.MaterialRemovalrate;
            this.Noise = t.Noise;
            this.Ofwear = t.Ofwear;
            this.Operationtime = t.Operationtime;
            this.Processing = t.Processing;
            this.RampingAngle = t.RampingAngle;
            this.Resolution = t.Resolution;
            this.SpindleLoadingCapacity = t.SpindleLoadingCapacity;
            this.Strategy = t.Strategy;
            this.ToolInfo = t.ToolInfo;
            this.ToolLife = t.ToolLife;
            //t.ToollistItem==null-->ctor of ApiToolListItem cant handle
            //this.ToollistItem = new ApiToollistItem(t.ToollistItem);
            this.ToolvaluepagePicture1 = PictureHelper.ConvertToBytes(t.ToolvaluepagePicturename1);
            this.ToolvaluepagePicture2 = PictureHelper.ConvertToBytes(t.ToolvaluepagePicturename2);
            this.ToolvaluepagePicture3 = PictureHelper.ConvertToBytes(t.ToolvaluepagePicturename3);
            this.ToolvaluepagePicture4 = PictureHelper.ConvertToBytes(t.ToolvaluepagePicturename4);
            this.ToolvaluepagePicture5 = PictureHelper.ConvertToBytes(t.ToolvaluepagePicturename5);
            this.Wearintensity = t.Wearintensity;
        }
        public int? Id { get; set; }

        public string Processing { get; set; }

        public string Noise { get; set; }

        public string Cooling { get; set; }

        public int? Operationtime { get; set; }

        public string Ofwear { get; set; }

        public string Wearintensity { get; set; }

        public string Strategy { get; set; }

        public string MaterialRemovalrate { get; set; }

        public double? RampingAngle { get; set; }

        public string Application { get; set; }

        public double? ActualCuttingspeed { get; set; }

        public double? ActualSpindlespeed { get; set; }

        public double? ActualFeedPerMinute { get; set; }

        public double? ActualFeedPerTooth { get; set; }

        public double? ActualFeedPerRotation { get; set; }

        public double? ActualAxial { get; set; }

        public double? ActualRadial { get; set; }

        public double? ExpectedCuttingspeed { get; set; }

        public double? ExpectedSpindlespeed { get; set; }

        public double? ExpectedFeedPerMinute { get; set; }

        public double? ExpectedFeedPerTooth { get; set; }

        public double? ExpectedFeedPerRotation { get; set; }

        public double? ExpectedAxial { get; set; }

        public double? ExpectedRadial { get; set; }

        public int? Resolution { get; set; }

        public bool Hasavideo { get; set; }

        public string Applicationtype { get; set; }

        public string SpindleLoadingCapacity { get; set; }

        public string ToolLife { get; set; }

        public string ToolInfo { get; set; }
        public virtual ApiToollistItem ToollistItem { get; set; }

        public byte[] ToolvaluepagePicture1 { get; set; }

        public byte[] ToolvaluepagePicture2 { get; set; }

        public byte[] ToolvaluepagePicture3 { get; set; }

        public byte[] ToolvaluepagePicture4 { get; set; }

        public byte[] ToolvaluepagePicture5 { get; set; }
    }
}
