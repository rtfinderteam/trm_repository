﻿using newTRM.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.NewModels.ApiModels
{
    public class ApiReport
    {
        public ApiReport(Report r)
        {
            this.Id = r.Id;
            this.Projectname = r.Projectname;
            this.Employeeshort = r.Employeeshort;
            this.Date = r.Date;
            this.Buildingcomponent = r.Buildingcomponent;
            this.Workpiececlamping = r.Workpiececlamping;
            this.Strength = r.Strength;
            this.Engine = new ApiEngine(r.Engine);
            this.EngineInfo = r.EngineInfo;
            this.Material = r.Material;
            this.Customer = r.Customer;
            this.ClampingInfo = r.ClampingInfo;
            this.MaterialInfo = r.MaterialInfo;
            this.ReportPicture1 = PictureHelper.ConvertToBytes(r.ReportPicturename1);
            this.ReportPicture2 = PictureHelper.ConvertToBytes(r.ReportPicturename2);
            this.ReportPicture3 = PictureHelper.ConvertToBytes(r.ReportPicturename3);

        }
        public ApiReport()
        {
            this.Date = new DateTime();
        }
        public int Id { get; set; }

        public string Projectname { get; set; }

        public string Employeeshort { get; set; }

        public DateTime? Date { get; set; } = new DateTime();

        public string Buildingcomponent { get; set; }

        public string Workpiececlamping { get; set; }

        public string Strength { get; set; }

        public string EngineInfo { get; set; }

        public string MaterialInfo { get; set; }

        public string ClampingInfo { get; set; }
        public virtual ApiEngine Engine { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Material Material { get; set; }

        public Byte[] ReportPicture1 { get; set; }

        public Byte[] ReportPicture2 { get; set; }

        public Byte[] ReportPicture3 { get; set; }
    }
}
