﻿using newTRM.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.NewModels.ApiModels
{
    public class ApiToollistItem
    {
        public ApiToollistItem(ToollistItem t)
        {
            this.Coating = t.Coating;
            this.Cuttingedges = t.Cuttingedges;
            this.Diameter = t.Diameter;
            this.Flutes = t.Flutes;
            this.Id = t.Id;
            this.Insert = t.Insert;
            this.IsCompetitor = t.IsCompetitor;
            this.Manufacturer = t.Manufacturer;
            this.Necklength = t.Necklength;
            this.Overhang = t.Overhang;
            this.Processing = t.Processing;
            //t.Report==null-->const of ApiReport cant handle
            //this.Report = new ApiReport(t.Report);
            this.Toolholder = t.Toolholder;
            this.ToolId = t.ToolId;
            this.ToolPicture = PictureHelper.ConvertToBytes(t.ToolPicturename);
        }
        public int? Id { get; set; }
        public byte[] ToolPicture { get; set; }

        public string Manufacturer { get; set; }

        public bool IsCompetitor { get; set; }

        public string Toolholder { get; set; }

        public string ToolId { get; set; }

        public string Insert { get; set; }

        public string Coating { get; set; }

        public int? Flutes { get; set; }

        public int? Cuttingedges { get; set; }

        public int? Diameter { get; set; }

        public int? Necklength { get; set; }

        public int? Overhang { get; set; }
        public string Processing { get; set; }
        public virtual ApiReport Report { get; set; }
    }
}
