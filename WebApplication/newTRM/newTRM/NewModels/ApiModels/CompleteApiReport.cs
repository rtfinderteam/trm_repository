﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.NewModels.ApiModels
{
    public class CompleteApiReport
    {
        public CompleteApiReport(CompleteModel m)
        {
            this.Id = m.Id;
            this.Report = new ApiReport(m.Report);
            this.Toollistitem1 = new ApiToollistItem(m.Toollistitem1);
            this.Toolvaluepage1 = new ApiToolvaluepage(m.Toolvaluepage1);
            this.Toollistitem2 = new ApiToollistItem(m.Toollistitem2);
            this.Toolvaluepage2 = new ApiToolvaluepage(m.Toolvaluepage2);
            this.Toollistitem3 = new ApiToollistItem(m.Toollistitem3);
            this.Toolvaluepage3 = new ApiToolvaluepage(m.Toolvaluepage3);
            this.Toollistitem4 = new ApiToollistItem(m.Toollistitem4);
            this.Toolvaluepage4 = new ApiToolvaluepage(m.Toolvaluepage4);
            this.Toollistitem5 = new ApiToollistItem(m.Toollistitem5);
            this.Toolvaluepage5 = new ApiToolvaluepage(m.Toolvaluepage5);
            this.Toollistitem6 = new ApiToollistItem(m.Toollistitem6);
            this.Toolvaluepage6 = new ApiToolvaluepage(m.Toolvaluepage6);
            this.Toollistitem7 = new ApiToollistItem(m.Toollistitem7);
            this.Toolvaluepage7 = new ApiToolvaluepage(m.Toolvaluepage7);

        }

        public int Id { get; set; }
        public ApiReport Report { get; set; }
        public ApiToollistItem Toollistitem1 { get; set; }
        public ApiToolvaluepage Toolvaluepage1 { get; set; }
        public ApiToollistItem Toollistitem2 { get; set; }
        public ApiToolvaluepage Toolvaluepage2 { get; set; }
        public ApiToollistItem Toollistitem3 { get; set; }
        public ApiToolvaluepage Toolvaluepage3 { get; set; }
        public ApiToollistItem Toollistitem4 { get; set; }
        public ApiToolvaluepage Toolvaluepage4 { get; set; }
        public ApiToollistItem Toollistitem5 { get; set; }
        public ApiToolvaluepage Toolvaluepage5 { get; set; }
        public ApiToollistItem Toollistitem6 { get; set; }
        public ApiToolvaluepage Toolvaluepage6 { get; set; }
        public ApiToollistItem Toollistitem7 { get; set; }
        public ApiToolvaluepage Toolvaluepage7 { get; set; }
    }
}
