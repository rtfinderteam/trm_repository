﻿using newTRM.HelperClasses;

namespace newTRM.NewModels.ApiModels
{
    public class ApiEngine
    {
        public ApiEngine()
        {

        }
        public ApiEngine(Engine e)
        {
            this.Id = e.Id;
            this.Hourlyrate = e.Hourlyrate;
            this.Activationyear = e.Activationyear;
            this.Controlling = e.Controlling;
            this.Coolantinside = e.Coolantinside;
            this.Coolantoutside = e.Coolantoutside;
            this.EnginePicture = PictureHelper.ConvertToBytes(e.EnginePicturename);
            this.Feed = e.Feed;
            this.Manufacturer = e.Manufacturer;
            this.Power = e.Power;
            this.Programming = e.Programming;
            this.Rotationalspeed = e.Rotationalspeed;
            this.Spindle = e.Spindle;
            this.Type = e.Type;
        }
        public int Id { get; set; }

        public string Manufacturer { get; set; }

        public string Type { get; set; }

        public string Spindle { get; set; }

        public double Coolantoutside { get; set; }

        public double Coolantinside { get; set; }

        public int Power { get; set; }

        public int Rotationalspeed { get; set; }

        public int Feed { get; set; }

        public int Activationyear { get; set; }

        public int Hourlyrate { get; set; }

        public string Programming { get; set; }

        public string Controlling { get; set; }

        public byte[] EnginePicture { get; set; }
    }
}