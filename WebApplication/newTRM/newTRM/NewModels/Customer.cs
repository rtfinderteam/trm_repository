﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.NewModels
{
    public class Customer
    {
        public int Id { get; set; }
        [Required]
        [Display(Name="Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Branch")]
        public string Branch { get; set; }
        [Required]
        [Display(Name = "Contact Partner")]
        public string Contactpartner { get; set; }
    }
}
