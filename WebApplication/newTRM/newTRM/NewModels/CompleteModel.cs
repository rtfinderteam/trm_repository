﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.NewModels
{
    public class CompleteModel
    {
        public int Id { get; set; }
        public Report Report { get; set; }
        public ToollistItem Toollistitem1 { get; set; }
        public Toolvaluepage Toolvaluepage1 { get; set; }

        public ToollistItem Toollistitem2 { get; set; }
        public Toolvaluepage Toolvaluepage2 { get; set; }
        public ToollistItem Toollistitem3 { get; set; }
        public Toolvaluepage Toolvaluepage3 { get; set; }
        public ToollistItem Toollistitem4 { get; set; }
        public Toolvaluepage Toolvaluepage4 { get; set; }
        public ToollistItem Toollistitem5 { get; set; }
        public Toolvaluepage Toolvaluepage5 { get; set; }
        public ToollistItem Toollistitem6 { get; set; }
        public Toolvaluepage Toolvaluepage6 { get; set; }
        public ToollistItem Toollistitem7 { get; set; }
        public Toolvaluepage Toolvaluepage7 { get; set; }

    }
}
