﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace newTRM.NewModels
{
    public class Toolvaluepage
    {
        public int Id { get; set; }
        //[Required]
        [Display(Name = "Processing")]
        public string Processing { get; set; }
        //[Required]
        [Display(Name = "Noise")]
        public string Noise { get; set; }
        //[Required]
        [Display(Name = "Cooling")]
        public string Cooling { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        //[Required]
        [Display(Name = "Operation time")]
        public int? Operationtime { get; set; }
        //[Required]
        [Display(Name = "Wear")]
        public string Ofwear { get; set; }
        //[Required]
        [Display(Name = "Wear intensity")]
        public string Wearintensity { get; set; }
        //[Required]
        [Display(Name = "Strategy")]
        public string Strategy { get; set; }
        //[Required]
        [Display(Name = "Material removal rate")]
        public string MaterialRemovalrate { get; set; }
        //[Required]
        [Display(Name = "Ramping angle")]
        public double? RampingAngle { get; set; }
        //[Required]
        [Display(Name = "Application")]
        public string Application { get; set; }
        //actual is always displayed without the actual 'cause of the page layout
        //[Required]
        [Display(Name = "Cutting speed")]
        public double? ActualCuttingspeed { get; set; }
        //[Required]
        [Display(Name = "Spindle speed")]
        public double? ActualSpindlespeed { get; set; }
        //[Required]
        [Display(Name = "Feed per minute")]
        public double? ActualFeedPerMinute { get; set; }
        //[Required]
        [Display(Name = "Feed per tooth")]
        public double? ActualFeedPerTooth { get; set; }
        //[Required]
        [Display(Name = "Feed per rotation")]
        public double? ActualFeedPerRotation { get; set; }
        //[Required]
        [Display(Name = "Axial")]
        public double? ActualAxial { get; set; }
        //[Required]
        [Display(Name = "Radial")]
        public double? ActualRadial { get; set; }
        //[Required]
        [Display(Name = "Expected cutting speed")]
        public double? ExpectedCuttingspeed { get; set; }
        //[Required]
        [Display(Name = "Expected spindle speed")]
        public double? ExpectedSpindlespeed { get; set; }
        //[Required]
        [Display(Name = "Expected feed per minute")]
        public double? ExpectedFeedPerMinute { get; set; }
        //[Required]
        [Display(Name = "Expected feed per tooth")]
        public double? ExpectedFeedPerTooth { get; set; }
        //[Required]
        [Display(Name = "Expected feed per rotation")]
        public double? ExpectedFeedPerRotation { get; set; }
        //[Required]
        [Display(Name = "Expected axial")]
        public double? ExpectedAxial { get; set; }
        //[Required]
        [Display(Name = "Expected radial")]
        public double? ExpectedRadial { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Please enter a valid number.")]
        //[Required]
        [Display(Name = "Resolution")]
        public int? Resolution { get; set; }
        //[Required]
        [Display(Name = "Video")]
        public bool Hasavideo { get; set; }
        //[Required]
        [Display(Name = "Application type")]
        public string Applicationtype { get; set; }
        //[Required]
        [Display(Name = "Spindle loading capacity")]
        public string SpindleLoadingCapacity { get; set; }
        //[Required]
        [Display(Name = "Tool life")]
        public string ToolLife { get; set; }
        //[Required]
        [Display(Name = "Tool Information")]
        public string ToolInfo { get; set; }
        public virtual ToollistItem ToollistItem { get; set; }
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg)$", ErrorMessage = "Please select an image-file.")]
        [Display(Name = "Tool picture 1")]
        public IFormFile ToolvaluepagePicturename1 { get; set; }
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg)$", ErrorMessage = "Please select an image-file.")]
        [Display(Name = "Tool picture 2")]
        public IFormFile ToolvaluepagePicturename2 { get; set; }
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg)$", ErrorMessage = "Please select an image-file.")]
        [Display(Name = "Tool picture 3")]
        public IFormFile ToolvaluepagePicturename3 { get; set; }
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg)$", ErrorMessage = "Please select an image-file.")]
        [Display(Name = "Tool picture 4")]
        public IFormFile ToolvaluepagePicturename4 { get; set; }
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg)$", ErrorMessage = "Please select an image-file.")]
        [Display(Name = "Tool picture 5")]
        public IFormFile ToolvaluepagePicturename5 { get; set; }
    }
}
